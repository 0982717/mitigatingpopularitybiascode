# -*- coding: utf-8 -*-
"""
Created on Tue Feb 28 16:54:35 2023

@author: unrob
"""
import secrets
from flask import Flask, request, render_template, redirect, session, url_for
import webbrowser

import csv
import os
import time
import datetime
import random
import string
import pandas as pd
import numpy as np
import spotipy
from requests.auth import HTTPBasicAuth
import requests
from requests_oauthlib import OAuth2Session

app = Flask(__name__)
app.secret_key = secrets.token_hex(16)
redirect_uri = 'http://localhost:8888/callback'
token_url = 'https://accounts.spotify.com/api/token'
auth_url = 'https://accounts.spotify.com/authorize'

client_id='' #insert your client ID here
client_secret='' #insert your client secret here
# Set up Spotipy authentication credentials
scope = "user-read-private user-read-email user-read-recently-played user-top-read"

sp_oauth = spotipy.oauth2.SpotifyOAuth(client_id=client_id, client_secret=client_secret,
                                      redirect_uri=redirect_uri, scope=scope)


def get_headers(token):
    return {"Authorization": "Bearer " + token}


@app.route('/')
def index():
    # Generate a random ID for the user
    user_id = ''.join(random.choices(string.ascii_uppercase + string.digits, k=10))
    
    # Store the user ID in the session
    session['user_id'] = user_id
    
    return render_template('login.html', user_id=user_id)


@app.route('/login')
def login():
    auth_url = sp_oauth.get_authorize_url()
    return redirect(auth_url)


@app.route('/callback')
def callback():
    code = request.args.get('code')
    #token_info = sp_oauth.get_access_token() ###this does not get access to the user's profile but the token of the client's account
    uris = pd.read_csv('data/spotify/Processed/spotify_uris.csv')
    track_popularities = pd.read_csv('data/spotify/Processed/track_popularity.csv')
    
    code = request.args.get('code')
    resp = requests.post(token_url,
        auth=HTTPBasicAuth(client_id, client_secret),
        data={
            'grant_type': 'authorization_code',
            'code': code,
            'redirect_uri': redirect_uri
        })
    access_token = resp.json()['access_token']

    if access_token:

        # Create a new Spotipy client with the access token
        sp = spotipy.Spotify(auth=access_token)
        
        # Get the user's profile information
        user_info = sp.current_user()
        
        # Extract the user email
        user_email = user_info['email']

        # Get the user's recently played tracks
        limit = 50
        
        # Get the user's recently played tracks
        results = sp.current_user_recently_played(limit=50)
        
        
        # Define the CSV file path
        csv_file = 'data/track_data.csv'
        
        # Check if the CSV file exists
        file_exists = os.path.exists(csv_file)


        # Extract the track information from the API response
        recent_tracks = []
     
        for item in results['items']:
            track = item['track']
            tid = uris.loc[uris['uri'] == track['id'], 'track_id'].values
            if len(tid) == 0:
                tid = np.nan
                lfm_pop = np.nan
                lfm_int = np.nan
            else:
                tid = tid[0]
                pop = track_popularities[track_popularities['track_id']==tid]
                lfm_pop = pop['popularity'].item()
                lfm_int = pop['interactions'].item()
            
            recent_tracks.append({
                'name': track['name'],
                'artist': track['artists'][0]['name'],
                'album': track['album']['name'],
                'image': track['album']['images'][0]['url'],
                'uri': track['uri'],
                'sp_popularity':track['popularity'],
                'track_id':tid,
                'lfm_popularity':lfm_pop,
                'lfm_interactions':lfm_int
                
            })
     
        # Get the user's short-term top tracks
        short_term_tracks = []
        results = sp.current_user_top_tracks(time_range='short_term', limit=limit)
        for item in results['items']:
            track = item
            tid = uris.loc[uris['uri'] == track['id'], 'track_id'].values
            if len(tid) == 0:
                tid = np.nan
                lfm_pop = np.nan
                lfm_int = np.nan
            else:
                tid = tid[0]
                pop = track_popularities[track_popularities['track_id']==tid]
                lfm_pop = pop['popularity'].item()
                lfm_int = pop['interactions'].item()
            short_term_tracks.append({
                'name': track['name'],
                'artist': track['artists'][0]['name'],
                'album': track['album']['name'],
                'image': track['album']['images'][0]['url'],
                'uri': track['uri'],
                'sp_popularity':track['popularity'],
                'track_id':tid,
                'lfm_popularity':lfm_pop,
                'lfm_interactions':lfm_int
            })
            
        # Get the user's medium-term top tracks
        medium_term_tracks = []
        results = sp.current_user_top_tracks(time_range='medium_term', limit=limit)
        for item in results['items']:
            track = item
            tid = uris.loc[uris['uri'] == track['id'], 'track_id'].values
            if len(tid) == 0:
                tid = np.nan
                lfm_pop = np.nan
                lfm_int = np.nan
            else:
                tid = tid[0]
                pop = track_popularities[track_popularities['track_id']==tid]
                lfm_pop = pop['popularity'].item()
                lfm_int = pop['interactions'].item()
            medium_term_tracks.append({
                'name': track['name'],
                'artist': track['artists'][0]['name'],
                'album': track['album']['name'],
                'image': track['album']['images'][0]['url'],
                'uri': track['uri'],
                'sp_popularity':track['popularity'],
                'track_id':tid,
                'lfm_popularity':lfm_pop,
                'lfm_interactions':lfm_int
            })

        # Get the user's long-term top tracks
        long_term_tracks = []
        results = sp.current_user_top_tracks(time_range='long_term', limit=limit)
        for item in results['items']:
            track = item
            tid = uris.loc[uris['uri'] == track['id'], 'track_id'].values
            if len(tid) == 0:
                tid = np.nan
                lfm_pop = np.nan
                lfm_int = np.nan
            else:
                tid = tid[0]
                pop = track_popularities[track_popularities['track_id']==tid]
                lfm_pop = pop['popularity'].item()
                lfm_int = pop['interactions'].item()
            long_term_tracks.append({
                'name': track['name'],
                'artist': track['artists'][0]['name'],
                'album': track['album']['name'],
                'image': track['album']['images'][0]['url'],
                'uri': track['uri'],
                'sp_popularity':track['popularity'],
                'track_id':tid,
                'lfm_popularity':lfm_pop,
                'lfm_interactions':lfm_int
            })

        session['start_time'] = time.time()

        # Combine all tracks into a single list
        all_tracks = [('recent', recent_tracks),
                      ('short_term', short_term_tracks),
                      ('medium_term', medium_term_tracks),
                      ('long_term', long_term_tracks)]
        
        # Write the track information to the CSV file
        with open(csv_file, 'a', newline='', encoding='utf-8') as file:
            writer = csv.writer(file)
            
            # If the file doesn't exist, write the header row
            if not file_exists:
                writer.writerow(['User','Type', 'Track', 'Artist', 'Album', 'Image', 'URI', 'TrackID', 'LFMPopularity', 'LFMInteractions', 'SpotifyPopularity'])
            
            # Write the track information for each type
            for track_type, tracks in all_tracks:
                for track in tracks:
                    writer.writerow([user_email, track_type, track['name'], track['artist'], track['album'], track['image'], track['uri'], track['track_id'], track['lfm_popularity'], track['lfm_interactions'], track['sp_popularity']])

        return render_template('played_tracks.html', recent_tracks=recent_tracks, short_term_top_tracks=short_term_tracks, medium_term_top_tracks=medium_term_tracks, long_term_top_tracks=long_term_tracks)

    return "Error: Unable to authenticate with Spotify."

@app.route('/logout')
def logout():
    # Retrieve the user ID from the session
    user_id = session.get('user_id')

    # Check if the user ID exists
    if user_id:
        # Get the current time and date
        current_time = datetime.datetime.now().strftime('%H:%M:%S')
        current_date = datetime.datetime.now().strftime('%Y-%m-%d')

        # Calculate the session duration
        if 'start_time' in session:
            start_time = session['start_time']
            end_time = time.time()
            session_duration = end_time - start_time

            # Save the data to the CSV file
            with open('data/interaction_data.csv', 'a', newline='', encoding='utf-8') as csvfile:
                writer = csv.writer(csvfile)
                writer.writerow([user_id, current_date, current_time, start_time, end_time, session_duration])
    
    # Clear the session by removing all session variables
    session.clear()
    
    # Revoke the access token by making a request to the Spotify API
    if 'access_token' in session:
        access_token = session['access_token']
        sp = spotipy.Spotify(auth=access_token)
        sp.auth_manager.revoke_access_token(access_token)

    # Redirect the user back to the login page
    return redirect(url_for('index'))


if __name__ == '__main__':
    
    # Open the browser automatically when the app starts
    webbrowser.open('http://localhost:8888/')
    
    app.run(port=8888)
    
    

