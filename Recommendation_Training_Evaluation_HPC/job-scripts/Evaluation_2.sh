#!/bin/bash

#SBATCH --time=3:00:00
#SBATCH --partition=thin
#SBATCH --nodes 1
#SBATCH --ntasks=3
#SBATCH --cpus-per-task=38


#SBATCH --mail-type=BEGIN,END,TIME_LIMIT,TIME_LIMIT_90,FAIL
#SBATCH --mail-user=r.ungruh@students.uu.nl

module load 2022
module load Python/3.10.4-GCCcore-11.3.0

cp -r $HOME/Thesis_1_Recommender/Recommenders-HPC/data/EvaluationSpotify "$TMPDIR"
cp -r $HOME/Thesis_1_Recommender/Recommenders-HPC/Models "$TMPDIR"

mkdir "$TMPDIR"/output_dir

for i in `seq 11 13`; do
  python $HOME/Thesis_1_Recommender/Recommenders-HPC/scripts/Evaluator.py "$TMPDIR"/EvaluationSpotify "$TMPDIR"/output_dir "$TMPDIR"/Models 1 "$i" &
done
wait


cp -r "$TMPDIR"/output_dir $HOME/Thesis_1_Recommender/Recommenders-HPC/Evaluation