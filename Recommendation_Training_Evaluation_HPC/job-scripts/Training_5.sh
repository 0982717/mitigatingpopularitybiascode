#!/bin/bash

#SBATCH --time=2:00:00
#SBATCH --partition=thin
#SBATCH --nodes 1
#SBATCH --ntasks=2
#SBATCH --cpus-per-task=32


#SBATCH --mail-type=BEGIN,END,TIME_LIMIT,TIME_LIMIT_90,FAIL
#SBATCH --mail-user=r.ungruh@students.uu.nl

module load 2022
module load Python/3.10.4-GCCcore-11.3.0

cp -r $HOME/Thesis_1_Recommender/Recommenders-HPC/data/TrainingSpotify "$TMPDIR"
#cp -r $HOME/Thesis_1_Recommender/Recommenders-HPC/Models "$TMPDIR"

mkdir "$TMPDIR"/output_dir


for i in `seq 13 14`; do
  python $HOME/Thesis_1_Recommender/Recommenders-HPC/scripts/Training.py "$TMPDIR"/TrainingSpotify "$TMPDIR"/output_dir "" "$i" &
done
wait


cp -r "$TMPDIR"/output_dir $HOME/Thesis_1_Recommender/Recommenders-HPC/Models