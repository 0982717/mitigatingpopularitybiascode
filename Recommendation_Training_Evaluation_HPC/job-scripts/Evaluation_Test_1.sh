#!/bin/bash

#SBATCH --time=2:00:00
#SBATCH --partition=thin
#SBATCH --nodes 1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=32


#SBATCH --mail-type=BEGIN,END
#SBATCH --mail-user=r.ungruh@students.uu.nl

module load 2022
module load Python/3.10.4-GCCcore-11.3.0

cp -r $HOME/Thesis_1_Recommender/Recommenders-HPC/data/EvaluationSpotify "$TMPDIR"

mkdir "$TMPDIR"/output_dir

python $HOME/Thesis_1_Recommender/Recommenders-HPC/scripts/Evaluator.py "$TMPDIR"/EvaluationSpotify "$TMPDIR"/output_dir 0 0

cp -r "$TMPDIR"/output_dir $HOME/Thesis_1_Recommender/Recommenders-HPC/Evaluation