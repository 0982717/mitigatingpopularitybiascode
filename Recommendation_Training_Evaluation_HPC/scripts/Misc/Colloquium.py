

'''
import os
abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)


import pandas as pd
import main

import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np

sns.set(font=main.get_font())
customPalette  = sns.set_palette(main.get_color_palette(), color_codes=True)
cm = main.get_color_mapping()




def analyse_track_popularity(dataPath = main.trackPopularityDataPath, savePath = ('Data_Analysis/' + 'popularity_distribution_reduced.png'),
                             logTitle='Distribution of user-artist Interactions by Item Rank on subset'):
    # Load the created dataset
    print("Loading the track popularity dataset...")
    track_popularity = pd.read_csv(dataPath)
    print("Dataset loaded successfully.")
    
    track_popularity['interactions'] = np.log10(track_popularity['interactions'])
    
    
    # Sort the tracks based on the number of interactions
    print("Sorting the tracks based on the number of interactions...")
    track_popularity = track_popularity.sort_values('interactions', ascending=False).reset_index(drop=True)
    
    # Calculate the total number of interactions
    print("Calculating the total number of interactions...")
    total_interactions = track_popularity['interactions'].sum()
    
    # Calculate the number of interactions corresponding to 20% of the total interactions
    print("Calculating the number of interactions corresponding to 20% of the total interactions...")
    interaction_cutoff = total_interactions * 0.2
    
    # Find the index where the cumulative sum exceeds the interaction cutoff
    print("Finding the index where the cumulative sum exceeds the interaction cutoff...")
    head_index = track_popularity['interactions'].cumsum().ge(interaction_cutoff).idxmax()
    
    # Find the index where the cumulative sum reaches or exceeds the interaction cutoff
    print("Finding the index where the cumulative sum reaches or exceeds the interaction cutoff...")
    tail_index = track_popularity['interactions'].cumsum().ge(total_interactions - interaction_cutoff).idxmax()
    
    
    num_head_items = head_index + 1
    num_tail_items = len(track_popularity) - tail_index
    num_mid_items = len(track_popularity) - num_head_items - num_tail_items

    

    track_popularity['pop'] = (track_popularity['interactions'] / total_interactions) * 100
    
    # Step 10: Plot the distribution using Seaborn
    print('Plot the distribution using Seaborn...')
    plt.figure(figsize=(12, 6))
    plt.ylim(0,0.004)
    ax = sns.lineplot(x=track_popularity.index, y='pop', data=track_popularity, color=cm['gray'])
    ax.set_xlabel('Item Rank')
    ax.set_ylabel('Popularity (%)')
    ax.set(xticklabels=[])
    
    
    # Add vertical dotted lines for head, mid, and tail items
    ax.axvline(x=head_index, color=cm['b'], linestyle='--', label='Head')
    ax.axvline(x=tail_index, color=cm['r'], linestyle='--', label='Tail')
    
    
    # Add symbols for head, mid, and tail items on top of the graph
    ax.text((0.5 * head_index), track_popularity['pop'].max(), 'H', color=cm['b'], alpha=0.3, fontsize=12, ha='center')
    ax.text((head_index + tail_index) / 2, track_popularity['pop'].max(), 'M', color=cm['g'], alpha=0.3, fontsize=12, ha='center')
    ax.text((tail_index + track_popularity.index[-1]) / 2, track_popularity['pop'].max(), 'T', alpha=0.3, color=cm['r'], fontsize=12, ha='center')
    
    ax.legend()
    
    print("Saving the plot as an image file...")
    # Save the plot as an image file
    plot_path = main.plotPath + savePath
    plt.savefig(plot_path)  # Provide the desired file name and extension
    plt.show()
    
    

        
analyse_track_popularity(dataPath = main.trackPopularityDataPathTesting, savePath = ('Colloquium_item_centred_mitigation.png'),
                             logTitle='Distribution of user-artist Interactions by Item Rank on subset')        
'''



import os
import pandas as pd
import main

import seaborn as sns
import matplotlib.pyplot as plt

sns.set(font=main.get_font())
customPalette  = sns.set_palette(main.get_color_palette(), color_codes=True)
cm = main.get_color_mapping()



abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)

def analyse_user_profiles(dataPath = main.userProfileDataPath, savePath= ('Data_Analysis/' + 'user_group_profiles.png'),
                          logTitle='Distribution of popularity groups in user profiles by user group in subset'):

    
    # Calculate the average ratio of tail, mid, and head items for each user group
    print("Calculating the average ratio of tail, mid, and head items for each user group...")
    user_types = pd.DataFrame({'user_type':['User Profile', 'Recommendations'], 
                                   'tail_ratio':[0.2, 0.2], 
                                   'mid_ratio':[0.6, 0.6], 
                                   'head_ratio':[0.2, 0.2]})
    
    # Set up the figure and axis
    fig, ax = plt.subplots(figsize=(4, 6), constrained_layout=True)
    
    # Accumulate the values for each category
    user_types['mid_head'] = user_types['head_ratio'] + user_types['mid_ratio']
    user_types['tail_mid_head'] = user_types['mid_head'] + user_types['tail_ratio']
    
    
    # Create the stacked barplot with the specified colors
    print("Creating the stacked barplot...")
    sns.barplot(x='user_type', y='tail_mid_head', data=user_types, ax=ax, label='Tail', color=cm['r'], errorbar=None)
    sns.barplot(x='user_type', y='mid_head', data=user_types, ax=ax, label='Mid', color=cm['g'], errorbar=None)
    sns.barplot(x='user_type', y='head_ratio', data=user_types, ax=ax, label='Head', color=cm['b'], errorbar=None)
    
    
    
    
    ax.set_xlabel("")
    ax.set_ylabel("Ratio")
    ax.legend(title="Popularity")
    sns.move_legend(
        ax, "lower center",
        bbox_to_anchor=(.5, 1), ncol=3, title=None, frameon=False,
    )
    
    # Save the plot as an image file
    print("Saving the plot as an image file...")
    plot_path = main.plotPath + savePath
    plt.savefig(plot_path)  # Provide the desired file name and extension
    plt.show()
    
    
analyse_user_profiles(dataPath = main.userProfileDataPath, savePath= ('Colloquium_user_centred_fairness_fair'))