# -*- coding: utf-8 -*-
"""
Created on Sun Jun 11 09:21:51 2023

@author: unrob
"""
// P step: update user vectors
            DenseVector q_tilde = new VectorBasedDenseVector(numFactors);
            DenseMatrix A_tilde = new DenseMatrix(numFactors, numFactors);

            for (int j = 0; j < numItems; j++) {
                DenseVector qj = itemFactors.row(j);
                double sj = importance_vector.get(j);
                q_tilde = q_tilde.plus(qj.times(sj));
                A_tilde = A_tilde.plus(qj.outer(qj).times(sj));
            }

            List<Integer> cus = nonEmptyRows(trainMatrix); // list of users with$c_ui=1$
            double user_loss = 0;
            for (int u : cus) {
                // for each user
                DenseMatrix A_bar = new DenseMatrix(numFactors, numFactors);
                DenseVector q_bar = new VectorBasedDenseVector(numFactors);
                DenseVector b_bar = new VectorBasedDenseVector(numFactors);
                DenseVector b_tilde = new VectorBasedDenseVector(numFactors);

                SequentialSparseVector Ru = trainMatrix.row(u);
                double I_bar = Ru.getNumEntries();
                double r_tilde = 0, r_bar = 0;

                for (VectorEntry ve : Ru) {
                    int i = ve.index();
                    double rui = ve.get();
                    // double cui = 1;
                    DenseVector qi = itemFactors.row(i);

                    A_bar = A_bar.plus(qi.outer(qi));
                    q_bar = q_bar.plus(qi);
                    b_bar = b_bar.plus(qi.times(rui));

                    // ratings of unrated items will be 0
                    double si = importance_vector.get(i);
                    r_tilde += si * rui;
                    r_bar += rui;
                    b_tilde = b_tilde.plus(qi.times(si * rui));
                }

                DenseMatrix M = A_bar.times(weight_sum).minus(q_bar.outer(q_tilde)).minus(q_tilde.outer(q_bar))
                        .plus(A_tilde.times(I_bar));

                DenseVector y = b_bar.times(weight_sum).minus(q_bar.times(r_tilde)).minus(q_tilde.times(r_bar))
                        .plus(b_tilde.times(I_bar));

                DenseVector pu = M.inverse().times(y);
                user_loss += y.getLengthSquared();
                userFactors.row(u).assign((index, value) -> {
                    return pu.get(index);
                });
            }
            String info = "RankALS iter " + iter + ": sq. user loss = " + user_loss;
            LOG.info(info);
            
            
// Q step: update item vectors
            Map<Integer, Double> r_tilde = new HashMap<>();
            Map<Integer, Double> r_bar = new HashMap<>();
            Map<Integer, Double> diag_z = new HashMap<>();
            Map<Integer, DenseVector> Q_bar = new HashMap<>();

            for (int u : cus) {
                SequentialSparseVector Ru = trainMatrix.row(u);

                double ru_tilde = 0, r_bar = 0, diag_z_u = Ru.getNumEntries();
                DenseVector Qu_bar = new VectorBasedDenseVector(numFactors);

                for (VectorEntry ve : Ru) {
                    int j = ve.index();
                    double ruj = ve.get();
                    double sj = supportVector.get(j);

                    ru_tilde += sj * ruj;
                    r_bar += ruj;
                    Qu_bar = Qu_bar.plus(itemFactors.row(j));
                }

                r_tilde.put(u, ru_tilde);
                r_bar.put(u, ru_bar);
                diag_z.put(u, diag_z_u);
                Q_bar.put(u, Qu_bar);
            }

            for (int i = 0; i < numItems; i++) {
                // for each item
                DenseMatrix A_bar = new DenseMatrix(numFactors, numFactors);
                DenseMatrix A_bar_bar = new DenseMatrix(numFactors, numFactors);
                DenseVector p_2_bar_bar = new VectorBasedDenseVector(numFactors);
                DenseVector b_bar = new VectorBasedDenseVector(numFactors);
                DenseVector p_1_bar_bar = new VectorBasedDenseVector(numFactors);
                DenseVector p_3_bar_bar = new VectorBasedDenseVector(numFactors);
                DenseVector b_bar_bar = new VectorBasedDenseVector(numFactors);

                double si = supportVector.get(i);
                DenseVector itemVector = new VectorBasedDenseVector(trainMatrix.column(i));

                for (int u : cus) {
                    DenseVector pu = userFactors.row(u);
                    // double rui = trainMatrix.get(u, i);
                    double rui = itemVector.get(u);

                    DenseMatrix pp = pu.outer(pu);
                    A_bar = A_bar.plus(pp);
                    p_2_bar_bar = p_2_bar_bar.plus(pp.times(Q_bar.get(u)));
                    A_bar_bar = A_bar_bar.plus(pp.times(diag_z.get(u)));
                    p_3_bar_bar = p_3_bar_bar.plus(pu.times(r_bar.get(u)));

                    if (rui > 0) {
                        b_bar = b_bar.plus(pu.times(rui));
                        p_1_bar_bar = p_1_bar_bar.plus(pu.times(r_tilde.get(u)));
                        b_bar_bar = b_bar_bar.plus(pu.times(rui * diag_z.get(u)));
                    }
                }
                DenseMatrix subtract = A_bar.times(si + 1);
                DenseMatrix M = A_bar.times(weight_sum).plus(A_bar_bar.times(si)).minus(subtract);
                DenseVector y = A_bar.times(q_tilde).plus(b_bar.times(weight_sum)).minus(p_1_bar_bar)
                        .plus(p_2_bar_bar.times(si)).minus(p_3_bar_bar.times(si)).plus(b_bar_bar.times(si));
                DenseVector qi = M.inverse().times(y.minus(subtract.times(itemFactors.row(i))));
                itemFactors.row(i).assign((index, value) -> {
                    return qi.get(index);
                });
            }
        }
    }