# -*- coding: utf-8 -*-
"""
Created on Thu Sep 28 15:55:25 2023

@author: unrob
"""

import sys
import os

import numpy as np
from Preprocessing import ALS_preprocessing as pre

import pandas as pd
import pickle
import csv
from scipy.sparse import save_npz, load_npz
from matplotlib import pyplot as plt
import seaborn as sns

# This script was created for analysing the difference in the distributions between recommendations and user profiles.
# For more context, refer to section 4.5.5 of the Thesis


input_directory = "C:/Users/unrob/OneDrive - Universiteit Utrecht/Thesis/Script Submissions/Recommenders - HPC_1/data/EvaluationSpotify"
model_directory = "C:/Users/unrob/OneDrive - Universiteit Utrecht/Thesis/Script Submissions/Recommenders - HPC_1/data/EvaluationReranking"
# Check if the input directory exists
if not os.path.isdir(input_directory):
    print("Input directory does not exist.")
    

trainPath = os.path.join(input_directory, "A_train.npz")
testPath = os.path.join(input_directory, "A_test.npz")
mappingPath = os.path.join(input_directory, "init_mappings.npz")

test, train, mappings = pre.load_train_and_test_matrix(dataPathTest = testPath, dataPathTrain = trainPath, dataPathMappings = mappingPath)
user_index_map, track_index_map = mappings


    
profilePath = input_directory + '/testset/'
trackDataPath = os.path.join(input_directory, "track_popularity.csv")
userDataPath = os.path.join(input_directory, "user_profiles.csv")


track_popularity = pd.read_csv(trackDataPath)
user_profiles = pd.read_csv(userDataPath)



def analyse_popularity_metric(recommendations=True, save=None):

    profile_popularity_ratios = pd.read_csv(os.path.join(model_directory,'profile_popularity_ratios.csv'))
    profile_average_popularity = pd.read_csv(os.path.join(model_directory,'profile_average_popularity.csv'))
    user_ids = load_from_1D_csv(os.path.join(model_directory, 'user_ids.csv'))
    user_idxs = load_from_1D_csv(os.path.join(model_directory, 'user_idxs.csv'))
    test = load_npz(os.path.join(model_directory, 'test.npz'))
    init_recommended_track_ids_list = load_from_2D_csv(os.path.join(model_directory, 'init_recommended_track_ids_list.csv'))
    init_recommended_track_idxs_list = load_from_2D_csv(os.path.join(model_directory, 'init_recommended_track_idxs_list.csv'))
    init_recommended_track_scores_list = load_from_2D_csv(os.path.join(model_directory, 'init_recommended_track_scores_list.csv'))
    

    profile_nonzero_idxs = [row.indices.tolist() for row in test]
    profile_nonzero_ids = [[track_index_map[idx] for idx in row] for row in profile_nonzero_idxs]
    profile_popularities = [list(track_popularity[track_popularity['track_id'].isin(tids)]['interactions']) for tids in profile_nonzero_ids]
    
    
    recommendation_popularities = [list(track_popularity[track_popularity['track_id'].isin(tids[:25])]['interactions']) for tids in init_recommended_track_ids_list]
    if recommendations:
        popularity_scores = recommendation_popularities
    else:
        popularity_scores = profile_popularities
    
    upper_limit = 3000
    popularity_ranges = list(range(0, upper_limit, 50))

    
    # Flatten the popularity_scores list
    flat_scores = [score for user_scores in popularity_scores for score in user_scores]
    print(max(flat_scores))
            
    row_means = [np.mean(scores) for scores in popularity_scores]
    row_medians = [np.median(scores) for scores in popularity_scores]
    
    # Calculate the average mean and average median
    average_mean = np.mean(row_means)
    average_median = np.mean(row_medians)
    
    # Define the indices for "Head," "Mid," and "Tail"
    tail_index = 56 
    head_index = 1390
    
    # Plotting
    plt.figure(figsize=(10, 6))  # Set the figure size
    
    # Create a histogram using Seaborn
    sns.histplot(flat_scores, bins=popularity_ranges, kde=True, alpha=0.5, color='#2D683A', label='Data Distribution', stat='probability')
    plt.axvline(average_mean, color='grey', linestyle='dotted', linewidth=2, label=f'Average Mean: {average_mean:.2f}', ymax=0.5)
    plt.axvline(average_median, color='grey', linestyle='dashed', linewidth=2, label=f'Average Median: {average_median:.2f}', ymax=0.5)
    
    # Add vertical dotted lines for "Head" and "Tail" items
    plt.axvline(x=head_index, color='#0047B3', linestyle='--', label='Head', alpha=0.5)
    plt.axvline(x=tail_index, color='#B30000', linestyle='--', label='Tail', alpha=0.5)
    
    # Add text labels for "Average Mean" and "Average Median"
    plt.text(average_mean+175, 0.095, f'Average Mean\n{average_mean:.2f}', color='black', fontsize=12, ha='center')
    plt.text(average_median-175, 0.095, f'Average Median\n{average_median:.2f}', color='black', fontsize=12, ha='center')
    
    plt.xlabel('Popularity (LFM metric)')
    plt.ylabel('Frequency')
    
    
    plt.xlim(0, upper_limit)  # Adjust the limits as needed
    plt.ylim(0, 0.2)
    # Show every 5th x-tick
    plt.xticks(popularity_ranges[::5])
    
    # Add labels for "Head," "Mid," and "Tail" items
    plt.text((head_index+ upper_limit) / 2, 0.19, 'H', color='#0047B3', fontsize=12, ha='center')
    plt.text((head_index + tail_index) / 2, 0.19, 'M', color='#2D683A', fontsize=12, ha='center')
    plt.text(tail_index / 2,  0.19, 'T', color='#B30000', fontsize=12, ha='center')
    
    
    plt.tight_layout()
    if save != None:
        plt.savefig(save)
    plt.show()
        
        

def save_as_2D_csv(data, file_path):
    with open(file_path, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerows(data)     
        
def save_as_1D_csv(data, file_path):
    with open(file_path, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(data)      
        
def load_from_2D_csv(file_path):
    with open(file_path, 'r') as csvfile:
        reader = csv.reader(csvfile)
        data = [[int(item) if item.isdigit() else float(item) for item in row] for row in reader]
    return data

def load_from_1D_csv(file_path):
    with open(file_path, 'r') as csvfile:
        reader = csv.reader(csvfile)
        data = [int(item) if item.isdigit() else float(item) for item in next(reader)]
    return data



analyse_popularity_metric(True, 'C:/Users/unrob/OneDrive - Universiteit Utrecht/Thesis/Figures/popularityMetricHistogramProfiles.png')