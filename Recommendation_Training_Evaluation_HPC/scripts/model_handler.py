import os


abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)

import pickle


import re

from Models.baselines import popularity_ranker, random_ranker
from Models.CF_standard import ALS
from Models.mitigation import rerank_CP, rerank_XQ, rerank_fair
from Models.RankALS import RankALS
from Models import baselines

import fairsearch as fsc

#test, train, mappings = pre.load_train_and_test_matrix(dataPathTest=main.savePathTestTesting, dataPathTrain=main.savePathTrainTesting, dataPathMappings=main.initSavePathMappingsTesting)
#user_index_map, track_index_map = mappings



def train_and_save_model(modelName, trainset, modelPath = "", iterations=15, factors=32):
    if modelName == 'Random':
        model = random_ranker()
    elif modelName == 'Popularity':
        model = popularity_ranker()
    elif modelName == 'ALS':
        model = ALS()
    elif re.match(r'^RankALS.*', modelName):
        model = RankALS(iterations=iterations, factors=factors)
    else:
        print('No model selected')
    
        
    print('Training model...' +  modelName)
    model.fit(trainset)
    
    savePath = os.path.join(modelPath, modelName + '.pkl')
    print('Model trained, saving now...')
    with open(savePath, "wb") as f:
        pickle.dump(model, f)
    print('Model saved.')
    
def load_and_retrain_model(initModelName, newModelName, trainset, modelPath = "", savePath="", iterations = 5):
    model = load_model(modelName=initModelName, modelPath=modelPath)
    model.fit(trainset, iterations = 5)
    savePathModel = os.path.join(savePath, newModelName + '.pkl')
    print('Model trained, saving now...')
    with open(savePathModel, "wb") as f:
        pickle.dump(model, f)
    print('Model saved.')
    
def load_model(modelName='', modelPath=""):
    if modelName == 'Random':
        model = random_ranker()
    elif modelName == 'Popularity':
        model = popularity_ranker()
    elif modelName == 'ALS':
        model = ALS()
    elif re.match(r'^RankALS.*', modelName):
        model = RankALS()
    else:
        print('No model selected')
        return None
    
    print('Loading model...')
    loadPath = os.path.join(modelPath, modelName + '.pkl')
    try:
        with open(loadPath, "rb") as f:
            model = pickle.load(f)
        print('Model loaded.')
        return model
    except FileNotFoundError:
        print('Model file not found.')
        return None
    
    
def predict(model, user_ids=[], user_idxs=[], user_items=[], N=10, user_index_map={}, track_index_map={}):
    if user_ids != []:
    
        user_idxs =  [idx for idx, user_id in user_index_map.items() if user_id in user_ids]
    
    if user_idxs != []:
        
        
        recommendations_idxs, scores = model.predict(user_idxs, N, user_items)
        recommendations_ids = [[track_index_map[index] for index in recommended_indices] for recommended_indices in recommendations_idxs]
        
        return recommendations_idxs, recommendations_ids, scores
    else:
        print('no fitting indices found')
        return [], [], []


def rerank(algorithm, initial_idxs, initial_ids, initial_scores, k=10, user_ids=[], user_profiles=None, track_popularities=None, delta=0, alpha_fair=0.1, p_fair=0.5):
    print(f'reranking now, with delta={delta}...')
    reranked_ids = []
    reranked_idxs = []
    reranked_scores = []
    if algorithm == 'FAIR':
        # create the Fair object 
        #f = fsc.Fair(k, p_fair, alpha_fair)
        

        # get alpha adjusted
        #alpha_adjusted = f.adjust_alpha()
        #print(f'adjusted alpha: {alpha_adjusted}')
    
        # create a new unadjusted mtable with the new alpha
        f_adjusted = fsc.Fair(k, p_fair, alpha_fair)
        
    for user_id, ids, idxs, scores in zip(user_ids, initial_ids, initial_idxs, initial_scores):
        
        scores = list(scores)
        user_profile = user_profiles[user_profiles['user_id']==user_id]
        
        if algorithm == 'XQ':
            reranked_list = rerank_XQ(ids[:], scores[:], track_popularities, user_profile, delta=delta, k=k)
            
        elif algorithm == 'CP':
           
            reranked_list = rerank_CP(ids[:], scores[:], track_popularities, user_profile, delta=delta, k=k)
            
        elif algorithm == 'FAIR':

            reranked_list = rerank_fair(ids[:], scores[:], track_popularities, f_adjusted)
            
        reranked_ids.append(reranked_list)
        #Rerank the initial_idxs and initial_scores based on the sorted_ids
        
        reranked_idxs.append([idxs[ids.index(id_)] for id_ in reranked_list])
        reranked_scores.append([scores[ids.index(id_)] for id_ in reranked_list])
        
    print('reranked!')
    
    return reranked_ids, reranked_idxs, reranked_scores

#test, train, mappings = pre.load_train_and_test_matrix(dataPathTest = main.savePathTestTesting, dataPathTrain = main.savePathTrainTesting, dataPathMappings = main.initSavePathMappingsTesting)
#user_index_map, track_index_map = mappings

#train_and_save_model('RankALS1', train, modelPath=main.modelPathTesting)
#track_popularity = pd.read_csv(main.trackPopularityDataPathTesting)
#user_profiles = pd.read_csv(main.userProfileDataPathTesting)

#test_user_idxs = [8]
#test_user_ids = [user_index_map[idxs] for idxs in test_user_idxs]

#initial_recommendations_idxs, initial_recommendations_ids, initial_scores = predict(model, user_ids=test_user_ids, N=10, user_index_map=user_index_map, track_index_map=track_index_map)

#print(initial_recommendations_ids)
#reranked_ids = rerank(algorithm='CP', initial_idxs=initial_recommendations_idxs, initial_ids=initial_recommendations_ids, initial_scores=initial_scores, k=5, user_ids=test_user_ids,
#                      user_profiles=user_profiles, track_popularities=track_popularity, delta=0.99)
#
#
#print(user_profiles[user_profiles['user_id'].isin(test_user_ids)][['head_ratio', 'mid_ratio', 'tail_ratio']])