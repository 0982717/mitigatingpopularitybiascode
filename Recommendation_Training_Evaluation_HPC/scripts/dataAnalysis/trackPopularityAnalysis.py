# -*- coding: utf-8 -*-
"""
Created on Wed May 17 09:19:08 2023

@author: unrob
"""
import os
abspath = os.path.abspath(__file__)

import pandas as pd
from scripts import main

import seaborn as sns
import matplotlib.pyplot as plt

sns.set(font=main.get_font())
customPalette  = sns.set_palette(main.get_color_palette(), color_codes=True)
cm = main.get_color_mapping()




def analyse_track_popularity(dataPath = main.trackPopularityDataPath, savePath = ('Data_Analysis/' + 'popularity_distribution_reduced.png'),
                             logTitle='Distribution of user-artist Interactions by Item Rank on subset'):
    # Load the created dataset
    print("Loading the track popularity dataset...")
    track_popularity = pd.read_csv(dataPath)
    print("Dataset loaded successfully.")
        
    # Sort the tracks based on the number of interactions
    print("Sorting the tracks based on the number of interactions...")
    track_popularity = track_popularity.sort_values('interactions', ascending=False).reset_index(drop=True)
    
    # Calculate the total number of interactions
    print("Calculating the total number of interactions...")
    total_interactions = track_popularity['interactions'].sum()
    
    # Calculate the number of interactions corresponding to 20% of the total interactions
    print("Calculating the number of interactions corresponding to 20% of the total interactions...")
    interaction_cutoff = total_interactions * 0.2
    
    # Find the index where the cumulative sum exceeds the interaction cutoff
    print("Finding the index where the cumulative sum exceeds the interaction cutoff...")
    head_index = track_popularity['interactions'].cumsum().ge(interaction_cutoff).idxmax()
    
    # Find the index where the cumulative sum reaches or exceeds the interaction cutoff
    print("Finding the index where the cumulative sum reaches or exceeds the interaction cutoff...")
    tail_index = track_popularity['interactions'].cumsum().ge(total_interactions - interaction_cutoff).idxmax()
    
    
    num_head_items = head_index + 1
    num_tail_items = len(track_popularity) - tail_index
    num_mid_items = len(track_popularity) - num_head_items - num_tail_items
    
    total_items = len(track_popularity)
    percentage_head_items = (num_head_items / total_items) * 100
    percentage_tail_items = (num_tail_items / total_items) * 100
    percentage_mid_items = (num_mid_items / total_items) * 100
    
    
    print(f"Number of Head Items:, {num_head_items:.4f}")
    print(f"Number of Tail Items: {num_tail_items:.4f}")
    print(f"Number of Mid Items: {num_mid_items:.4f}")
    
    print(f"Percentage of Head Items: {percentage_head_items:.4f}")
    print(f"Percentage of Tail Items: {percentage_tail_items:.4f}")
    print(f"Percentage of Mid Items: {percentage_mid_items:.4f}")
    
    track_popularity['pop'] = (track_popularity['interactions'] / total_interactions) * 100
    
    # Step 10: Plot the distribution using Seaborn
    print('Plot the distribution using Seaborn...')
    plt.figure(figsize=(12, 6))
    ax = sns.lineplot(x=track_popularity.index, y='pop', data=track_popularity, color=cm['gray'])
    ax.set_xlabel('Item Rank')
    ax.set_ylabel('Popularity (%)')
    ax.set(xticklabels=[])
    
    
    # Add vertical dotted lines for head, mid, and tail items
    ax.axvline(x=head_index, color=cm['b'], linestyle='--', label='Head')
    ax.axvline(x=tail_index, color=cm['r'], linestyle='--', label='Tail')
    
    
    # Add symbols for head, mid, and tail items on top of the graph
    ax.text((0.5 * head_index), track_popularity['pop'].max(), 'H', color=cm['b'], alpha=0.3, fontsize=12, ha='center')
    ax.text((head_index + tail_index) / 2, track_popularity['pop'].max(), 'M', color=cm['g'], alpha=0.3, fontsize=12, ha='center')
    ax.text((tail_index + track_popularity.index[-1]) / 2, track_popularity['pop'].max(), 'T', alpha=0.3, color=cm['r'], fontsize=12, ha='center')
    
    ax.legend()
    
    print("Saving the plot as an image file...")
    # Save the plot as an image file
    plot_path = main.plotPath + savePath
    plt.savefig(plot_path)  # Provide the desired file name and extension
    plt.show()
    
    
    #create Log
    if False:
        main.write_to_log(script_name=abspath, title = logTitle, dataSet=dataPath,
                          notes=f'Test of subset dataset.\n'\
                          f'# Tail: {num_tail_items}, Mid: {num_mid_items}, Head: {num_head_items}\n'\
                              f'% Tail: {percentage_tail_items:.4f}, Mid: {percentage_mid_items:.4f}, Head: {percentage_head_items:.4f}',
                              plot_path=plot_path)
        
analyse_track_popularity(dataPath = main.trackPopularityDataPathTesting, savePath = ('Data_Analysis/' + 'popularity_distribution_example.png'),
                             logTitle='Distribution of user-artist Interactions by Item Rank on subset')        