# -*- coding: utf-8 -*-
"""
Created on Tue Jul  4 14:26:36 2023

@author: unrob
"""

import os
import scipy.sparse
import numpy as np
from scipy.sparse import csr_matrix, lil_matrix

abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)

from model_handler import load_model, predict

RankALS = load_model("RankALS", "../Models/base")

user_ids=['test']
user_items = lil_matrix((len(user_ids), RankALS.model.item_factors.shape[0]))

# Set 100 entries to 1 in each row
for row in range(user_items.shape[0]):
    indices = np.random.choice(user_items.shape[1], 100, replace=False)
    user_items[row, indices] = 1

# Convert LIL matrix to CSR format
user_items = user_items.tocsr()

mapping = RankALS.model.partial_fit_users(user_ids, user_items)

mappings = np.load("../data/EvaluationSpotify/init_mappings.npz", allow_pickle=True)
dec_mappings = [mappings['user_index_map_inv'].item(), mappings['track_index_map_inv'].item()]
user_index_map, track_index_map = dec_mappings


recommendations_idxs, recommendations_ids, scores = predict(RankALS, user_ids=user_ids, user_items=user_items, N=10, user_index_map=mapping, track_index_map=track_index_map)

print(recommendations_idxs)