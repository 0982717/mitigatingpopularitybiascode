# -*- coding: utf-8 -*-
"""
Created on Thu May 18 14:38:42 2023

@author: unrob
"""



import numpy as np

class popularity_ranker:
    def __init__(self):
        self.track_indices = None
        
    def fit(self, user_item_matrix):
        track_popularity = np.sum(user_item_matrix, axis=0)
        track_indices = np.argsort(-track_popularity)
        self.track_indices = track_indices
        
    def predict(self, user_idxs, n):
        #if self.track_indices == None:
        #    print('Please fit the model first to your dataset')

        return [self.track_indices[0,:n].tolist()[0] for _ in range(len(user_idxs))], []



class random_ranker:
    def __init__(self):
        self.num_songs = None
        
    def fit(self, user_item_matrix):
        self.num_songs = user_item_matrix.shape[1]
        
    def predict(self, user_idxs, n):
        #if self.num_songs == None:
        #    print('Please fit the model first to your dataset')
        if n > self.num_songs:
            raise ValueError("n cannot be greater than the total number of songs.")
        track_indices = []
        for i in range(len(user_idxs)):
            track_indices.append(list(np.random.choice(self.num_songs, n, replace=False)))
           
        return track_indices, []
    
