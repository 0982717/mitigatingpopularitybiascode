This script includes the necessary data and scripts for training and evaluating the models using the Snellius computer
After gaining access to Snellius, this directory can be copied to MobaXTerm and the job-scripts can be called. To test and evaluate the models further, adapt the subset_evaluator.py and Training.py scripts. 


Remark: Please add the working models and data to the correct paths.