
import os
import sys

current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(current_dir)

import pickle
import gzip
from Models.RankALS import RankALS

def compress_model(modelName='RankALSmin'):
    modelPath = "../Models"  # Update this with the actual path to your model files
    loadPath = os.path.join(modelPath, modelName + '.pkl')
    compressedPath = os.path.join(modelPath, modelName + '.pkl.gz')

    try:
        # Load the model
        model = RankALS()
        print('Loading model:', modelName)

        with open(loadPath, "rb") as f:
            model = pickle.load(f)

        # Compress the model
        print('Compressing model...')

        with gzip.open(compressedPath, "wb") as f:
            pickle.dump(model, f, protocol=pickle.HIGHEST_PROTOCOL)

        print('Model compressed.')
    except Exception as e:
        print('Error occurred:', e)
        return

# Call the function to compress the model file
compress_model()
