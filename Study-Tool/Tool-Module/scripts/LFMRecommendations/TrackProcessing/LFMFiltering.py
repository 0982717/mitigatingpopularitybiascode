# -*- coding: utf-8 -*-
"""
Created on Tue Jul  4 17:34:12 2023

@author: unrob
"""

def get_valid_spids(spids, spid_ids):
    all_valids = set(spid_ids['uri'])
    valid_spids = list(set(spids) & all_valids)
    return valid_spids