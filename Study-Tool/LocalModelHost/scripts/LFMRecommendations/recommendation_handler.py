# -*- coding: utf-8 -*-
"""
Created on Tue Jul  4 17:41:09 2023

@author: unrob
"""

import pandas as pd
import numpy as np
from scipy.sparse import lil_matrix



from LFMRecommendations.Recommending.model_handler import load_model
from LFMRecommendations.Recommending.Recommender import predict


# Set display option to show all columns
pd.set_option('display.max_columns', None)

N = 5000
k = 25
p_fair = 0.98
lambd = 0.99


def get_base_recommendations(user_id, initial_track_ids):
    mappings = np.load("../data/mappings.npz", allow_pickle=True)
    dec_mappings = [mappings['user_index_map_inv'].item(), mappings['track_index_map_inv'].item()]
    user_index_map, track_index_map = dec_mappings
    
    del user_index_map

    model = load_model()
    
 
    user_items = lil_matrix((1, model.model.item_factors.shape[0]))
    track_index_map_reverse = {value: key for key, value in track_index_map.items()}
    #fill user_items

    for tid in initial_track_ids:
        tidx =  track_index_map_reverse[tid]

        user_items[0,tidx] = 1
        
    # Convert LIL matrix to CSR format
    user_items = user_items.tocsr()
    
    
    
    user_map = model.model.partial_fit_users([user_id], user_items)
    print('Preprocessed user data')
    
    recommendations_idxs, recommendations_ids, scores = predict(model, user_ids=[user_id], user_items=user_items, N=N, user_index_map=user_map, track_index_map=track_index_map)
    
    return recommendations_idxs, recommendations_ids, scores

