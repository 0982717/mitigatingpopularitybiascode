# -*- coding: utf-8 -*-
"""
Created on Tue Jul  4 17:16:06 2023

@author: unrob
"""

    
def predict(model, user_ids=[], user_idxs=[], user_items=[], N=10, user_index_map={}, track_index_map={}):
    if user_ids != []:
    
        user_idxs =  [idx for idx, user_id in user_index_map.items() if user_id in user_ids]
    
    if user_idxs != []:
        
        
        recommendations_idxs, scores = model.predict(user_idxs, N, user_items)
        recommendations_ids = [[track_index_map[index] for index in recommended_indices] for recommended_indices in recommendations_idxs]
        
        return recommendations_idxs, recommendations_ids, scores
    else:
        print('no fitting indices found')
        return [], [], []
