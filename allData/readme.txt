Preprocessed and raw data.

- raw: the raw dataset LFM-2b
- complete: the filtered subset including also items with no available spotify_url
- spotify: the filtered subset only including items with available spotify_url
- Testing: reduced test set only including a few users and items for testing purposes
