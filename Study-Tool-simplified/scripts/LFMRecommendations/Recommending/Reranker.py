# -*- coding: utf-8 -*-
"""
Created on Tue Jul  4 17:17:05 2023

@author: unrob
"""
import LFMRecommendations.fairsearch as fsc
from LFMRecommendations.Models.mitigation import rerank_CP, rerank_XQ, rerank_fair

def rerank(algorithm, initial_idxs, initial_ids, initial_scores, k=10, user_ids=[], user_profiles=None, track_popularities=None, delta=0, alpha_fair=0.1, p_fair=0.5):
    print(f'reranking now, with delta={delta}...')
    reranked_ids = []
    reranked_idxs = []
    reranked_scores = []
    if algorithm == 'FAIR':
        # create the Fair object 
        #f = fsc.Fair(k, p_fair, alpha_fair)
        

        # get alpha adjusted
        #alpha_adjusted = f.adjust_alpha()
        #print(f'adjusted alpha: {alpha_adjusted}')
    
        # create a new unadjusted mtable with the new alpha
        f_adjusted = fsc.Fair(k, p_fair, alpha_fair)
        
    for user_id, ids, idxs, scores in zip(user_ids, initial_ids, initial_idxs, initial_scores):
        
        scores = list(scores)
        user_profile = user_profiles[user_profiles['user_id']==user_id]
        
        if algorithm == 'XQ':
            reranked_list = rerank_XQ(ids[:], scores[:], track_popularities, user_profile, delta=delta, k=k)
            
        elif algorithm == 'CP':
           
            reranked_list = rerank_CP(ids[:], scores[:], track_popularities, user_profile, delta=delta, k=k)
            
        elif algorithm == 'FAIR':

            reranked_list = rerank_fair(ids[:], scores[:], track_popularities, f_adjusted)
            
        reranked_ids.append(reranked_list)
        #Rerank the initial_idxs and initial_scores based on the sorted_ids
        
        reranked_idxs.append([idxs[ids.index(id_)] for id_ in reranked_list])
        reranked_scores.append([scores[ids.index(id_)] for id_ in reranked_list])
        
    print('reranked!')
    
    return reranked_ids, reranked_idxs, reranked_scores