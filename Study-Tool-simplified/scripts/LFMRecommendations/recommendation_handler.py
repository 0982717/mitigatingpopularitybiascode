# -*- coding: utf-8 -*-
"""
Created on Tue Jul  4 17:41:09 2023

@author: unrob
"""
import os
import pandas as pd
import numpy as np
from scipy.sparse import lil_matrix
from datetime import datetime
import random


from LFMRecommendations.TrackProcessing.URIprocessing import spids_to_ids, ids_to_spids, uris_to_spids
from LFMRecommendations.TrackProcessing.LFMFiltering import get_valid_spids
from LFMRecommendations.Recommending.model_handler import load_model
from LFMRecommendations.Recommending.Recommender import predict
from LFMRecommendations.Recommending.Reranker import rerank
from LFMRecommendations.UserProcessing.profileCreation import create_user_profile
#from DataAnalysis.Statistics import compute_recommendation_statistics
from LFMRecommendations.TrackProcessing.TrackAnalysis import analyse_recommendations
from Application.logging import write_recommendations_stats, write_recommended_tracks

# Set display option to show all columns
pd.set_option('display.max_columns', None)

N = 5000
k = 25
p_fair = 0.98
lambd = 0.99

import requests
ngrok_url = "https://f133-46-244-6-75.ngrok.io"


def create_recommendation_lists(user_id, initial_uris, profile_sample_size=50):
    if user_id == None:
        user_id = datetime.now().strftime("%H:%M:%S")

    mappings = np.load("../data/mappings.npz", allow_pickle=True)
    dec_mappings = [mappings['user_index_map_inv'].item(), mappings['track_index_map_inv'].item()]
    user_index_map, track_index_map = dec_mappings
    
    del user_index_map
    
    spids_ids = pd.read_csv("../data/spotify_uris.csv")
    track_popularities = pd.read_csv("../data/track_popularity.csv")
    
    
    initial_spids = uris_to_spids(initial_uris)
    valid_initial_spids = get_valid_spids(initial_spids, spids_ids)

    if len(valid_initial_spids) < 5:
        raise InsufficientItemsException("Not enough items")
    
    insufficientItems = 'False'
    if len(valid_initial_spids) < 100:
        insufficientItems = 'True'
    profile_sample = random.sample(valid_initial_spids, profile_sample_size)

    initial_track_ids = spids_to_ids(valid_initial_spids, spids_ids)
    
    user_profile = create_user_profile(user_id, initial_track_ids, track_popularities, len(initial_spids))
    print(user_profile)
    model = load_model()
    
    
    user_items = lil_matrix((1, model.model.item_factors.shape[0]))
    track_index_map_reverse = {value: key for key, value in track_index_map.items()}
    #fill user_items

    for tid in initial_track_ids:
        tidx =  track_index_map_reverse[tid]

        user_items[0,tidx] = 1

    # Convert LIL matrix to CSR format
    user_items = user_items.tocsr()
    
    user_map = model.model.partial_fit_users([user_id], user_items)
    print('Preprocessed user data')
    recommendations_idxs, recommendations_ids, scores = predict(model, user_ids=[user_id], user_items=user_items, N=N, user_index_map=user_map, track_index_map=track_index_map)


    print('Computed Recommendations')

    base_recommendations = recommendations_ids[0][:k]
    print("Created Base Recommendations")

    fair_reranked_ids, fair_reranked_idxs, fair_reranked_scores = rerank('FAIR', recommendations_idxs, recommendations_ids, scores, k=k, user_ids=[user_id], 
                                  user_profiles=user_profile[['user_id', 'head_ratio', 'mid_ratio', 'tail_ratio']], track_popularities=track_popularities, alpha_fair=0.1, p_fair=p_fair)
    print("Created FAIR Recommendations")

    cp_reranked_ids, cp_reranked_idxs, cp_reranked_scores = rerank('CP', recommendations_idxs, recommendations_ids, scores, k=k, user_ids=[user_id], 
                                user_profiles=user_profile[['user_id', 'head_ratio', 'mid_ratio', 'tail_ratio']], track_popularities=track_popularities, delta=lambd)
    print("Created CP Recommendations")
    
    
    recommendation_stats, similarity_stats = analyse_recommendations(user_id, base_recommendations, fair_reranked_ids[0], cp_reranked_ids[0], 
                                                                     user_profile, track_popularities)
    
    print(recommendation_stats)
    print(similarity_stats)
    
    write_recommendations_stats(user_id, user_profile, recommendation_stats, similarity_stats)
    
    base_recommendation_uris = ids_to_spids(base_recommendations, spids_ids)
    
    FAIR_recommendation_uris = ids_to_spids(fair_reranked_ids[0], spids_ids)
    
    CP_recommendation_uris = ids_to_spids(cp_reranked_ids[0], spids_ids)
    
    write_recommended_tracks(user_id, base_recommendation_uris, FAIR_recommendation_uris, CP_recommendation_uris)
    
    return base_recommendation_uris, FAIR_recommendation_uris, CP_recommendation_uris, profile_sample, insufficientItems
    

class InsufficientItemsException(Exception):
    pass