# -*- coding: utf-8 -*-
"""
Created on Tue May 16 13:23:55 2023

@author: unrob
"""

import os
import datetime
import matplotlib.font_manager as fm
import seaborn as sns

abspath = os.path.abspath(__file__)



path = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
completeDataPath = os.path.join(path, 'data', 'complete')

# Data paths
fullDataPath = os.path.join(path,'data', 'raw', 'listening-counts.tsv.bz2')
reducedDataPath = os.path.join(completeDataPath, 'Processed', 'reduced_listening_counts.csv')
trackPopularityDataPath = os.path.join(completeDataPath, 'Processed', 'track_popularity.csv')
userProfileDataPath = os.path.join(completeDataPath, 'Processed', 'user_profiles.csv')
initAMatrixPath = os.path.join(completeDataPath, 'Matrices', 'init_A_matrix.npz')
initRMatrixPath = os.path.join(completeDataPath, 'Matrices', 'init_R_matrix.npz')
initSavePathMappings = os.path.join(completeDataPath, 'Matrices', 'init_mappings.npz')
savePathTest = os.path.join(completeDataPath, 'Matrices', 'A_test.npz')
savePathTrain = os.path.join(completeDataPath, 'Matrices', 'A_train.npz')
spotifyTracksPath = os.path.join(completeDataPath, 'Processed', 'spotify_uris.csv')

spotifyDataPath = os.path.join(path, 'data', 'spotify')
# Data paths

SpreducedDataPath = os.path.join(spotifyDataPath, 'Processed', 'reduced_listening_counts.csv')
SptrackPopularityDataPath = os.path.join(spotifyDataPath, 'Processed', 'track_popularity.csv')
SpuserProfileDataPath = os.path.join(spotifyDataPath, 'Processed', 'user_profiles.csv')
SpinitAMatrixPath = os.path.join(spotifyDataPath, 'Matrices', 'init_A_matrix.npz')
SpinitRMatrixPath = os.path.join(spotifyDataPath, 'Matrices', 'init_R_matrix.npz')
SpinitSavePathMappings = os.path.join(spotifyDataPath, 'Matrices', 'init_mappings.npz')
SpsavePathTest = os.path.join(spotifyDataPath, 'Matrices', 'A_test.npz')
SpsavePathTrain = os.path.join(spotifyDataPath, 'Matrices', 'A_train.npz')
SpspotifyTracksPath = os.path.join(spotifyDataPath, 'Processed', 'spotify_uris.csv')

# Model paths
modelPath = os.path.join(path, 'models')
plotPath = os.path.join(path, 'plots')
evaluationPath = os.path.join(path, 'evaluation')

testingDataPath = os.path.join(path, 'data', 'Testing')
# Testing data paths
reducedDataPathTesting = os.path.join(testingDataPath, 'Processed', 'reduced_listening_counts_testing.csv')
trackPopularityDataPathTesting = os.path.join(testingDataPath, 'Processed', 'track_popularity_testing.csv')
userProfileDataPathTesting = os.path.join(testingDataPath, 'Processed', 'user_profiles_testing.csv')
initAMatrixPathTesting = os.path.join(testingDataPath, 'Matrices', 'init_A_matrix_testing.npz')
initRMatrixPathTesting = os.path.join(testingDataPath, 'Matrices', 'init_R_matrix_testing.npz')
initSavePathMappingsTesting = os.path.join(testingDataPath, 'Matrices', 'init_mappings_testing.npz')
savePathTestTesting = os.path.join(testingDataPath, 'Matrices', 'A_test_testing.npz')
savePathTrainTesting = os.path.join(testingDataPath, 'Matrices', 'A_train_testing.npz')
modelPathTesting = os.path.join(path, 'models', 'testing')

# Evaluation log path
log_file_path = os.path.join(path, 'evaluation_log.txt')
log_file_path = path + '/evaluation_log.txt'

def write_to_log(file_path=log_file_path, dataSet=reducedDataPath, script_name=abspath, title='', notes='', plot_path=''):
    current_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    log_entry = f"{current_time} - Script: {script_name}\n"\
            f"Source Data Set: {dataSet}"\
                f"Title: {title}\nNotes: {notes}\nPath: {plot_path}\n\n"
    
    with open(file_path, 'a') as log_file:
        log_file.write(log_entry)

# Step 1: Define the font path for "opensans"
font_path = path + '/Misc/fonts/OpenSans.ttf'  # Replace with the actual path to "opensans.ttf"
fm.fontManager.addfont(font_path)

# Step 2: Load the font using the font manager
prop = fm.FontProperties(fname=font_path)


def get_font():
    return prop.get_name()

custom_color_palette = ["#0047B3", "#B30000","#2D683A", "#5C3E8E", "#FF6600", "#008C8C", "#4D4D4D", "#7C2855"]
#custom_color_palette = ["#B3D1FF", "#FF9999","#A5C9A2", "#FFB6D9", "#FFD9B3", "#B5E7E7", "#CFCFCF", "#C6B2D6"]
color_mapping = {
    'b': custom_color_palette[0],
    'r': custom_color_palette[1],
    'g': custom_color_palette[2],
    'p': custom_color_palette[3],
    'o': custom_color_palette[4],
    'c': custom_color_palette[5],
    'gray': custom_color_palette[6],
    'dark_p': custom_color_palette[7]
}

def get_color_palette():
    return sns.color_palette(custom_color_palette)

def get_color_mapping():
    return color_mapping