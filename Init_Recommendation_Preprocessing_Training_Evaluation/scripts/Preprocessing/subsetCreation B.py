# -*- coding: utf-8 -*-
"""
Created on Tue May 16 14:37:42 2023

@author: unrob
"""

import os
import pandas as pd
from scripts import main

abspath = os.path.abspath(__file__)



def create_subset(logTitle = 'Creation of reduced Dataset', savePath = main.reducedDataPath):
    # Step 1: Define the batch size and initialize an empty DataFrame to store the results
    batch_size = 100000
    
    i = 0
    
    saved_valid_tracks = set()
    saved_valid_users = set()
    
    df = pd.DataFrame(columns=['user_id', 'track_id', 'count'])
    
    # Create a file to save the reduced dataset
    with open(savePath, 'w') as f:
        f.write('user_id\ttrack_id\tcount\n')  # Write header to the file


        print('Load batches...')
        # Step 2: Iterate over the dataset in chunks
        for i, chunk in enumerate(pd.read_csv(main.fullDataPath, delimiter='\\', chunksize=batch_size)):
            if i % 25 == 0:
                print('it:' + str(i * batch_size) + '; df size: ' + str(len(df)))
        
            # Split the merged column into separate columns
            chunk[['user_id', 'track_id', 'count']] = chunk['user_id\ttrack_id\tcount'].str.split('\t', expand=True)
            
            # Drop the merged column
            chunk.drop('user_id\ttrack_id\tcount', axis=1, inplace=True)
        
            # Step 3: Process each chunk
            chunk['count'] = pd.to_numeric(chunk['count'], downcast='integer')
            
            # Step 5: Filter out interactions where the playcount is smaller than 2
            chunk = chunk[chunk['count'] >= 2]
            valid_interactions = chunk[(chunk['user_id'].isin(saved_valid_users)) & (chunk['track_id'].isin(saved_valid_tracks))]
                
            if not valid_interactions.empty:
                valid_interactions.to_csv(f, sep='\t', index=False, header=False, mode='a')
                chunk = chunk[~chunk.index.isin(valid_interactions.index)]
                
            
            chunk, saved_valid_tracks, saved_valid_users, df_to_save = compute_valid_interactions(chunk, saved_valid_tracks, saved_valid_users)
            
            if not df_to_save.empty:
                df_to_save.to_csv(f, sep='\t', index=False, header=False, mode='a')
                
            # Step 6: Append the processed chunk to the main dataframe
            df = pd.concat([df, chunk], ignore_index=True)
            
            # Clear the chunk variable to free memory
            del chunk
    
            i += 1
            
            if i % 100 == 0:
                df, saved_valid_tracks, saved_valid_users, df_to_save = compute_valid_interactions(df, saved_valid_tracks, saved_valid_users)
                
                if not df_to_save.empty:
                    df_to_save.to_csv(f, sep='\t', index=False, header=False, mode='a')
            
        df, saved_valid_tracks, saved_valid_users, df_to_save = compute_valid_interactions(df, saved_valid_tracks, saved_valid_users)
        
        if not df_to_save.empty:
            df_to_save.to_csv(f, sep='\t', index=False, header=False, mode='a')
            
    print('Dataset saved to ' + savePath)
    
    #create Log
    main.write_to_log(script_name=abspath, title = logTitle, dataSet=savePath,
                      notes='remove interactions where "count" < 2, and with tracks with less than 5 listeners and users with less than 5 listens',
                          plot_path=savePath)
    
    
    
    
def compute_valid_interactions(df, saved_valid_tracks, saved_valid_users):
    # Step 6: Calculate the count of unique users per track
    track_user_count = df.groupby('track_id')['user_id'].nunique().reset_index()
    
    # Step 7: Calculate the count of unique tracks per user
    user_track_count = df.groupby('user_id')['track_id'].nunique().reset_index()
    valid_tracks = track_user_count['track_id'].tolist()
    valid_users = user_track_count['user_id'].tolist()
    
    sub_df = df.copy(deep = True)
    while True:
        # Step 8: Filter out tracks listened to by fewer than 5 different users
        new_valid_tracks = track_user_count[track_user_count['user_id'] >= 5]['track_id'].tolist()
        sub_df = sub_df[sub_df['track_id'].isin(new_valid_tracks) | sub_df['track_id'].isin(saved_valid_tracks)]
        
        # Step 9: Filter out users who listened to fewer than 5 different tracks
        new_valid_users = user_track_count[user_track_count['track_id'] >= 5]['user_id'].tolist()
        sub_df = sub_df[sub_df['user_id'].isin(new_valid_users) | sub_df['user_id'].isin(saved_valid_users)]
        
        
        if len(new_valid_tracks) < len(valid_tracks) or len(new_valid_users) < len(valid_users):
            valid_tracks = new_valid_tracks
            valid_users = new_valid_users
            
            # Step 6: Calculate the count of unique users per track
            track_user_count = sub_df.groupby('track_id')['user_id'].nunique().reset_index()
            
            # Step 7: Calculate the count of unique tracks per user
            user_track_count = sub_df.groupby('user_id')['track_id'].nunique().reset_index()
        elif not sub_df.empty:  
            saved_valid_tracks.update(sub_df['track_id'])
            saved_valid_users.update(sub_df['user_id'])
            df = df[~df.index.isin(sub_df.index)]
            break
        else:
            break
    return df, saved_valid_tracks, saved_valid_users, sub_df

#create_subset(logTitle = 'Creation of reduced Dataset')