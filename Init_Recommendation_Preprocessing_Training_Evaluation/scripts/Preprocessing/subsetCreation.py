# -*- coding: utf-8 -*-
"""
Created on Tue May 16 14:37:42 2023

@author: unrob
"""

import os
import pandas as pd
from scripts import main

abspath = os.path.abspath(__file__)


def create_subset(logTitle = 'Creation of reduced Dataset'):
    # Step 1: Define the batch size and initialize an empty DataFrame to store the results
    batch_size = 100000
    
    i = 0
    
    df = pd.DataFrame(columns=['user_id', 'track_id', 'count'])
    print('Load batches...')
    # Step 2: Iterate over the dataset in chunks
    for i, chunk in enumerate(pd.read_csv(main.fullDataPath, delimiter='\\', chunksize=batch_size)):
        if i % 10 == 0:
            print('it:' + str(i * batch_size))
    
        # Split the merged column into separate columns
        chunk[['user_id', 'track_id', 'count']] = chunk['user_id\ttrack_id\tcount'].str.split('\t', expand=True)
        
        # Drop the merged column
        chunk.drop('user_id\ttrack_id\tcount', axis=1, inplace=True)
    
        # Step 3: Process each chunk
        chunk['count'] = pd.to_numeric(chunk['count'], downcast='integer')
        
        # Step 5: Filter out interactions where the playcount is smaller than 2
        chunk = chunk[chunk['count'] >= 2]
        
        # Step 6: Append the processed chunk to the main dataframe
        df = pd.concat([df, chunk], ignore_index=True)
        
        # Clear the chunk variable to free memory
        del chunk
        
    
    print('Process batches...')
    
    
    # Step 6: Calculate the count of unique users per track
    track_user_count = df.groupby('track_id')['user_id'].nunique().reset_index()
    
    # Step 7: Calculate the count of unique tracks per user
    user_track_count = df.groupby('user_id')['track_id'].nunique().reset_index()
    valid_tracks = track_user_count['track_id'].tolist()
    valid_users = user_track_count['user_id']
    
    while True:
        # Step 8: Filter out tracks listened to by fewer than 5 different users
        new_valid_tracks = track_user_count[track_user_count['user_id'] >= 5]['track_id'].tolist()
        df = df[df['track_id'].isin(new_valid_tracks)]
        
        # Step 9: Filter out users who listened to fewer than 5 different tracks
        new_valid_users = user_track_count[user_track_count['track_id'] >= 5]['user_id'].tolist()
        df = df[df['user_id'].isin(new_valid_users)]
        
        
        if len(new_valid_tracks) < len(valid_tracks) or len(new_valid_users) < len(valid_users):
            valid_tracks = new_valid_tracks
            valid_users = new_valid_users
            
            # Step 6: Calculate the count of unique users per track
            track_user_count = df.groupby('track_id')['user_id'].nunique().reset_index()
            
            # Step 7: Calculate the count of unique tracks per user
            user_track_count = df.groupby('user_id')['track_id'].nunique().reset_index()
        else:
            break
    
    
    
    
    # Step 11: Save the new dataset to a CSV file
    df.to_csv(main.reducedDataPath, index=False)
    
    print('Dataset saved to', main.reducedDataPath)
    
    #create Log
    main.write_to_log(script_name=abspath, title = logTitle, dataSet=main.fullDataPath,
                      notes='remove interactions where "count" < 2, and with tracks with less than 5 listeners and users with less than 5 listens',
                          plot_path=main.reducedDataPath)
#create_subset(logTitle = 'Creation of reduced Dataset')