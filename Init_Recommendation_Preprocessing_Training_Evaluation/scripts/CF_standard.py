import os

from scipy.sparse import csr_matrix

import numpy as np
import implicit


from implicit.als import AlternatingLeastSquares


class ALS:
    def __init__(self):
        self.user_item_matrix = None
        self.factors = 32
        self.regularization = 0.01
        self.alpha=1.0
        self.iterations=15
        self.model = AlternatingLeastSquares(factors=32,regularization=0.01,alpha=1.0,dtype=np.float32,use_native=True, 
                                        use_cg=True, use_gpu=implicit.gpu.HAS_CUDA, iterations=15,
                                        calculate_training_loss=False,num_threads=0, random_state=None)
    def fit(self, user_item_matrix):
        # Convert the train matrix to a sparse CSR matrix
        self.user_item_matrix = csr_matrix(user_item_matrix)
        self.model.fit(self.user_item_matrix)

    def predict(self, user_idxs, N):

        ids, scores = self.model.recommend(user_idxs, self.user_item_matrix[user_idxs, :], N=N, filter_already_liked_items=True)
        return ids, scores
    
 