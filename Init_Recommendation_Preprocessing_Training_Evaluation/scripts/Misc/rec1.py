# -*- coding: utf-8 -*-
"""
Created on Tue Feb 28 16:54:35 2023

@author: unrob
"""

import spotipy
from spotipy.oauth2 import SpotifyClientCredentials

#Authentication - without user
client_credentials_manager = SpotifyClientCredentials(client_id='d95bedc0b93641a397908874e87131ac', 
                                                      client_secret='aeacde1abd244f8bb47acd9df60d4949')
sp = spotipy.Spotify(client_credentials_manager = client_credentials_manager)


ex_song_link = 'https://open.spotify.com/track/5mHdCZtVyb4DcJw8799hZp?si=dc19c5b7afc8481c'
ex_song_URI = ex_song_link.split('/')[-1].split("?")[0]

song = sp.track(ex_song_URI)

audio_features = sp.audio_features(ex_song_URI)
audio_analysis = sp.audio_analysis(ex_song_URI)
#print(audio_features)

ex_artist_unpopular_link = 'https://open.spotify.com/artist/6PZUrbjktV3P6m9RSuqzX5?si=Z4ifvuQuTpKnWWemvH3eXQ'
#ex_artist_unpopular_link = 'https://open.spotify.com/artist/7za6M9P94wuMsOVCHopTsI?si=zNrqRe0WTtKtZTzmPlJJXg'
ex_artist_unpopular_URI = ex_artist_unpopular_link.split('/')[-1].split("?")[0]
artist_unpopular_features = sp.artist(ex_artist_unpopular_URI)

ex_artist_popular_link = 'https://open.spotify.com/artist/3TVXtAsR1Inumwj472S9r4?si=GL5ODPFMS_2XKIw4CmaYJw'
ex_artist_popular_URI = ex_artist_popular_link.split('/')[-1].split("?")[0]
artist_popular_features = sp.artist(ex_artist_popular_URI)

print(artist_unpopular_features, artist_popular_features)