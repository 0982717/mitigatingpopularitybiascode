# -*- coding: utf-8 -*-
"""
Created on Mon Jun 26 17:55:05 2023

@author: unrob
"""
import os
import sys
current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)

import pandas as pd

pd.read_csv('track_data.csv')