# -*- coding: utf-8 -*-
"""
Created on Sat Jun 24 14:01:54 2023

@author: unrob
"""

import os
import sys
current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)

import pandas as pd
import main
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from pygam import LinearGAM

sns.set(font=main.get_font())
customPalette  = sns.set_palette(main.get_color_palette(), color_codes=True)
cm = main.get_color_mapping()


popularities = pd.read_csv(os.path.join(main.path, 'data', 'popularity_comparisons.csv'))

popularities = popularities.sort_values('interactions', ascending=True).reset_index(drop=True)
    
    
# Calculate the total number of interactions

total_interactions = popularities['interactions'].sum()


# Calculate the index range for head, mid, and tail items
head_indices = popularities[popularities['popularity'] == 'head'].index
mid_indices = popularities[popularities['popularity'] == 'mid'].index
tail_indices = popularities[popularities['popularity'] == 'tail'].index

# Determine the indices for last tail, first mid, last mid, and first head items
last_tail_item = tail_indices[-1]
first_mid_item = mid_indices[0]
last_mid_item = mid_indices[-1]
first_head_item = head_indices[0]


tail_cutoff = np.log1p((popularities.loc[last_tail_item, 'interactions'] + popularities.loc[first_mid_item, 'interactions']) / 2)
head_cutoff = np.log1p((popularities.loc[last_mid_item, 'interactions'] + popularities.loc[first_head_item, 'interactions']) / 2)

color_map={'head': cm['b'], 'mid': cm['g'], 'tail':cm['r']}
    
sns.set()

# Scatterplot
plt.figure(figsize=(12, 6))
ax = sns.scatterplot(x=np.log1p(popularities['interactions']), y='sp_popularity', hue='popularity', data=popularities, 
                     color=cm['gray'], palette=color_map)

# Linear regression
sns.regplot(x=np.log1p(popularities['interactions']), y='sp_popularity', data=popularities, scatter=False, label='Linear Regression', ci=None, ax=ax)

# Polynomial regression
sns.regplot(x=np.log1p(popularities['interactions']), y='sp_popularity', data=popularities, scatter=False, order=2, label='Polynomial Regression', ci=None, ax=ax)

# Lowess regression
sns.lineplot(x=np.log1p(popularities['interactions']), y='sp_popularity', data=popularities, label='Lowess Regression', ci=None, ax=ax)

# GAM
gam = LinearGAM().fit(np.log1p(popularities['interactions']), popularities['sp_popularity'])
XX = np.linspace(np.log1p(popularities['interactions']).min(), np.log1p(popularities['interactions']).max(), num=100)
yy = gam.predict(XX)
plt.plot(XX, yy, color='black', label='GAM')


ax.set_xlabel('Item Rank')
ax.set_ylabel('Popularity (%)')

# Add statistical information
correlation = popularities['interactions'].corr(popularities['sp_popularity'])
plt.text(0.95, 0.05, f"Correlation: {correlation:.2f}", horizontalalignment='right', verticalalignment='bottom', transform=ax.transAxes)


# Add vertical dotted lines for head, mid, and tail items
ax.axvline(x=head_cutoff, color=cm['b'], linestyle='--', label='Head')
ax.axvline(x=tail_cutoff, color=cm['r'], linestyle='--', label='Tail')



# Add symbols for head, mid, and tail items on top of the graph
ax.text((tail_cutoff + np.log1p(popularities.loc[popularities.index[0], 'interactions'])) / 2, popularities['sp_popularity'].max(), 'T', color=cm['r'], alpha=1, fontsize=12, ha='center')
ax.text((tail_cutoff + head_cutoff) / 2, popularities['sp_popularity'].max(), 'M', color=cm['g'], alpha=1, fontsize=12, ha='center')
ax.text((head_cutoff + np.log1p(popularities.loc[popularities.index[-1], 'interactions'])) / 2, popularities['sp_popularity'].max(), 'H', alpha=1, color=cm['b'], fontsize=12, ha='center')

ax.legend()

plt.show()




# Compute the correlation between 'SpotifyPopularity' and 'LFMInteractions'
correlation = popularities['interactions'].corr(popularities['sp_popularity'])

# Print the correlation
print(f"Correlation between SpotifyPopularity and LFMInteractions: {correlation:.2f}")
