# -*- coding: utf-8 -*-
"""
Created on Thu May 18 11:30:45 2023

@author: unrob
"""
import os

abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)

import main
import numpy as np
from scipy.sparse import csr_matrix



'''
needs revision and has to be actually adapted so that it works
'''

class RankALS:
    def __init__(self, num_features):
        self.num_features = num_features
        self.num_users = 0
        self.num_items = 0
        self.user_embeddings = None
        self.item_embeddings = None
        self.sum_weights = 0


    def value_initialization(self, sigma, importance_weights):
        self.user_embeddings = np.random.uniform(low=-sigma, high=sigma, size=(self.num_users, self.num_features))
        self.item_embeddings = np.random.uniform(low=-sigma, high=sigma, size=(self.num_items, self.num_features))
        
        self.sum_weights = np.sum(importance_weights)

    def fit(self, user_item_matrix, num_iterations, sigma, importance_weights):
        self.num_users = user_item_matrix.shape[0]
        self.num_items = user_item_matrix.shape[1]
        
        self.value_initialization(sigma, importance_weights)



        for iteration in range(num_iterations):
            #P-step
            q_tilde = np.transpose(self.item_embeddings) @ importance_weights
            A_tilde = np.transpose(self.item_embeddings) @ np.diag(importance_weights) @ self.item_embeddings

            
            # Update user embeddings
            for user in range(self.num_users):
                user_ratings = user_item_matrix[user, :]
                one_macron = sum(user_ratings)
                weighted_ratings = user_ratings * self.item_weights

                weighted_item_embeddings = self.item_embeddings.T * weighted_ratings
                sum_weighted_item_embeddings = np.sum(weighted_item_embeddings, axis=1)

                regularization_term = self.regularization * self.sum_weights * np.eye(self.num_features)
                a = sum_weighted_item_embeddings + np.dot(self.user_embeddings.T, regularization_term)
                b = np.dot(self.user_embeddings, sum_weighted_item_embeddings) + np.dot(self.user_embeddings, np.dot(self.item_embeddings.T, self.item_embeddings))
                self.user_embeddings[user] = np.linalg.solve(b, a)

            # Update item embeddings
            for item in range(self.num_items):
                item_ratings = user_item_matrix[:, item]
                user_weights = importance_weights[:, item]
                weighted_ratings = item_ratings * user_weights

                weighted_user_embeddings = self.user_embeddings.T * weighted_ratings
                sum_weighted_user_embeddings = np.sum(weighted_user_embeddings, axis=1)

                regularization_term = self.regularization * self.sum_weights * np.eye(self.num_features)
                a = sum_weighted_user_embeddings + np.dot(self.item_embeddings.T, regularization_term)
                b = np.dot(self.item_embeddings, sum_weighted_user_embeddings) + np.dot(self.item_embeddings, np.dot(self.user_embeddings.T, self.user_embeddings))
                self.item_embeddings[item] = np.linalg.solve(b, a)




class RankALSRecommender:
    def __init__(self, num_factors):
        self.num_factors = num_factors
        self.num_users = 0
        self.num_items = 0

    def train_model(self, train_matrix, num_iterations, sigma, importance_vector):
        
        self.num_users = train_matrix.shape[0]
        self.num_items = train_matrix.shape[1]
        
        self.user_embeddings = np.random.uniform(low=-sigma, high=sigma, size=(self.num_users, self.num_factors))
        self.item_embeddings = np.random.uniform(low=-sigma, high=sigma, size=(self.num_items, self.num_factors))
        
        sum_weights = np.sum(importance_vector)
        
        for iter in range(1, num_iterations):
            
            #P step: update user vectors
            
            q_tilde = np.zeros(self.num_factors)
            A_tilde = np.zeros((self.num_factors, self.num_factors))

            for j in range(self.num_items):
                qj = self.item_embeddings[j, :]
                sj = importance_vector[j]

                q_tilde += qj * sj
                A_tilde += np.outer(qj, qj) * sj
                
            for u in range(self.num_users):
                A_macron = np.zeros((self.num_factors, self.num_factors))
                sum_cq = np.zeros(self.num_factors)
                b_macron = np.zeros(self.num_factors)
                sum_sqr = np.zeros(self.num_factors)

                Ru = train_matrix[u, :]
                r_blank = np.count_nonzero(Ru)
                sum_sr = 0
                sum_cr = 0
                
                
                
                for i, rui in zip(Ru.indices, Ru.data):
                    qi = self.item_factors[i, :]

                    A_macron += np.outer(qi, qi)
                    sum_cq += qi
                    b_macron += qi * rui

                    si = self.support_vector[i]
                    sum_sr += si * rui
                    sum_cr += rui
                    sum_sqr += qi * si * rui

                M = A_macron * sum_weights - (sum_cq @ np.transpose(q_tilde)) - (q_tilde @ np.transpose(sum_cq)) + A_tilde * r_blank

                y = b_macron * sum_weights - sum_cq * sum_sr - q_tilde * sum_cr + sum_sqr * self.sum_support

                pu = np.linalg.inv(M) @ y
                self.user_factors[u, :] = pu

            m_sum_sr = {}
            m_sum_cr = {}
            m_sum_c = {}
            m_sum_cq = {}

            for u in range(self.num_users):
                Ru = self.train_matrix[u, :]
                sum_sr = 0
                sum_cr = 0
                sum_c = Ru.count_nonzero()
                sum_cq = np.zeros(self.num_factors)

                for i, rui in zip(Ru.indices, Ru.data):
                    sj = self.support_vector[i]

                    sum_sr += sj * rui
                    sum_cr += rui
                    sum_cq += self.item_factors[i, :]

                m_sum_sr[u] = sum_sr
                m_sum_cr[u] = sum_cr
                m_sum_c[u] = sum_c
                m_sum_cq[u] = sum_cq

            for i in range(self.num_items):
                sum_cpp = np.zeros((self.num_factors, self.num_factors))
                sum_p_p_c = np.zeros((self.num_factors, self.num_factors))
                sum_p_p_cq = np.zeros(self.num_factors)
                sum_cpr = np.zeros(self.num_factors)
                sum_c_sr_p = np.zeros(self.num_factors)
                sum_cr_p = np.zeros(self.num_factors)
                sum_p_r_c = np.zeros(self.num_factors)

                si = self.support_vector[i]

                for u in range(self.num_users):
                    pu = self.user_factors[u, :]
                    rui = self.train_matrix[u, i]

                    pp = np.outer(pu, pu)
                    sum_cpp += pp
                    sum_p_p_cq += pp @ m_sum_cq[u]
                    sum_p_p_c += pp * m_sum_c[u]
                    sum_cr_p += pu * m_sum_cr[u]

                    if rui > 0:
                        sum_cpr += pu * rui
                        sum_c_sr_p += pu * m_sum_sr[u]
                        sum_p_r_c += pu * rui * m_sum_c[u]

                M = sum_cpp * self.sum_support + sum_p_p_c * si
                #y = sum_cpp @ sum_sq + sum_cpr * self.sum_support - sum_c_sr_p + sum_p_p_cq * si - sum_cr_p * si + sum_p_r_c * si

                qi = np.linalg.inv(M) @ y
                self.item_factors[i, :] = qi
         
                
import ALS_preprocessing
A, R = ALS_preprocessing.load_matrices(dataPathA = main.initAMatirxPathTesting, dataPathR = main.initRMatirxPathTesting)
ranker= RankALSRecommender(10)
ranker.train_model(train_matrix=R, num_iterations=10, sigma=1, importance_vector=[1 for _ in range(R.shape[1])])