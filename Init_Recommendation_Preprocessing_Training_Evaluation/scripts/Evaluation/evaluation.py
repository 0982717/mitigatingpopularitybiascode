# -*- coding: utf-8 -*-
"""
Created on Thu May 18 15:33:23 2023

@author: unrob
"""


import os

abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)

from scripts import main
import numpy as np


import pandas as pd
import pickle


import seaborn as sns
import matplotlib.pyplot as plt

from scripts.model_handler import predict
sns.set(font=main.get_font())
customPalette  = sns.set_palette(main.get_color_palette(), color_codes=True)
cm = main.get_color_mapping()

def save_metric_values(metric_name, algorithm_name, metric_values):
    # Create a filename based on the metric name
    filename = metric_name + ".csv"

    # Check if the file already elet'xists
    if os.path.exists(filename):
        # If the file exists, load it into a DataFrame
        df = pd.read_csv(filename, index_col=0)
    else:
        # If the file doesn't exist, create a new DataFrame
        df = pd.DataFrame()

    # Add a new column with the metric values for the algorithm
    df[algorithm_name] = metric_values

    # Save the DataFrame to the CSV file
    df.to_csv(filename)
    
    
def preprocess_profile_data(train_user_item_matrix, user_index_map, track_index_map, trackDataPath, userDataPath, savePath):
    track_popularity = pd.read_csv(trackDataPath)

    profile_indices = []
    for row in train_user_item_matrix:
        profile_indices.append(row.nonzero()[1])

    print('get track ids...')
    profile_track_ids = [[track_index_map[index] for index in indices] for indices in profile_indices]
    valid_user_idx = []
    profile_popularity_means = []
    profile_popularity_medians = []
    profile_popularity_variances = []
    print('compute means, medians, variances...')
    for user_idx in range(train_user_item_matrix.shape[0]):
        if user_idx % 1000 == 0:
            print(f'Progress: {(user_idx / train_user_item_matrix.shape[0] * 100):.4f} %')
        user_track_ids = profile_track_ids[user_idx]
        
        if len(user_track_ids) < 2:
            continue
        valid_user_idx.append(user_idx)
        user_tracks = track_popularity[track_popularity['track_id'].isin(user_track_ids)]
        user_track_popularity = user_tracks['interactions']
        
        profile_popularity_means.append(user_track_popularity.mean())
        profile_popularity_medians.append(user_track_popularity.median())
        profile_popularity_variances.append(user_track_popularity.var())
    
    print('create frame for user profile ratios')
    profile_popularity_ratios = pd.DataFrame({
        'user_id': [user_index_map[idx] for idx in valid_user_idx],
        'user_type' :['Overall'] * len(valid_user_idx),
        'head_ratio': np.zeros(len(valid_user_idx)),
        'mid_ratio': np.zeros(len(valid_user_idx)),
        'tail_ratio': np.zeros(len(valid_user_idx))
    })
    
    profile_popularity_ratios['head_ratio'] = np.nan
    profile_popularity_ratios['mid_ratio'] = np.nan
    profile_popularity_ratios['tail_ratio'] = np.nan
    
    track_popularity_labels = track_popularity['popularity'].unique()
    
    print('compute ratios for each user...')
    for i, idx in enumerate(valid_user_idx):
        user_track_ids = profile_track_ids[idx]
        if i % 1000 == 0:
            print(f'Progress: {(i / len(profile_track_ids) * 100):.4f} %')
        user_tracks = track_popularity[track_popularity['track_id'].isin(user_track_ids)]
        track_counts = user_tracks['popularity'].value_counts()
        total_tracks = track_counts.sum()
        
        for label in track_popularity_labels:
            ratio = track_counts.get(label, 0) / total_tracks
            profile_popularity_ratios.loc[i, label + '_ratio'] = ratio
    
    
    profile_average_popularity = pd.DataFrame({
        'user_id': [user_index_map[idx] for idx in valid_user_idx],
        'mean_popularity': profile_popularity_means
    })
    print('save data...')
    # Save preprocessed data
    profile_popularity_ratios.to_csv(savePath + 'profile_popularity_ratios.csv', index=False)
    profile_average_popularity.to_csv(savePath + 'profile_average_popularity.csv', index=False)
    
    with open(savePath + 'profile_track_ids.pkl', 'wb') as f:
        pickle.dump(profile_track_ids, f)

    with open(savePath + 'profile_popularity_means.pkl', 'wb') as f:
        pickle.dump(profile_popularity_means, f)
        
    with open(savePath + 'profile_popularity_medians.pkl', 'wb') as f:
        pickle.dump(profile_popularity_medians, f)
    
    with open(savePath + 'profile_popularity_variances.pkl', 'wb') as f:
        pickle.dump(profile_popularity_variances, f)

    
    main.write_to_log(script_name=abspath, title = 'Created user profile data for testset', dataSet='',
                      notes=f'Processed {len(profile_average_popularity["user_id"])} profiles',
                          plot_path=savePath)
    
    
def calc_precision(recommended_indices, true_indices, n):
    relevant_recommended = recommended_indices[:n]
    num_common = len(set(relevant_recommended) & set(true_indices))
    precision = num_common / n

    # Calculate normalized precision
    random_precision = len(true_indices) / len(recommended_indices)
    perfect_precision = 1.0
    normalized_precision = (precision - random_precision) / (perfect_precision - random_precision)

    return precision, normalized_precision


def calc_recall(recommended_indices, true_indices, n):
    relevant_recommended = recommended_indices[:n]
    num_common = len(set(relevant_recommended) & set(true_indices))
    recall = num_common / len(true_indices)

    # Calculate normalized recall
    random_recall = len(true_indices) / len(recommended_indices)
    perfect_recall = 1.0
    normalized_recall = (recall - random_recall) / (perfect_recall - random_recall)

    return recall, normalized_recall

def calc_ndcg(recommended_indices, true_indices, n):
    #formula based on: http://ethen8181.github.io/machine-learning/recsys/2_implicit.html#NDCG
    relevant_recommended = recommended_indices[:n]
    
    # Calculate the Discounted Cumulative Gain (DCG)
    
    dcg = 0
    for i, idx in enumerate(relevant_recommended):
        relevance = 1 if idx in true_indices else 0
        dcg += (2 ** relevance - 1) / np.log2(i + 2)
    
    # Calculate the Ideal DCG (IDCG) as if all true items were recommended
    idcg = 0
    for i in range(min(n, len(true_indices))):
        idcg += (2 ** 1 - 1) / np.log2(i + 2)
    
    # Calculate the Normalized DCG (NDCG)
    ndcg = dcg / idcg if idcg > 0 else 0
    
    return ndcg

def calc_average_precision(recommended_indices, true_indices, n):
    relevant_recommended = recommended_indices[:n]
    num_common = len(set(relevant_recommended) & set(true_indices))

    precision_sum = 0
    relevant_count = 0
    for i, idx in enumerate(relevant_recommended):
        if idx in true_indices:
            relevant_count += 1
            precision = relevant_count / (i + 1)
            precision_sum += precision

    average_precision = precision_sum / min(n, len(true_indices))
    return average_precision


def evaluate(recommendations_indices, test_user_item_matrix, n, log=False, algorithmName='', logTitle=''):
    num_users = test_user_item_matrix.shape[0]
    precision_list = []
    recall_list = []
    ndcg_list = []
    map_list = []
    normalized_precision_list = []
    normalized_recall_list = []


    for user_idx in range(num_users):
        
        true_indices = test_user_item_matrix[user_idx].nonzero()[1]
        rec_idxs = recommendations_indices[user_idx]
        
        precision, normalized_precision = calc_precision(rec_idxs, true_indices, n)
        recall, normalized_recall = calc_recall(rec_idxs, true_indices, n)

        precision_list.append(precision)
        recall_list.append(recall)
        normalized_precision_list.append(normalized_precision)
        normalized_recall_list.append(normalized_recall)
        
        ndcg_list.append(calc_ndcg(rec_idxs, true_indices, n))
        map_list.append(calc_average_precision(rec_idxs, true_indices, n))

        
    ndcg = sum(ndcg_list) / num_users
    precision = sum(precision_list) / num_users
    recall = sum(recall_list) / num_users
    map_score = sum(map_list) / num_users
    normalized_precision = sum(normalized_precision_list) / num_users
    normalized_recall = sum(normalized_recall_list) / num_users
    
    print(f'Precision: {precision:.4f}')
    print(f'Recall: {recall:.4f}')
    print(f'NDCG: {ndcg:.4f}')
    print(f'Mean Average Precision (MAP): {map_score:.4f}')
    print(f'Normalized Precision: {normalized_precision:.4f}')
    print(f'Normalized Recall: {normalized_recall:.4f}')
    
    
    
    if log==True:
        main.write_to_log(script_name=abspath, title = logTitle, dataSet='',
                          notes=f'Perofrmance metrics; algorithm: {algorithmName}.\n'\
                              f'Precision: {precision:.4f}\n'\
                                  f'Recall: {recall:.4f}',
                          plot_path='')
    return precision, recall, ndcg, map_score, normalized_precision, normalized_recall, precision_list, recall_list, ndcg_list, map_list

def popularity_evaluate(recommender, train_user_item_matrix, test_user_item_matrix, n, user_index_map, track_index_map, 
                        trackDataPath='trackPopularityData.csv', userDataPath='userProfileData.csv', profilePath=main.path + '/data/testset/',
                        savePath='Recommender_Evaluation/', log=False, algorithmName='None',
                        logTitle='Evaluation of popularity of recommendation algorithm'):
    logTitle = logTitle + f'for top {n} recommendations'
    # Load preprocessed data
    profile_popularity_ratios = pd.read_csv(profilePath + 'profile_popularity_ratios.csv')
    profile_average_popularity = pd.read_csv(profilePath + 'profile_average_popularity.csv')
    # Load profile_track_ids
    with open(profilePath + 'profile_track_ids.pkl', 'rb') as f:
        profile_track_ids = pickle.load(f)
    
    # Load profile_popularity_means
    with open(profilePath + 'profile_popularity_means.pkl', 'rb') as f:
        profile_popularity_means = pickle.load(f)
    
    # Load profile_popularity_medians
    with open(profilePath + 'profile_popularity_medians.pkl', 'rb') as f:
        profile_popularity_medians = pickle.load(f)
    
    # Load profile_popularity_variances
    with open(profilePath + 'profile_popularity_variances.pkl', 'rb') as f:
        profile_popularity_variances = pickle.load(f)
    track_popularity = pd.read_csv(trackDataPath)
    user_profiles = pd.read_csv(userDataPath)
    num_users = test_user_item_matrix.shape[0]
    num_tracks = test_user_item_matrix.shape[1]
    
    
    recommended_popularity_means = []
    recommended_popularity_medians = []
    recommended_popularity_variances = []

    recommended_head_ratio = []
    recommended_mid_ratio = []
    recommended_tail_ratio = []

    batch_size = 500
    num_batches = int(np.ceil(num_users / batch_size))
    
    print('Compute user ids')
    user_ids = [user_index_map[user_idx] for user_idx in list(range(num_users))]

    if algorithmName != 'profiles':
        print('Compute recommendations...')
        recommended_track_ids_list = []
        recommended_track_idxs_list = []
        recommended_track_scores_list = []
        i = 0
        for batch_num in range(num_batches):
            start_idx = batch_num * batch_size
            end_idx = min((batch_num + 1) * batch_size, num_users)
            user_batch = list(range(start_idx, end_idx))
        
            recommendations_idxs, recommended_track_ids, scores = predict(recommender, user_idxs=user_batch, N=n, user_index_map=user_index_map, track_index_map=track_index_map)
            
            # Store recommended_track_ids for each user in the batch
            recommended_track_idxs_list.extend(recommendations_idxs)
            recommended_track_ids_list.extend(recommended_track_ids)
            recommended_track_scores_list.extend(scores)
        
            i += len(user_batch)
            print(f'Progress: {(i / num_users * 100)} %')
        
        #recommendations_idxs, recommended_track_ids  = predict(recommender, user_idxs=list(range(num_users)), N=10, user_index_map=user_index_map, track_index_map=track_index_map)
        print('Computing recommended distributions for each user...')
        i=0
        for recommended_track_ids_u in recommended_track_ids_list:
            if i % 500 == 0:
                print(f'Progress: {(i /num_users * 100)} %')
            # Calculate the popularity of the recommended tracks
            recommended_tracks = track_popularity[track_popularity['track_id'].isin(recommended_track_ids_u)]
            recommended_popularity = recommended_tracks['interactions']
    
            recommended_popularity_means.append(recommended_popularity.mean())
            recommended_popularity_medians.append(recommended_popularity.median())
            recommended_popularity_variances.append(recommended_popularity.var())
    
            track_counts = recommended_tracks['popularity'].value_counts()
            total_tracks = track_counts.sum()
    
            recommended_head_ratio.append(track_counts.get('head', 0) / total_tracks)
            recommended_mid_ratio.append(track_counts.get('mid', 0) / total_tracks)
            recommended_tail_ratio.append(track_counts.get('tail', 0) / total_tracks)

            i += 1
    
    if algorithmName != 'profiles':
        print("Calculating recommended popularity ratios...")
        recommended_popularity_ratios = pd.DataFrame({
            'user_id': user_ids,
            'user_type': ['Overall'] * num_users,
            'head_ratio': recommended_head_ratio,
            'mid_ratio': recommended_mid_ratio,
            'tail_ratio': recommended_tail_ratio
        })
    
    
    
        print("Calculating recommended average popularity...")
        recommended_average_popularity = pd.DataFrame({
            'user_id': user_ids,
            'mean_popularity': recommended_popularity_means
        })

    if algorithmName == 'profiles':
        
        print("Calculating average percentage of long-tail items for profiles...")
        average_perc_long_tail_items(profile_popularity_ratios, num_users, log=log, algorithmName='User profile', logTitle=logTitle + 'User profile')
    

        print("Calculating coverage of long tail items for profiles...")
        average_cov_long_tail_items(profile_track_ids, track_popularity, num_tracks, log=log, algorithmName='User profile', logTitle=logTitle + 'User profile')
        
        print("Calculating aggregate diversity for profiles...")
        aggregate_diversity(profile_track_ids, num_tracks, log=log, algorithmName='User profile', logTitle=logTitle + 'User profile')

        print("Calculating Gini for profiles...")
        Gini(profile_track_ids, num_tracks, log=log, logTitle=logTitle + 'User profile')
        
        print("Calculating group average popularity for profiles...")
        group_average_popularity(profile_average_popularity, user_profiles, log=log, algorithmName='User profile', logTitle=logTitle + 'User profile')
        
        print("Calculating descriptive popularity measures for profiles...")
        descriptive_popularity_measures(sum(profile_popularity_means), sum(profile_popularity_medians),
                                        sum(profile_popularity_variances), num_users, log=log, algorithmName='User profile', logTitle=logTitle + 'User profile')

        print("Plotting popularity distributions for user profiles...")
        plot_popularity_distributions(profile_popularity_ratios, savePath, user_profiles, log=log, algorithmName='User profile',
                                      logTitle=logTitle + 'User profile')
        
    else:
        print('Performance metrics:')
        evaluate(recommended_track_idxs_list, test_user_item_matrix, n, log=log, algorithmName=algorithmName, logTitle = logTitle + 'Recommendations')
        
                
        print("Calculating user popularity deviation...")
        user_popularity_deviation(recommended_popularity_ratios, profile_popularity_ratios, user_profiles, log=log, algorithmName=algorithmName, logTitle=logTitle + 'Recommendations')
    
        print("Calculating popularity lift...")
        popularity_lift(recommended_average_popularity, profile_average_popularity, user_profiles, log=log, algorithmName=algorithmName, logTitle=logTitle + 'Recommendations')
    
    
        print("Calculating average percentage of long-tail items for recommendations...")
        average_perc_long_tail_items(recommended_popularity_ratios, num_users, log=log, algorithmName=algorithmName, logTitle=logTitle + 'Recommendations')
    
        print("Calculating coverage of long tail items for recommendations...")
        average_cov_long_tail_items(recommended_track_ids_list, track_popularity, num_tracks, log=log, algorithmName='User profile', logTitle=logTitle + 'Recommendations')
        
        print("Calculating aggregate diversity for recommendations...")
        aggregate_diversity(recommended_track_ids_list, num_tracks, log=log, algorithmName=algorithmName, logTitle=logTitle + 'Recommendations')
        
        print("Calculating Gini for recommendations...")
        Gini(recommended_track_ids_list, num_tracks, log=log, logTitle=logTitle + 'Recommendations', algorithmName=algorithmName)
    
        print("Calculating group average popularity for recommendations...")
        group_average_popularity(recommended_average_popularity, user_profiles, log=log, algorithmName=algorithmName, logTitle=logTitle + 'Recommendations')
    
        
        print("Calculating descriptive popularity measures for recommendations...")
        descriptive_popularity_measures(sum(recommended_popularity_means), sum(recommended_popularity_medians),
                                        sum(recommended_popularity_variances), num_users, log=log, algorithmName=algorithmName, logTitle=logTitle + 'Recommendations')
    
        print("Plotting popularity distributions for recommendations...")
        plot_popularity_distributions(recommended_popularity_ratios,  savePath, user_profiles, log=log, algorithmName=algorithmName,
                                      logTitle=logTitle + 'Recommendations')
    
    print('Finished Evaluation')
import math

def user_popularity_deviation(recommended_popularity_ratios, profile_popularity_ratios, user_profiles, log=False, algorithmName='', logTitle=''):
    group_upd_list = [] 
    group_js_list = []
    for u_type in ['Blockbuster-focused', 'Diverse', 'Niche']:
        gupd, g_js_list = group_user_popularity_deviation(recommended_popularity_ratios, profile_popularity_ratios, user_profiles, u_type, algorithmName=algorithmName, logTitle=logTitle+' for further computation for pop lift')
        group_upd_list.append(gupd)
        group_js_list.append(g_js_list)
    upd = sum(group_upd_list) / 3
    print(f'User popularity deviation: {upd:.4f}')
    
    if log==True:
        main.write_to_log(script_name=abspath, title = logTitle, dataSet='',
                          notes=f'User popularity deviation metric; algorithm: {algorithmName}.\n'\
                              f"User popularity deviation: {upd:.4f}\n",
                          plot_path='')
    return upd, group_upd_list, group_js_list
    
def group_user_popularity_deviation(recommended_popularity_ratios, profile_popularity_ratios, user_profiles, user_type, log=False, algorithmName='', logTitle=''):
    up = user_profiles.drop(['head_ratio', 'mid_ratio', 'tail_ratio'], axis=1)
    
    user_ids = up[(up['user_type'] == user_type) & (up['user_id'].isin(profile_popularity_ratios['user_id']))]['user_id']

    group_js_list = []
    for uid in user_ids:
        group_js_list.append(jensen_shannon(recommended_popularity_ratios, profile_popularity_ratios, uid))
    upd_g = sum(group_js_list) / len(user_ids)
    
    print(f'User group deviation for {user_type}  users:  {upd_g:.4f}')
    if log==True:
        main.write_to_log(script_name=abspath, title = logTitle, dataSet='',
                          notes=f'Group user popularity deviation metric; algorithm: {algorithmName}.\n'\
                              f"User group deviation for {user_type}  users:  {upd_g:.4f}\n",
                          plot_path='')
    return upd_g, group_js_list
    
def jensen_shannon(recommended_popularity_ratios, profile_popularity_ratios, user_id):
    epsilon = 1e-8  # Small non-zero value
    
    A = 0
    B = 0
    for c in ['head_ratio', 'mid_ratio', 'tail_ratio']:
        
        profile_ratio = profile_popularity_ratios[profile_popularity_ratios['user_id']==user_id][c].iloc[0]
        recommended_ratio = recommended_popularity_ratios[recommended_popularity_ratios['user_id']==user_id][c].iloc[0]
        
        if profile_ratio == 0:
            profile_ratio += epsilon
        
        if recommended_ratio == 0:
            recommended_ratio += epsilon
    
        A += profile_ratio * math.log2((2 * profile_ratio) / (profile_ratio + recommended_ratio))
        B += recommended_ratio * math.log2((2 * recommended_ratio) / (profile_ratio + recommended_ratio))
        
    js = (A + B) / 2
    return js

def average_cov_long_tail_items(recommended_track_ids, track_popularity, num_tracks, log=False, algorithmName='', logTitle=''):
    all_track_ids = [tid for user_tracks in recommended_track_ids for tid in user_tracks]
    
    unique_tracks = set(all_track_ids)

    num_tail_items = len(track_popularity[(track_popularity['track_id'].isin(unique_tracks)) & (track_popularity['popularity'] == 'tail')])
    
    actl = num_tail_items / len(track_popularity[track_popularity['popularity']=='tail'])
    
    print(f'Average Coverage of Long Tail Items: {actl:.4f}')
    
    if log==True:
        main.write_to_log(script_name=abspath, title = logTitle, dataSet='',
                          notes=f'Average Coverage of Long Tail Items metric; algorithm: {algorithmName}.\n'\
                              f"Average Coverage of Long Tail Items: {actl:.4f}\n",
                          plot_path='')
    return actl

def popularity_lift(recommended_average_popularity, profile_average_popularity, user_profiles, log=False, algorithmName='', logTitle=''):
    
    gap_r_block, gap_r_div, gap_r_niche = group_average_popularity(recommended_average_popularity, user_profiles, algorithmName=algorithmName, logTitle=logTitle + 'User profile' + ' for further computation for pop lift')
    gap_p_block, gap_p_div, gap_p_niche = group_average_popularity(profile_average_popularity, user_profiles, algorithmName='User profile', logTitle=logTitle + 'User profile' +' for further computation for pop lift')

    arp_r = recommended_average_popularity['mean_popularity'].mean()
    arp_p = profile_average_popularity['mean_popularity'].mean()

    pl_all = (arp_r - arp_p) / arp_p
    pl_block = (gap_r_block - gap_p_block) / gap_p_block
    pl_div = (gap_r_div - gap_p_div) / gap_p_div
    pl_niche = (gap_r_niche - gap_p_niche) / gap_p_niche
    print('Popularity lift per group:')
    print(f'Overall: {pl_all:.4f}')
    print(f'Blockbuster-focused: {pl_block:.4f}')
    print(f'Diverse: {pl_div:.4f}')
    print(f'Niche: {pl_niche:.4f}')
    
    if log==True:
        main.write_to_log(script_name=abspath, title = logTitle, dataSet='',
                          notes=f'Popularity lift metrics; algorithm: {algorithmName}.\n'\
                              f"Overall: {pl_all:.4f}\n"\
                                  f"Blockbuster-focused: {pl_block:.4f}\n"\
                                      f"Diverse: {pl_div:.4f}\n"\
                                          f"Niche: {pl_niche:.4f}\n",
                          plot_path='')
            
    return pl_all, pl_block, pl_div, pl_niche

def average_perc_long_tail_items(popularity_ratios, num_users, log=False, algorithmName='', logTitle=''):
    aptl = popularity_ratios['tail_ratio'].mean()
    
    print(f'Average Percentage of Long Tail Items: {aptl:.4f}')
    
    if log==True:
        main.write_to_log(script_name=abspath, title = logTitle, dataSet='',
                          notes=f'Average Percentage of Long Tail items metric; algorithm: {algorithmName}.\n'\
                              f"Average Percentage of Long Tail Items: {aptl:.4f}\n",
                          plot_path='')
    return aptl
            
from collections import Counter
   
def Gini(track_ids, num_items, log=False, algorithmName='', logTitle=''):
    print('calculate Gini')
    #print(track_ids)
    flattened_track_ids = [tid for user_tracks in track_ids for tid in user_tracks]
    sum_ratio = 0
    counts = list(Counter(flattened_track_ids).values())
    counts += [0] * (num_items - len(counts))
    L_len = sum(counts)
    
    counts.sort()
    occ_sum = 0
    for k, count in enumerate(counts):
        
        occ =  count / L_len
        occ_sum += occ
        sum_ratio += ((num_items - (k+1) + 1) / num_items) * occ
    
    gini = 1 - ((2 / occ_sum) * sum_ratio)
    
    print(f'Gini-index: {gini:.4f}')
    
    if log==True:
        main.write_to_log(script_name=abspath, title = logTitle, dataSet='',
                          notes=f'Gini-index metric; algorithm: {algorithmName}.\n'\
                              f"Gini-index: {gini:.4f}\n",
                          plot_path='')
    return gini

def aggregate_diversity(recommended_track_ids, num_tracks, log=False, algorithmName='', logTitle=''):
    all_track_ids = [tid for user_tracks in recommended_track_ids for tid in user_tracks]
    
    num_unique_tracks = len(set(all_track_ids))
    
    agg_div = num_unique_tracks / num_tracks
    
    print(f'Aggregate Diversity: {agg_div:.4f}')
    
    if log==True:
        main.write_to_log(script_name=abspath, title = logTitle, dataSet='',
                          notes=f'Aggregate Diversity metric; algorithm: {algorithmName}.\n'\
                              f"Aggregate Diversity: {agg_div:.4f}\n",
                          plot_path='')
    return agg_div
    
def group_average_popularity(average_popularity, user_profiles, log=False, algorithmName='', logTitle=''):
    up = user_profiles.drop(['head_ratio', 'mid_ratio', 'tail_ratio'], axis=1)
    ap = pd.merge(average_popularity, up, on='user_id')
    
    blockbuster_avg_pop = ap[ap['user_type'] == 'Blockbuster-focused']['mean_popularity'].mean()
    
    diverse_avg_pop = ap[ap['user_type'] == 'Diverse']['mean_popularity'].mean()
    niche_pop = ap[ap['user_type'] == 'Niche']['mean_popularity'].mean()
    
    print('Group Average Popularity:')
    print(f'Blockbuster-focused: {blockbuster_avg_pop:.4f}')
    print(f'Diverse: {diverse_avg_pop:.4f}')
    print(f'Niche: {niche_pop:.4f}')
    if log==True:
        main.write_to_log(script_name=abspath, title = logTitle, dataSet='',
                          notes=f'Group average popularity metrics; algorithm: {algorithmName}.\n'\
                              f"Blockbuster-focused: {blockbuster_avg_pop:.4f}\n"\
                                  f"Diverse: {diverse_avg_pop:.4f}"\
                                      f"Niche: {niche_pop:.4f}\n",
                          plot_path='')
        
    
    return (blockbuster_avg_pop, diverse_avg_pop, niche_pop)
    
def descriptive_popularity_measures(popularity_mean_sum, popularity_median_sum, popularity_variance_sum, num_users, log=False, algorithmName='', logTitle=''):
    popularity_mean = popularity_mean_sum / num_users
    popularity_median = popularity_median_sum / num_users
    popularity_variance = popularity_variance_sum / num_users
    
    print(f"Mean Popularity (Average Recommendation Popularity): {popularity_mean:.4f}")
    print(f"Median Popularity: {popularity_median:.4f}")
    print(f"Variance Popularity: {popularity_variance:.4f}")
    if log==True:
        main.write_to_log(script_name=abspath, title = logTitle, dataSet='',
                          notes=f'Descriptive metrics; algorithm: {algorithmName}.\n'\
                              f"Mean Popularity (Average Recommendation Popularity): {popularity_mean:.4f}\n"\
                                  f"Median Popularity: {popularity_median:.4f}\n"\
                                      f"Variance Popularity: {popularity_variance:.4f}\n",
                          plot_path='')
    
    return popularity_mean, popularity_median, popularity_variance

def plot_popularity_distributions(popularity_ratios, savePath, user_profiles, log=False, algorithmName='', logTitle=''):
     
     # Set up the figure and axis
     fig, ax = plt.subplots(figsize=(3, 6))
     
     # Accumulate the values for each category
     popularity_ratios['mid_head'] = popularity_ratios['head_ratio'] + popularity_ratios['mid_ratio']
     popularity_ratios['tail_mid_head'] = popularity_ratios['mid_head'] + popularity_ratios['tail_ratio']
     
     # Create the stacked barplot with the specified colors
     cm = {'r': 'red', 'g': 'green', 'b': 'blue'}  # Define color map
     
     sns.barplot(y='tail_mid_head', data=popularity_ratios, ax=ax, label='Tail', color=cm['r'], errorbar=None)
     sns.barplot(y='mid_head', data=popularity_ratios, ax=ax, label='Mid', color=cm['g'], errorbar=None)
     sns.barplot(y='head_ratio', data=popularity_ratios, ax=ax, label='Head', color=cm['b'], errorbar=None)
     
     
     ax.set_ylabel("Ratio")
     ax.legend(title=algorithmName)
     sns.move_legend(
         ax, "lower center",
         bbox_to_anchor=(.5, 1), ncol=3, title=None, frameon=False,
     )
     
     # Save the plot as an image file
     plot_path = main.plotPath + savePath
     plt.savefig(plot_path + algorithmName + '_overall_distribution.png')  # Provide the desired file name and extension
     plt.show()
     #create Log
     main.write_to_log(script_name=abspath, title = logTitle, dataSet='',
                       notes=f'Evaluation of item distribution for all users; algorithm: {algorithmName}.\n'\
                           'Popularity Ratios: \n'\
                               f'Head: {popularity_ratios["head_ratio"]}, Mid: {popularity_ratios["mid_ratio"]}, Niche: {popularity_ratios["tail_ratio"]}\n',
                           plot_path=savePath)
     
     pr = popularity_ratios.drop(['user_type'], axis=1)
     up = user_profiles.drop(['head_ratio', 'mid_ratio', 'tail_ratio'], axis=1)
     
     # Merge user_profiles with user_types based on their index
     pr = pd.merge(pr, up, on='user_id')
     
     # Set up the figure and axis
     fig, ax = plt.subplots(figsize=(6, 6))
     
     x_axis_order = ['Blockbuster-focused', 'Diverse', 'Niche']
     
     # Create the stacked barplot with the specified colors
     cm = {'r': 'red', 'g': 'green', 'b': 'blue'}  # Define color map
     sns.barplot(x='user_type', y='tail_mid_head', data=pr, ax=ax, label='Tail', color=cm['r'], errorbar=None, order=x_axis_order)
     sns.barplot(x='user_type', y='mid_head', data=pr, ax=ax, label='Mid', color=cm['g'], errorbar=None, order=x_axis_order)
     sns.barplot(x='user_type', y='head_ratio', data=pr, ax=ax, label='Head', color=cm['b'], errorbar=None, order=x_axis_order)
     
     ax.set_xlabel("User Type")
     ax.set_ylabel("Ratio")
     ax.legend(title=algorithmName)
     sns.move_legend(
         ax, "lower center",
         bbox_to_anchor=(.5, 1), ncol=3, title=None, frameon=False,
     )
     
     # Save the plot as an image file
     plot_path = main.plotPath + savePath
     plt.savefig(plot_path + algorithmName + '_distribution_by_user_type.png')  # Provide the desired file name and extension
     plt.show()
     
     # Calculate the average popularity ratios for each user type
     average_blockbuster = pr.loc[pr['user_type'] == 'Blockbuster-focused', ['head_ratio', 'mid_ratio', 'tail_ratio']].mean()
     average_diverse = pr.loc[pr['user_type'] == 'Diverse', ['head_ratio', 'mid_ratio', 'tail_ratio']].mean()
     average_niche = pr.loc[pr['user_type'] == 'Niche', ['head_ratio', 'mid_ratio', 'tail_ratio']].mean()

     print('plotted distributions...')
     #create Log
     if log==True:
         main.write_to_log(script_name=abspath, title = logTitle, dataSet='',
                           notes=f'Evaluation of item list for users; algorithm: {algorithmName}.'\
                               f'Popularity ratios for Blockbuster focused: \n'\
                                   f'Head: {average_blockbuster["head_ratio"]}, Mid: {average_blockbuster["mid_ratio"]}, Niche: {average_blockbuster["tail_ratio"]}\n'\
                                       f'Popularity ratios for Diverse users: \n'\
                                           f'Head: {average_diverse["head_ratio"]}, Mid: {average_diverse["mid_ratio"]}, Niche: {average_diverse["tail_ratio"]}\n'\
                                               f'Popularity ratios for Niche users: \n'\
                                                   f'Head: {average_niche["head_ratio"]}, Mid: {average_niche["mid_ratio"]}, Niche: {average_niche["tail_ratio"]}\n',
                               plot_path=savePath)
             

    