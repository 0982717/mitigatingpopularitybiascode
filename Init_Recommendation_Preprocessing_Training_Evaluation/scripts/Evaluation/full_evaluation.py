# -*- coding: utf-8 -*-
"""
Created on Thu May 18 15:33:23 2023

@author: unrob
"""


test_def = 'Testing'
import sys
import os

abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)

from scripts import main
import numpy as np
from scripts.Preprocessing import ALS_preprocessing as pre

import pandas as pd
import pickle


import seaborn as sns

from scripts.model_handler import predict, load_model, rerank
from scripts.Evaluation.evaluation import user_popularity_deviation, popularity_lift, descriptive_popularity_measures, evaluate, \
    group_average_popularity, Gini, aggregate_diversity, average_cov_long_tail_items, average_perc_long_tail_items,\
        plot_popularity_distributions


sns.set(font=main.get_font())
customPalette  = sns.set_palette(main.get_color_palette(), color_codes=True)
cm = main.get_color_mapping()

if len(sys.argv) > 1:
    test_def = sys.argv[1]
if test_def == 'Testing':

 
    test_user_item_matrix, train, mappings = pre.load_train_and_test_matrix(dataPathTest = main.savePathTestTesting, dataPathTrain = main.savePathTrainTesting, dataPathMappings = main.initSavePathMappingsTesting)
    user_index_map, track_index_map = mappings
    
    
    profilePath = main.path + '/data/testset/Testing'
    plotSubscript = '/Recommender_Evaluation/Testing/'
    metricPath = main.evaluationPath + '/TestEvaluation.csv'
    trackDataPath = main.trackPopularityDataPathTesting
    userDataPath = main.userProfileDataPathTesting
    
    modelPath = main.modelPathTesting + '/'
    
else:
    test_user_item_matrix, train, mappings = pre.load_train_and_test_matrix(dataPathTest = main.savePathTest, dataPathTrain = main.savePathTrain, dataPathMappings = main.initSavePathMappings)
    user_index_map, track_index_map = mappings
    
    
    profilePath = main.path + '/data/testset/'
    plotSubscript = 'Recommender_Evaluation/'
    metricPath = main.evaluationPath + '/FullEvaluation1.csv'
    trackDataPath = main.trackPopularityDataPath
    userDataPath = main.userProfileDataPath
    
    modelPath = main.modelPath + '/'

algorithmName = ''

    
profile_popularity_ratios = pd.read_csv(profilePath + 'profile_popularity_ratios.csv')
profile_average_popularity = pd.read_csv(profilePath + 'profile_average_popularity.csv')

# Load profile_track_ids
with open(profilePath + 'profile_track_ids.pkl', 'rb') as f:
    profile_track_ids = pickle.load(f)

# Load profile_popularity_means
with open(profilePath + 'profile_popularity_means.pkl', 'rb') as f:
    profile_popularity_means = pickle.load(f)

# Load profile_popularity_medians
with open(profilePath + 'profile_popularity_medians.pkl', 'rb') as f:
    profile_popularity_medians = pickle.load(f)

# Load profile_popularity_variances
with open(profilePath + 'profile_popularity_variances.pkl', 'rb') as f:
    profile_popularity_variances = pickle.load(f)
    
    
track_popularity = pd.read_csv(trackDataPath)
user_profiles = pd.read_csv(userDataPath)


num_users = test_user_item_matrix.shape[0]
num_tracks = test_user_item_matrix.shape[1]

all_user_ids = [user_index_map[uid] for uid in list(range(num_users))]

# Initialize the DataFrame
df = pd.DataFrame(columns=[
    "Algorithm", "precision", "recall", "normalized_precision", "normalized_recall", 
    "ndcg", "map", "mean", "median", "variance",
    "gap_blockbuster", "gap_diverse", "gap_niche", "aptl", "actl",
    "agg_div", "gini", "upd", "upd_blockbuster", "upd_diverse",
    "upd_niche", "pl", "pl_blockbuster", "pl_diverse", "pl_niche"
])


N=''
k=''
    
modelName = ''


delta = 0
p_fair = 0
alpha_fair = 0.1
#ranker=None
ranker = None
reranker = ''

recommended_track_ids_list, recommended_track_idxs_list, recommended_track_scores_list = [], [], []


recommended_popularity_ratios, recommended_average_popularity, recommended_popularity_means, recommended_popularity_medians, recommended_popularity_variances = pd.DataFrame(), pd.DataFrame(), [], [], []




def set_algorithmName():
    global algorithmName
    algorithmName = modelName + '@' + str(N)
    if reranker != '':
        if reranker != 'FAIR':
            algorithmName += '_' + reranker + '@' + str(k) + f'd{delta:.4f}'
        else:
            algorithmName += f'_{reranker}@{k}alpha{alpha_fair:.4f}p{p_fair:.4f}'
        

def save_metrics(metric_values):
    # Create a dictionary of metric values
    row_data = {
        "Algorithm": metric_values["Algorithm"],
        "precision": metric_values["precision"],
        "recall": metric_values["recall"],
        "normalized_precision": metric_values["normalized_precision"],
        "normalized_recall": metric_values["normalized_recall"],
        "ndcg": metric_values["ndcg"],
        "map": metric_values["map"],
        "mean": metric_values["mean"],
        "median": metric_values["median"],
        "variance": metric_values["variance"],
        "gap_blockbuster": metric_values["gap_blockbuster"],
        "gap_diverse": metric_values["gap_diverse"],
        "gap_niche": metric_values["gap_niche"],
        "aptl": metric_values["aptl"],
        "actl": metric_values["actl"],
        "agg_div": metric_values["agg_div"],
        "gini": metric_values["gini"],
        "upd": metric_values["upd"],
        "upd_blockbuster": metric_values["upd_blockbuster"],
        "upd_diverse": metric_values["upd_diverse"],
        "upd_niche": metric_values["upd_niche"],
        "pl": metric_values["pl"],
        "pl_blockbuster": metric_values["pl_blockbuster"],
        "pl_diverse": metric_values["pl_diverse"],
        "pl_niche": metric_values["pl_niche"]
    }

    # Add a row to the DataFrame
    df.loc[len(df)] = row_data

    # Save the DataFrame to the specified path
    df.to_csv(metricPath, index=False)

def create_recommendations(recommender, N=N):
    
    batch_size = 500
    num_batches = int(np.ceil(num_users / batch_size))
    
    print('Compute recommendations...')
    recommended_track_ids_list = []
    recommended_track_idxs_list = []
    recommended_track_scores_list = []
    i = 0
    for batch_num in range(num_batches):
        start_idx = batch_num * batch_size
        end_idx = min((batch_num + 1) * batch_size, num_users)
        user_batch = list(range(start_idx, end_idx))
        recommendations_idxs, recommended_track_ids, scores = predict(recommender, user_idxs=user_batch, N=N, user_index_map=user_index_map, track_index_map=track_index_map)
        
        # Store recommended_track_ids for each user in the batch
        recommended_track_idxs_list.extend(recommendations_idxs)
        recommended_track_ids_list.extend(recommended_track_ids)
        recommended_track_scores_list.extend(scores)
        
        i += len(user_batch)
        print(f'Progress: {(i / num_users * 100)} %')
        
    return recommended_track_ids_list, recommended_track_idxs_list, recommended_track_scores_list

def compute_standard_recommender_metrics():
    recommended_popularity_means = []
    recommended_popularity_medians = []
    recommended_popularity_variances = []

    recommended_head_ratio = []
    recommended_mid_ratio = []
    recommended_tail_ratio = []
    
    print('Compute user ids')
    user_ids = [user_index_map[user_idx] for user_idx in list(range(num_users))]

    #recommendations_idxs, recommended_track_ids  = predict(recommender, user_idxs=list(range(num_users)), N=10, user_index_map=user_index_map, track_index_map=track_index_map)
    print('Computing recommended distributions for each user...')
    i=0
    for recommended_track_ids_u in recommended_track_ids_list:
        if i % 500 == 0:
            print(f'Progress: {(i /num_users * 100)} %')
        # Calculate the popularity of the recommended tracks
        recommended_tracks = track_popularity[track_popularity['track_id'].isin(recommended_track_ids_u)]
        recommended_popularity = recommended_tracks['interactions']

        recommended_popularity_means.append(recommended_popularity.mean())
        recommended_popularity_medians.append(recommended_popularity.median())
        recommended_popularity_variances.append(recommended_popularity.var())

        track_counts = recommended_tracks['popularity'].value_counts()
        total_tracks = track_counts.sum()

        recommended_head_ratio.append(track_counts.get('head', 0) / total_tracks)
        recommended_mid_ratio.append(track_counts.get('mid', 0) / total_tracks)
        recommended_tail_ratio.append(track_counts.get('tail', 0) / total_tracks)

        i += 1

    
    print("Calculating recommended popularity ratios...")
    recommended_popularity_ratios = pd.DataFrame({
        'user_id': user_ids,
        'user_type': ['Overall'] * num_users,
        'head_ratio': recommended_head_ratio,
        'mid_ratio': recommended_mid_ratio,
        'tail_ratio': recommended_tail_ratio
    })
    
    
    
    print("Calculating recommended average popularity...")
    recommended_average_popularity = pd.DataFrame({
        'user_id': user_ids,
        'mean_popularity': recommended_popularity_means
    })
    
    return recommended_popularity_ratios, recommended_average_popularity, recommended_popularity_means, recommended_popularity_medians, recommended_popularity_variances


def compute_profile_metrics():
        
    print("Calculating descriptive popularity measures for profiles...")
    popularity_mean, popularity_median, popularity_variance = descriptive_popularity_measures(sum(profile_popularity_means), sum(profile_popularity_medians),
                                    sum(profile_popularity_variances), num_users)
    
    print("Calculating group average popularity for profiles...")
    blockbuster_avg_pop, diverse_avg_pop, niche_avg_pop= group_average_popularity(profile_average_popularity, user_profiles)

    print("Calculating average percentage of long-tail items for profiles...")
    aptl = average_perc_long_tail_items(profile_popularity_ratios, num_users)


    print("Calculating coverage of long tail items for profiles...")
    actl= average_cov_long_tail_items(profile_track_ids, track_popularity, num_tracks)
    
    print("Calculating aggregate diversity for profiles...")
    agg_div = aggregate_diversity(profile_track_ids, num_tracks)

    print("Calculating Gini for profiles...")
    gini = Gini(profile_track_ids, num_tracks)
    
    print("Plotting popularity distributions for user profiles...")
    plot_popularity_distributions(profile_popularity_ratios, plotSubscript + algorithmName, user_profiles)
    
    metric_values = {
    "Algorithm": algorithmName,
    "precision": np.nan,
    "recall": np.nan,
    "normalized_precision": np.nan,
    "normalized_recall": np.nan,
    "ndcg": np.nan,
    "map": np.nan,
    "mean": popularity_mean,
    "median": popularity_median,
    "variance": popularity_variance,
    "gap_blockbuster": blockbuster_avg_pop,
    "gap_diverse": diverse_avg_pop,
    "gap_niche": niche_avg_pop,
    "aptl": aptl,
    "actl": actl,
    "agg_div": agg_div,
    "gini": gini,
    "upd": np.nan,
    "upd_blockbuster": np.nan,
    "upd_diverse": np.nan,
    "upd_niche": np.nan,
    "pl": np.nan,
    "pl_blockbuster": np.nan,
    "pl_diverse": np.nan,
    "pl_niche": np.nan
    }
    
    save_metrics(metric_values)


def compute_recommender_metrics():    

    print('Performance metrics:')
    precision, recall, normalized_precision, normalized_recall, ndcg, map_score, _, _, _, _ = evaluate(recommended_track_idxs_list, test_user_item_matrix, N)
    
    print("Calculating descriptive popularity measures for recommendations...")
    popularity_mean, popularity_median, popularity_variance = descriptive_popularity_measures(sum(recommended_popularity_means), sum(recommended_popularity_medians),
                                    sum(recommended_popularity_variances), num_users)
    
    print("Calculating group average popularity for recommendations...")
    blockbuster_avg_pop, diverse_avg_pop, niche_avg_pop = group_average_popularity(recommended_average_popularity, user_profiles)

    print("Calculating average percentage of long-tail items for recommendations...")
    aptl = average_perc_long_tail_items(recommended_popularity_ratios, num_users)

    print("Calculating coverage of long tail items for recommendations...")
    actl= average_cov_long_tail_items(recommended_track_ids_list, track_popularity, num_tracks)
    
    print("Calculating aggregate diversity for recommendations...")
    agg_div =aggregate_diversity(recommended_track_ids_list, num_tracks)
    
    print("Calculating Gini for recommendations...")
    gini = Gini(recommended_track_ids_list, num_tracks)

    print("Calculating user popularity deviation...")
    upd, group_upd_list, _ = user_popularity_deviation(recommended_popularity_ratios, profile_popularity_ratios, user_profiles)
    upd_block, upd_div, upd_niche = group_upd_list
    
    print("Calculating popularity lift...")
    pl_all, pl_block, pl_div, pl_niche = popularity_lift(recommended_average_popularity, profile_average_popularity, user_profiles)
    
    print("Plotting popularity distributions for recommendations...")
    plot_popularity_distributions(recommended_popularity_ratios,  plotSubscript + algorithmName, user_profiles)
    
    metric_values = {
    "Algorithm": algorithmName,
    "precision": precision,
    "recall": recall,
    "normalized_precision": normalized_precision,
    "normalized_recall": normalized_recall,
    "ndcg": ndcg,
    "map": map_score,
    "mean": popularity_mean,
    "median": popularity_median,
    "variance": popularity_variance,
    "gap_blockbuster": blockbuster_avg_pop,
    "gap_diverse": diverse_avg_pop,
    "gap_niche": niche_avg_pop,
    "aptl": aptl,
    "actl": actl,
    "agg_div": agg_div,
    "gini": gini,
    "upd": upd,
    "upd_blockbuster": upd_block,
    "upd_diverse": upd_div,
    "upd_niche": upd_niche,
    "pl": pl_all,
    "pl_blockbuster": pl_block,
    "pl_diverse": pl_div,
    "pl_niche": pl_niche
    }
    
    save_metrics(metric_values)

def evaluation_step(modelName_, N_=10, rerankerName_='', k_=10):
    global N
    global k
    global delta
    global alpha_fair
    global p_fair
    global modelName
    global reranker
    global ranker

    
    global recommended_track_ids_list
    global recommended_track_idxs_list
    global recommended_track_scores_list
    global recommended_popularity_ratios
    global recommended_average_popularity
    global recommended_popularity_means
    global recommended_popularity_medians
    global recommended_popularity_variances
    
    N=N_
    k=k_

    reranker = rerankerName_
        
    modelName = modelName_
    
    set_algorithmName()
    
    print('Now processing: ' + algorithmName)

    if modelName == 'profiles':
        compute_profile_metrics()
    else:
        print(modelPath)
        ranker = load_model(modelName, modelPath=modelPath)
        print(ranker.predict)
        if reranker != '':
            init_recommended_track_ids_list, init_recommended_track_idxs_list, init_recommended_track_scores_list = create_recommendations(ranker, N=N)
            
            delta = 0.0
            while delta <= 1.0:
                if reranker =='FAIR':
                    p_fair = delta

                    if p_fair < 0.02:
                        p_fair = 0.02
                    if p_fair > 0.98:
                        p_fair = 0.98
                        
                recommended_track_ids_list, recommended_track_idxs_list, recommended_track_scores_list = \
                    rerank(reranker, init_recommended_track_idxs_list, init_recommended_track_ids_list, 
                           init_recommended_track_scores_list, k=k, user_ids=all_user_ids, 
                           user_profiles=user_profiles, track_popularities=track_popularity, delta=delta, p_fair=p_fair, alpha_fair=alpha_fair)
                
                
                recommended_popularity_ratios, recommended_average_popularity, recommended_popularity_means, recommended_popularity_medians, recommended_popularity_variances = \
                    compute_standard_recommender_metrics()
                
                compute_recommender_metrics()
                
                delta += 0.1

            
        else:
            recommended_track_ids_list, recommended_track_idxs_list, recommended_track_scores_list = create_recommendations(ranker, N=N)
            recommended_popularity_ratios, recommended_average_popularity, recommended_popularity_means, recommended_popularity_medians, recommended_popularity_variances = \
                compute_standard_recommender_metrics()
            
            compute_recommender_metrics()
    

evaluation_step(modelName_='profiles')
evaluation_step(modelName_='Random', N_=25)
evaluation_step(modelName_='Popularity', N_=25)   
#evaluation_step(modelName_='ALS', N_=25)
#evaluation_step(modelName_='ALS', N_=200, rerankerName_='XQ', k_=25)
#evaluation_step(modelName_='ALS', N_=200, rerankerName_='FAIR', k_=25)
#evaluation_step(modelName_='RankALS1', N_=100)
print('Finished Evaluation')
