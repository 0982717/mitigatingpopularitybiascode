# -*- coding: utf-8 -*-
"""
Created on Wed May 17 11:00:13 2023

@author: unrob
"""

import os
import sys
current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)


import os
import pandas as pd
import main

import seaborn as sns
import matplotlib.pyplot as plt

sns.set(font=main.get_font())
customPalette  = sns.set_palette(main.get_color_palette(), color_codes=True)
cm = main.get_color_mapping()



abspath = os.path.abspath(__file__)

def analyse_user_profiles(dataPath = main.userProfileDataPath, savePath= ('Data_Analysis/' + 'user_group_profiles.png'),
                          logTitle='Distribution of popularity groups in user profiles by user group in subset'):
    # Load the created dataset
    print("Loading the user profiles dataset...")
    user_types = pd.read_csv(dataPath)
    print("Dataset loaded successfully.")
    
    # Calculate the average ratio of tail, mid, and head items for each user group
    print("Calculating the average ratio of tail, mid, and head items for each user group...")
    average_ratios = user_types.groupby('user_type')[['tail_ratio', 'mid_ratio', 'head_ratio']].mean().reset_index()
    
    # Set up the figure and axis
    fig, ax = plt.subplots(figsize=(6, 6))
    
    # Accumulate the values for each category
    user_types['mid_head'] = user_types['head_ratio'] + user_types['mid_ratio']
    user_types['tail_mid_head'] = user_types['mid_head'] + user_types['tail_ratio']
    
    
    # Create the stacked barplot with the specified colors
    print("Creating the stacked barplot...")
    sns.barplot(x='user_type', y='tail_mid_head', data=user_types, ax=ax, label='Tail', color=cm['r'], errorbar=None)
    sns.barplot(x='user_type', y='mid_head', data=user_types, ax=ax, label='Mid', color=cm['g'], errorbar=None)
    sns.barplot(x='user_type', y='head_ratio', data=user_types, ax=ax, label='Head', color=cm['b'], errorbar=None)
    
    
    
    
    ax.set_xlabel("User Type")
    ax.set_ylabel("Ratio")
    ax.legend(title="Popularity")
    sns.move_legend(
        ax, "lower center",
        bbox_to_anchor=(.5, 1), ncol=3, title=None, frameon=False,
    )
    
    # Save the plot as an image file
    print("Saving the plot as an image file...")
    plot_path = main.plotPath + savePath
    plt.savefig(plot_path)  # Provide the desired file name and extension
    plt.show()
    
    #create Log
    main.write_to_log(script_name=abspath, title = logTitle, dataSet=dataPath,
                      notes=f'Test of subset dataset.\n'\
                          f"Blockbuster-focused users ratios: Tail: {average_ratios.loc[average_ratios['user_type'] == 'Blockbuster-focused', 'tail_ratio'].values[0]}, Mid items: {average_ratios.loc[average_ratios['user_type'] == 'Blockbuster-focused', 'mid_ratio'].values[0]}, Head: {average_ratios.loc[average_ratios['user_type'] == 'Blockbuster-focused', 'head_ratio'].values[0]}\n"\
                              f"Diverse users ratios: Tail: {average_ratios.loc[average_ratios['user_type'] == 'Diverse', 'tail_ratio'].values[0]}, Mid: {average_ratios.loc[average_ratios['user_type'] == 'Diverse', 'mid_ratio'].values[0]}, Head items: {average_ratios.loc[average_ratios['user_type'] == 'Diverse', 'head_ratio'].values[0]}\n"\
                                  f"Niche users ratios: Tail: {average_ratios.loc[average_ratios['user_type'] == 'Niche', 'tail_ratio'].values[0]}, Mid: {average_ratios.loc[average_ratios['user_type'] == 'Niche', 'mid_ratio'].values[0]}, Head items: {average_ratios.loc[average_ratios['user_type'] == 'Niche', 'head_ratio'].values[0]}",
                          plot_path=plot_path)
def analyse_profile_sizes(dataPath = main.userProfileDataPath, savePath = ('Data_Analysis/' + 'user_group_profile_sizes.png'),
                          logTitle = 'User profile size by user group in subset'):
    # Load the created dataset
    user_types = pd.read_csv(dataPath)
    
    plt.figure(figsize=(6, 6))
    # Create a bar chart for the profile size (total_items) for each user group
    ax = sns.barplot(x="user_type", y="total_items", data=user_types, palette=[cm['b'], cm['g'], cm['r']])
    ax.set_xlabel("User Type")
    ax.set_ylabel("Profile Size")
    
    # Save the plot as an image file
    plot_path = main.plotPath + savePath
    plt.savefig(plot_path)  # Provide the desired file name and extension
    plt.show()
    
    # Calculate the average profile sizes per user group
    average_profile_sizes = user_types.groupby('user_type')['total_items'].mean().reset_index()
    
    # Generate the formatted text for average profile sizes per user group
    average_sizes_text = ""
    for index, row in average_profile_sizes.iterrows():
        user_type = row['user_type']
        average_size = row['total_items']
        average_sizes_text += f"{user_type}: {average_size}\n"
    
    # Write the log entry
    main.write_to_log(script_name=abspath, title=logTitle, dataSet=dataPath,
                      notes='Test of subset dataset.\n' + average_sizes_text,
                      plot_path=plot_path)
    
def analyse_head_ratio(dataPath = main.userProfileDataPath, savePath = ('Data_Analysis/' + 'head_ratio.png'),
                          logTitle = 'User profile size by user group in subset'):
    # Load the created dataset
    user_types = pd.read_csv(dataPath)
    user_types = user_types.sort_values('tail_ratio', ascending=True).reset_index(drop=True)
    user_types['head_ratio'] = user_types['head_ratio']*100
    user_types['mid_ratio'] = user_types['mid_ratio']*100
    user_types['tail_ratio'] = user_types['tail_ratio']*100
    
    print('Plot the distribution using Seaborn...')
    plt.figure(figsize=(12, 6))
    ax = sns.lineplot(x=user_types.index, y='tail_ratio', data=user_types, color=cm['r'], label='Ratio Tail Items')
    
    ax.set_xlabel('User')
    ax.set_ylabel('Ratio (%)')
    #ax.set(xticklabels=[])
    
    
    # Add vertical dotted lines for head, mid, and tail items
    ax.axhline(y=80, color=cm['gray'], linestyle='--', label='80% Ratio')

    ax.legend()
    
    print("Saving the plot as an image file...")
    # Save the plot as an image file
    plot_path = main.plotPath + savePath
    plt.savefig(plot_path)  # Provide the desired file name and extension
    plt.show()

#analyse_user_profiles(dataPath = main.SpuserProfileDataPath, savePath= ('/Data_Analysis/' + 'Spuser_group_profiles.png'))

#analyse_profile_sizes(main.SpuserProfileDataPath, ('/Data_Analysis/' + 'Spuser_group_profile_sizes.png'))

#analyse_head_ratio(main.SpuserProfileDataPath, ('/Data_Analysis/' + 'Sptail_ratio.png'))