import os
import sys
current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)

#from scripts.Evaluation.full_evaluation import set_and_get_paths, evaluation_step
#set_and_get_paths(testing=True)

#evaluation_step(modelName_='profiles')
#evaluation_step(modelName_='Random', N_=25)

#sys.path.insert(0, "C:/Users/unrob/OneDrive - Universiteit Utrecht/Thesis/Spotify Recommender/Recommenders")

from scripts.Preprocessing import ALS_preprocessing as pre
from scripts import main

from model_handler import train_and_save_model

#model_handler
test, train, mappings = pre.load_train_and_test_matrix(dataPathTest = main.SpsavePathTest, dataPathTrain = main.SpsavePathTrain, dataPathMappings = main.SpinitSavePathMappings)
user_index_map, track_index_map = mappings

#train_and_save_model('RankALS1', train, modelPath=main.modelPathTesting + '/', iterations=15, factors=32)
#train_and_save_model('Popularity', train, modelPath=main.modelPathTesting + '/', iterations=15, factors=32)


#track_popularity = pd.read_csv(main.trackPopularityDataPathTesting)
#user_profiles = pd.read_csv(main.userProfileDataPathTesting)

#test_user_idxs = [8]
#test_user_ids = [user_index_map[idxs] for idxs in test_user_idxs]

#initial_recommendations_idxs, initial_recommendations_ids, initial_scores = predict(model, user_ids=test_user_ids, N=10, user_index_map=user_index_map, track_index_map=track_index_map)

#print(initial_recommendations_ids)
#reranked_ids = rerank(algorithm='CP', initial_idxs=initial_recommendations_idxs, initial_ids=initial_recommendations_ids, initial_scores=initial_scores, k=5, user_ids=test_user_ids,
#                      user_profiles=user_profiles, track_popularities=track_popularity, delta=0.99)
#
#
#print(user_profiles[user_profiles['user_id'].isin(test_user_ids)][['head_ratio', 'mid_ratio', 'tail_ratio']])



#evaluation
#test, train, mappings = pre.load_train_and_test_matrix(dataPathTest = main.SpsavePathTest, dataPathTrain = main.SpsavePathTrain, dataPathMappings = main.SpinitSavePathMappings)
#user_index_map, track_index_map = mappings

#modelName = 'Random'
#ranker=None
#ranker = load_model(modelName, modelPath=main.modelPathTesting)
#from scripts.Evaluation.evaluation import preprocess_profile_data
#preprocess_profile_data(train, user_index_map, track_index_map, trackDataPath= main.SptrackPopularityDataPath, userDataPath= main.SpuserProfileDataPath, savePath=main.path + '/data/spotify/testset/')

#popularity_evaluate(ranker, train, test, 100, user_index_map, track_index_map, trackDataPath = main.trackPopularityDataPathTesting, 
#                    userDataPath = main.userProfileDataPathTesting,  profilePath=main.path + '/data/testset/Testing', log=False,
#                    algorithmName=modelName, 
#                    logTitle='Evaluation of popularity of recommendation algorithm')

