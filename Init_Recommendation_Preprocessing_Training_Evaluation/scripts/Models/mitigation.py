# -*- coding: utf-8 -*-
"""
Created on Mon Jun  5 13:51:53 2023

@author: unrob
"""

import numpy as np
import math
import pandas as pd
from fairsearch.models import FairScoreDoc

def rerank_XQ(initial_list, scores, track_popularities, user_profile, delta=0, k=None):
    
    reranked_list = []
    
    category_counts = {'tail' : 0,
                       'mid': 0,
                       'head': 0}
    
    if k==None:
        k = len(initial_list)
    
    item_popularities = [track_popularities[track_popularities['track_id']==item]['popularity'].item() for item in initial_list]
    for i in range(k):
        
        criterion = [(1 - delta) * score + delta * marginal_likelihood(item, item_popularity, initial_list, user_profile, category_counts, len(reranked_list)) for item, item_popularity, score  in zip(initial_list, item_popularities, scores)]
        
        selected_idx = np.array(criterion).argmax()
        
        reranked_list.append(initial_list[selected_idx])
        category_counts[item_popularities[selected_idx]] += 1
        
        del initial_list[selected_idx]
        del scores[selected_idx]
        del item_popularities[selected_idx]
    return reranked_list

def marginal_likelihood(item, item_popularity, remaining_list, user_profile, category_counts, list_len):
    if list_len == 0:
        return 0
    
    score = 0
    user_profile['mid_tail_ratio'] = user_profile['mid_ratio'] + user_profile['tail_ratio']
    
    for c in ['mid_tail', 'head']:
        if item_popularity != c:
            continue
        else:
            
            score += (user_profile[c + '_ratio'] * math.prod([1 - (category_counts[c] / list_len)]))
    return score
        

def rerank_CP(initial_list, scores, track_popularities, user_profile, delta=0, k=None):
    reranked_list = []
    
    category_counts = {'tail' : 0,
                       'mid': 0,
                       'head': 0}
    
    score_count = 0
    
    if k==None:
        k = len(initial_list)
    item_popularities = [track_popularities[track_popularities['track_id']==item]['popularity'].item() for item in initial_list]
    for i in range(k):
        criterion = marginal_relevances(score_count, scores, item_popularities, category_counts, len(reranked_list), user_profile, delta)
        
        selected_idx = np.array(criterion).argmax()
        score_count += scores[selected_idx]
        
        reranked_list.append(initial_list[selected_idx])
        category_counts[item_popularities[selected_idx]] += 1
        
        del initial_list[selected_idx]
        del scores[selected_idx]
        del item_popularities[selected_idx]

    return reranked_list

def marginal_relevances(score_count, item_scores, item_popularities, category_counts, list_len, user_profile, delta):
    relevances = []
    recommendation_counts = pd.DataFrame({'head_ratio' : [category_counts['head']],
                             'mid_ratio' : [category_counts['mid']],
                             'tail_ratio' : [category_counts['tail']]})
    
    for score, popularity in zip(item_scores, item_popularities):

        recommendation_ratios = recommendation_counts.copy(deep=True)
        recommendation_ratios[popularity + '_ratio'] += 1
        recommendation_ratios /= (list_len + 1)

        relevances.append((1-delta) * (score_count + score) - delta * jensen_shannon(recommendation_ratios, user_profile))
        
    return relevances
        
    
def jensen_shannon(recommendation_ratios, user_profile):
    epsilon = 1e-8  # Small non-zero value
    
    A = 0
    B = 0

    
    for c in ['head_ratio', 'mid_ratio', 'tail_ratio']:
        
        profile_ratio = user_profile[c].item()
        
        recommended_ratio = recommendation_ratios[c].item()
        
        if profile_ratio == 0:
            profile_ratio += epsilon
        
        if recommended_ratio == 0:
            recommended_ratio += epsilon
    
        A += profile_ratio * math.log2((2 * profile_ratio) / (profile_ratio + recommended_ratio))
        B += recommended_ratio * math.log2((2 * recommended_ratio) / (profile_ratio + recommended_ratio))
        
    js = (A + B) / 2
    return js


def rerank_fair(initial_list, scores, track_popularities, fair):
    
    item_popularities = [track_popularities[track_popularities['track_id']==item]['popularity'].item() for item in initial_list]
    item_protection = [False if popularity == 'head' else True for popularity in item_popularities]

    
    unfair_ranking = [FairScoreDoc(iid, sc, ip) for iid, sc, ip in zip(initial_list, scores, item_protection)]
    
    re_ranked = fair.re_rank(unfair_ranking)
    
    reranked_list = [fsd.id for fsd in re_ranked]
    return reranked_list