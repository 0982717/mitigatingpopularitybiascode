import logging
import time

import numpy as np
from tqdm.auto import tqdm

from scipy import sparse

import implicit.gpu
from implicit.gpu.matrix_factorization_base import MatrixFactorizationBase, check_random_state
from implicit.utils import check_csr

log = logging.getLogger("implicit")


class RankAlternatingLeastSquares(MatrixFactorizationBase):
    """Alternating Least Squares

    A Recommendation Model based off the algorithms described in the paper 'Collaborative
    Filtering for Implicit Feedback Datasets' with performance optimizations described in
    'Applications of the Conjugate Gradient Method for Implicit Feedback Collaborative
    Filtering.' from the implicit library. Adapted by using the Java library librec and the paper
    'Alternating Least squares for personalized ranking' to create RankALS

    Parameters
    ----------
    factors : int, optional
        The number of latent factors to compute
    dtype : data-type, optional
        Specifies whether to use 16 bit or 32 bit floating point factors
    iterations : int, optional
        The number of ALS iterations to use when fitting data
    calculate_training_loss : bool, optional
        Whether to log out the training loss at each iteration
    random_state : int, RandomState or None, optional
        The random state for seeding the initial item and user factors.
        Default is None.

    Attributes
    ----------
    item_factors : ndarray
        Array of latent factors for each item in the training set
    user_factors : ndarray
        Array of latent factors for each user in the training set
    """

    def __init__(
        self,
        factors=64,
        dtype=np.float32,
        iterations=15,
        calculate_training_loss=False,
        random_state=None,
    ):
        if not implicit.gpu.HAS_CUDA:
            raise ValueError("No CUDA extension has been built, can't train on GPU.")

        super().__init__()

        # parameters on how to factorize
        self.factors = factors
        self.dtype = dtype

        # options on how to fit the model
        self.iterations = iterations
        self.calculate_training_loss = calculate_training_loss
        self.fit_callback = None
        self.random_state = random_state
        self.cg_steps = 3


    def fit(self, user_items, importance_vector=None, show_progress=True, callback=None):
        """Factorizes the user_items matrix.

        After calling this method, the members 'user_factors' and 'item_factors' will be
        initialized with a latent factor model of the input data.

        The user_items matrix does double duty here. It defines which items are liked by which
        users (P_ui in the original paper), as well as how much confidence we have that the user
        liked the item (C_ui.

        The negative items are implicitly defined: This code assumes that positive items in the
        user_items matrix means that the user liked the item. The negatives are left unset in this
        sparse matrix: the library will assume that means Piu = 0 and Ciu = 1 for all these items.
        Negative items can also be passed with a higher confidence value by passing a negative
        value, indicating that the user disliked the item.

        Parameters
        ----------
        user_items: csr_matrix
            Matrix of confidences for the liked items. This matrix should be a csr_matrix where
            the rows of the matrix are the user, the columns are the items liked by that user,
            and the value is the confidence that the user liked the item.
        show_progress : bool, optional
            Whether to show a progress bar during fitting
        callback: Callable, optional
            Callable function on each epoch with such arguments as epoch, elapsed time and progress
        """
        # initialize the random state
        random_state = check_random_state(self.random_state)

        # TODO: allow passing in cupy arrays on gpu
        Cui = check_csr(user_items)
        

        if Cui.dtype != np.float32:
            Cui = Cui.astype(np.float32)

        # Create importance vector with equal weights if not provided
        if importance_vector is None:
            importance_vector = np.ones(Cui.shape[1], dtype=self.dtype)
        else:
            importance_vector = np.array(importance_vector, dtype=self.dtype)
            if importance_vector.shape[0] != Cui.shape[1]:
                raise ValueError("importance_vector size must match number of items")

        weight_sum = np.sum(importance_vector)
        
        s = time.time()
        Ciu = Cui.T.tocsr()
        log.debug("Calculated transpose in %.3fs", time.time() - s)

        items, users = Ciu.shape

        s = time.time()

        # Initialize the variables randomly if they haven't already been set
        if self.user_factors is None:
            self.user_factors = random_state.uniform(
                users, self.factors, low=-0.5 / self.factors, high=0.5 / self.factors
            )
            self.user_factors = self.user_factors.astype(self.dtype)

        if self.item_factors is None:
            self.item_factors = random_state.uniform(
                items, self.factors, low=-0.5 / self.factors, high=0.5 / self.factors
            )
            self.item_factors = self.item_factors.astype(self.dtype)

        log.debug("Initialized factors in %s", time.time() - s)

        Ciu = implicit.gpu.CSRMatrix(Ciu) #item-user matrix
        Cui = implicit.gpu.CSRMatrix(Cui) #user-item matrix
        P = self.user_factors #user_factors
        Q = self.item_factors #item_factors
        loss = None

        cus = list(set(Cui.nonzero()[0]))
        
        log.debug("Running %i ALS iterations", self.iterations)
        with tqdm(total=self.iterations, disable=not show_progress) as progress:
            for iteration in range(self.iterations):
                s = time.time()
                P = user_factor(P, Q, items, cus, importance_vector, Cui, self.factors, weight_sum)
                
                Q = item_factor(P, Q, items, users, cus, importance_vector, Cui, Ciu, self.factors, weight_sum)
                
                        
                progress.update(1)

                #if self.calculate_training_loss:
                    #loss = self.solver.calculate_loss(Cui, X, Y, self.regularization)
                    #progress.set_postfix({"loss": loss})

                    #if not show_progress:
                    #    log.info("loss %.4f", loss)

                # Backward compatibility
                if not callback:
                    callback = self.fit_callback
                if callback:
                    callback(iteration, time.time() - s, loss)
                    
                    
        self.user_factors = P
        self.item_factors = Q
        
        print('finished fitting')
        if self.calculate_training_loss:
            log.info("Final training loss %.4f", loss)
            
    def recalculate_user(self, userid, user_items, importance_vector=None):
        users = 1 if np.isscalar(userid) else len(userid)
        user_factors = implicit.gpu.Matrix.zeros(users, self.factors).astype(self.dtype)
        Cui = implicit.gpu.CSRMatrix(user_items)
        if importance_vector == None:
            importance_vector = [1 for _ in range(users)]
        weight_sum = np.sum(importance_vector)
        user_factors = user_factor(user_factors, self.item_factors, self.item_factors[0], list(range(users)), importance_vector, user_items, self.factors, weight_sum)

        return user_factors[0] if np.isscalar(userid) else user_factors


    def partial_fit_users(self, userids, user_items):
        """Incrementally updates user factors

        This method updates factors for users specified by userids, given a
        sparse matrix of items that they have interacted with before. This
        allows you to retrain only parts of the model with new data, and
        avoid a full retraining when new users appear - or the liked
        items for an existing user change.

        Parameters
        ----------
        userids : array_like
            An array of userids to calculate new factors for
        user_items : csr_matrix
            Sparse matrix containing the liked items for each user
        """
        if len(userids) != user_items.shape[0]:
            raise ValueError("user_items must contain 1 row for every user in userids")

        # recalculate factors for each user in the input
        user_factors = self.recalculate_user(userids, user_items)

        # ensure that we have enough storage for any new users
        users, factors = self.user_factors.shape
        max_userid = max(userids)
        if max_userid >= users:
            # TODO: grow exponentially ?
            self.user_factors.resize(max_userid + 1, factors)

        self.user_factors.assign_rows(userids, user_factors)



    def to_cpu(self) -> implicit.cpu.als.AlternatingLeastSquares:
        """Converts this model to an equivalent version running on the CPU"""
        ret = implicit.cpu.als.AlternatingLeastSquares(
            factors=self.factors,
            dtype=self.dtype,
            iterations=self.iterations,
            calculate_training_loss=self.calculate_training_loss,
            random_state=self.random_state,
        )
        ret.user_factors = self.user_factors.to_numpy() if self.user_factors is not None else None
        ret.item_factors = self.item_factors.to_numpy() if self.item_factors is not None else None
        return ret
    

def user_factor(P, Q, items, cus, importance_vector, Cui, factors, weight_sum):
    #P-step
    # Initialize q_bar and A_bar
    q_tilde = implicit.gpu.Matrix.zeros(factors,)
    A_tilde = implicit.gpu.Matrix.zeros(factors, factors)
    
    # Calculate q_bar and A_bar
    for j in range(items):
        qj = Q[j]  # assuming Q is a matrix where each row represents an item's factors
        sj = importance_vector[j]  # importance weight for item j
        q_tilde += qj * sj
        A_tilde += np.outer(qj, qj) * sj
    
    # Iterate over each user u
    for u in cus:
        # Extract the fitting vector for user u from Cui
        Ru = Cui[u]
    
        # Set I_bar to be the number of positive entries in Ru
        I_bar = Ru.nnz
    
        # Set r_bar to be the sum of all ratings in Ru
        r_bar = Ru.sum()
    
        # Set q_bar to be the sum of all Q values for user u
        q_bar = implicit.gpu.Matrix.zeros(factors,)
        
        # Set b_bar to be the sum of all Q values for user u times the rating of user u
        b_bar = implicit.gpu.Matrix.zeros(factors,)
        
        A_bar = implicit.gpu.Matrix.zeros(factors, factors)
        
        r_tilde = implicit.gpu.Matrix.zeros(factors,)  # Initialize r_tilde
        
        b_tilde = implicit.gpu.Matrix.zeros(factors,)
        
        for i in range(Ru.nnz):
            
            item_idx = Ru.indices[i]
            qi = Q[item_idx]
            ri = Ru.data[i]
            
            q_bar += qi
            b_bar += qi * ri
            A_bar += np.outer(qi, qi)
            
            r_tilde += ri * importance_vector[item_idx]
            b_tilde +=  qi * importance_vector[item_idx] * ri
            
            
        # Compute M matrix
        M = weight_sum * A_bar - q_bar.outer(q_tilde) - q_tilde.outer(q_bar) + I_bar * A_tilde
    
        # Compute y vector
        y = b_bar * weight_sum - q_bar * r_tilde - r_bar * q_tilde + I_bar * b_tilde
    
        # Solve for updated P using M inverse * y
        P[u] = np.linalg.solve(M, y)
    return P
    
def item_factor(P, Q, items, users, cus, importance_vector, Cui, Ciu, factors, weight_sum):
    #Q step:
        
    q_tilde = np.zeros(factors)
    # Calculate q_bar and A_bar
    for j in range(items):
        qj = Q[j]
        sj = importance_vector[j]
        q_tilde += qj * sj
            
    # Calculate the number of positive interactions for each user
    z = Cui.getnnz(axis=1)
    
    # Create a diagonal matrix diag_z
    diag_z = implicit.gpu.CSRMatrix(sparse.diags(z, 0, format='csr'))
    
    A_bar_bar = P.transpose().dot(diag_z.dot(P))
    
    # Define data structures for r_tilde, r_bar, and Q_bar
    r_tilde = implicit.gpu.Matrix.zeros(users,)
    r_bar = implicit.gpu.Matrix.zeros(users,)
    Q_bar = implicit.gpu.Matrix.zeros(users, factors)
    
    # Iterate over each user u
    for u in cus:
        # Extract the fitting vector for user u from Cui
        Ru = Cui[u]
        
        qu_bar = implicit.gpu.Matrix.zeros(factors,)
        ru_tilde = 0
        ru_bar = 0
        
        for i in range(Ru.nnz):
            item_idx = Ru.indices[i]
            qi = Q[item_idx]
            ri = Ru.data[i]
            
            qu_bar += qi
            ru_tilde += importance_vector[item_idx] * ri
            ru_bar += ri
        
        # Update r_tilde, r_bar, and Q_bar
        r_tilde[u] = ru_tilde
        r_bar[u] = ru_bar
        Q_bar[u] = qu_bar
    
    
    for i in range(items):
        # Initialize A_bar_bar, p_2_bar_bar, b_bar_bar, p_1_bar_bar, and p_3_bar_bar with zero values
        A_bar = implicit.gpu.Matrix.zeros(factors, factors)
        b_bar = implicit.gpu.Matrix.zeros(factors,)
        p_2_bar_bar = implicit.gpu.Matrix.zeros(factors,)
        b_bar_bar = implicit.gpu.Matrix.zeros(factors,)
        p_1_bar_bar = implicit.gpu.Matrix.zeros(factors,)
        p_3_bar_bar = implicit.gpu.Matrix.zeros(factors,)
        
        
        si = importance_vector[i]
        itemVector = Ciu[i]
        
        for u in cus:
            # Extract the fitting vector for user u from Cui
            pu = P[u]
            rui = itemVector.data[u]
            
            pp = np.outer(pu, pu)
            
            A_bar += pp
            p_2_bar_bar += pp @ Q_bar[u]
            p_3_bar_bar += pu * r_bar[u]
            
            if rui > 0:
                b_bar += pu * rui
                p_1_bar_bar += pu * r_tilde[u]
                b_bar_bar += pu * diag_z[u] * rui
                
        # Compute M matrix
        M = A_bar * weight_sum + A_bar_bar * si
    
        # Compute y vector
        y = A_bar * weight_sum - b_bar * weight_sum - p_1_bar_bar + p_2_bar_bar - p_3_bar_bar * si + b_bar_bar * si
        
        # Solve for updated Q[i] using M inverse * y
        Q[i] = np.linalg.solve(M, y)
    return Q
