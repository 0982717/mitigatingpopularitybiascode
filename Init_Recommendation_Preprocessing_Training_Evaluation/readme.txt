This module includes most of the functionality for preprocessing the data, training the models and evaluating it. For the final training, the HPC module was primarily used. There might be slight variations between the two modules. Nevertheless, the main functionality remains the same.
Running scripts might not always work properly because there were some variations done with the imports. Please run the scripts and if it doesn't run, observe if the working directory is appropriate.

Remark: Please add the working models and data to the correct paths.