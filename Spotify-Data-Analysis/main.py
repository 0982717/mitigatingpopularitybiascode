# -*- coding: utf-8 -*-
"""
Created on Tue May 16 13:23:55 2023

@author: unrob
"""

import os
import datetime
import matplotlib.font_manager as fm
import seaborn as sns

abspath = os.path.abspath(__file__)

path = os.path.dirname(os.path.realpath(__file__))


# Step 1: Define the font path for "opensans"
font_path = 'C:/Users/unrob/OneDrive - Universiteit Utrecht/Thesis/Script Submissions/Recommenders/Misc/fonts/OpenSans.ttf'  # Replace with the actual path to "opensans.ttf"
fm.fontManager.addfont(font_path)

# Step 2: Load the font using the font manager
prop = fm.FontProperties(fname=font_path)


def get_font():
    return prop.get_name()

custom_color_palette = ["#0047B3", "#B30000","#2D683A", "#5C3E8E", "#FF6600", "#008C8C", "#4D4D4D", "#7C2855"]
#custom_color_palette = ["#B3D1FF", "#FF9999","#A5C9A2", "#FFB6D9", "#FFD9B3", "#B5E7E7", "#CFCFCF", "#C6B2D6"]
color_mapping = {
    'b': custom_color_palette[0],
    'r': custom_color_palette[1],
    'g': custom_color_palette[2],
    'p': custom_color_palette[3],
    'o': custom_color_palette[4],
    'c': custom_color_palette[5],
    'gray': custom_color_palette[6],
    'dark_p': custom_color_palette[7]
}

def get_color_palette():
    return sns.color_palette(custom_color_palette)

def get_color_mapping():
    return color_mapping