# -*- coding: utf-8 -*-
"""
Created on Tue Jun 27 15:42:21 2023

@author: unrob
"""

import os
import sys
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
import statsmodels.api as sm
import main
import ast

sns.set(font=main.get_font())
customPalette  = sns.set_palette(main.get_color_palette(), color_codes=True)
cm = main.get_color_mapping()

#Step 1: Calculate Percentiles of 'spotify_popularity'

def get_popularities_with_spotify_category(cutoff=0.2, head_cutoff=None):
    popularities = pd.read_csv(main.path + '/popularity_comparisons.csv')
    
    popularities = popularities.sort_values('interactions', ascending=True).reset_index(drop=True)
    if False:
        for idx, row in popularities.iterrows():
            if idx % 1000 == 0:
                print(f'Progress date filter: {idx / len(popularities) * 100}%')
            track = popularities.loc[idx, 'sp_track']
            date = ast.literal_eval(track)['album']['release_date']
            popularities.at[idx, 'date'] = date
    # Calculate the total number of interactions
    
    total_interactions = popularities['interactions'].sum()
    
    
    # Calculate the index range for head, mid, and tail items
    head_indices = popularities[popularities['popularity'] == 'head'].index
    mid_indices = popularities[popularities['popularity'] == 'mid'].index
    tail_indices = popularities[popularities['popularity'] == 'tail'].index
    
    # Determine the indices for last tail, first mid, last mid, and first head items
    last_tail_item = tail_indices[-1]
    first_mid_item = mid_indices[0]
    last_mid_item = mid_indices[-1]
    first_head_item = head_indices[0]
    
    
    tail_cutoff_interactions = (popularities.loc[last_tail_item, 'interactions'] + popularities.loc[first_mid_item, 'interactions']) / 2
    head_cutoff_interactions = (popularities.loc[last_mid_item, 'interactions'] + popularities.loc[first_head_item, 'interactions']) / 2
    
    # Sort the DataFrame based on 'spotify_popularity' in descending order
    popularities = popularities.sort_values('sp_popularity', ascending=False).reset_index(drop=True)
    
    # Calculate the total number of interactions
    total_interactions = popularities['sp_popularity'].sum()
    
    # Calculate the index range for head, mid, and tail items
    head_cutoff = total_interactions * cutoff if head_cutoff== None else total_interactions * head_cutoff
    tail_cutoff = total_interactions * (1-cutoff) 
    
    head_index = popularities['sp_popularity'].cumsum().ge(head_cutoff).idxmax()
    tail_index = popularities['sp_popularity'].cumsum().ge(tail_cutoff).idxmax()
    
    tail_cutoff_spotify = (popularities.loc[tail_index - 1, 'sp_popularity'] + popularities.loc[tail_index , 'sp_popularity'] ) / 2
    head_cutoff_spotify = (popularities.loc[head_index - 1, 'sp_popularity'] + popularities.loc[head_index , 'sp_popularity'] ) / 2
    
    
    
    # Determine the indices for head, mid, and tail items
    head_indices = popularities.index[:head_index]
    mid_indices = popularities.index[head_index:tail_index]
    tail_indices = popularities.index[tail_index:]
    
    # Update the 'popularity' column based on the thresholds
    popularities.loc[head_indices, 'sp_popularity_category'] = 'head'
    popularities.loc[mid_indices, 'sp_popularity_category'] = 'mid'
    popularities.loc[tail_indices, 'sp_popularity_category'] = 'tail'
    
    return popularities, tail_cutoff_spotify, head_cutoff_spotify, tail_cutoff_interactions, head_cutoff_interactions


popularities, tail_cutoff_spotify, head_cutoff_spotify, tail_cutoff_interactions, head_cutoff_interactions = \
    get_popularities_with_spotify_category(cutoff=0.2, head_cutoff=0.22)

print(f'Tail cutoff for LFM: {tail_cutoff_interactions}, Tail cutoff for Spotify: {tail_cutoff_spotify}')
print(f'Head cutoff for LFM: {head_cutoff_interactions}, Head cutoff for Spotify: {head_cutoff_spotify}')

#Step 2: Compare the ratios of categories

def compare_ratios(popularities):
    # Calculate the ratios of 'head', 'tail', and 'mid' items based on 'popularity' category
    popularity_ratios = popularities['popularity'].value_counts(normalize=True)
    
    # Calculate the ratios of 'head', 'tail', and 'mid' items based on 'spotify_popularity' category
    spotify_popularity_ratios = popularities['sp_popularity_category'].value_counts(normalize=True)
    
    # Display the ratios
    print("Ratios based on 'popularity' category:")
    print(popularity_ratios)
    print()
    
    print("Ratios based on 'spotify_popularity' category:")
    print(spotify_popularity_ratios)
    return popularity_ratios, spotify_popularity_ratios


popularity_ratios, spotify_popularity_ratios = compare_ratios(popularities)

print()
print()



#Step 3: Compare Percentiles with 'interactions' Classes
def compute_class_alignment(popularities):
    # Compute overall alignment ratio
    overall_alignment = (popularities['popularity'] == popularities['sp_popularity_category']).mean()

    # Compute alignment ratios per class
    class_alignment = popularities.groupby(['popularity', 'sp_popularity_category']).size() / popularities.groupby('popularity').size()

    return overall_alignment, class_alignment





overall_alignment_ratio, class_alignment_ratios = compute_class_alignment(popularities)

print("Overall Alignment Ratio:", overall_alignment_ratio)
print("\nAlignment Ratios per Class:")
print(class_alignment_ratios)




#Step 4: Validate the Overlap
from sklearn.metrics import confusion_matrix
from sklearn.metrics import cohen_kappa_score
from scipy.stats import chi2_contingency



def validate_overlap(popularities):
    # Compute the Confusion Matrix
    confusion_mat = confusion_matrix(popularities['popularity'], popularities['sp_popularity_category'])
    
    # Compute Cohen's Kappa Score
    kappa_score = cohen_kappa_score(popularities['popularity'], popularities['sp_popularity_category'])
    
    return confusion_mat, kappa_score


confusion_mat, kappa_score = validate_overlap(popularities)

print("Confusion Matrix:")
print(confusion_mat)


print("\nCohen's Kappa Score:", kappa_score)



#Step 5: Pairwise comparisons

def measure_relationship(popularities):
    # Calculate correlation coefficient
    correlation = popularities['interactions'].corr(popularities['sp_popularity'])

    # Perform chi-squared test
    contingency_table = pd.crosstab(popularities['interactions'], popularities['sp_popularity'])
    chi2, p_value, _, _ = chi2_contingency(contingency_table)

    if p_value < 0.05:
        association = "There is a significant association between 'interactions' and 'spotify_popularity'."
    else:
        association = "There is no significant association between 'interactions' and 'spotify_popularity'."

    return correlation, chi2, p_value, association

correlation, chi2, p_value, association = measure_relationship(popularities)
print("Correlation Coefficient:", correlation)
print("Chi-square Value:", chi2)
print("P-value:", p_value)
print(association)
