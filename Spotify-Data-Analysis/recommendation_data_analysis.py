# -*- coding: utf-8 -*-
"""
Created on Wed Jun 28 15:11:11 2023

@author: unrob
"""

import os
import sys
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np

import main

pd.set_option('display.max_columns', None)
pd.set_option('display.max_rows', None)

sns.set(font=main.get_font())
customPalette  = sns.set_palette(main.get_color_palette(), color_codes=True)
cm = main.get_color_mapping()


recommendation_data = pd.read_csv('C:\\Users\\unrob\\OneDrive - Universiteit Utrecht\\Thesis\\Spotify Recommender\\Spotify_data_analysis\\recommendation_data.csv')
track_data = pd.read_csv('C:\\Users\\unrob\\OneDrive - Universiteit Utrecht\\Thesis\\Spotify Recommender\\Spotify_data_analysis\\track_data.csv')
ids_and_uris = pd.read_csv('C:\\Users\\unrob\\OneDrive - Universiteit Utrecht\\Thesis\\Spotify Recommender\\Spotify_data_analysis\\spotify_uris.csv')


track_data = track_data[track_data['User'] != 'user1']
recommendation_data = recommendation_data[recommendation_data['user'] != 'user1']
print('Average popularity of user profiles:')
mean_profile = track_data['SpotifyPopularity'].mean()
print(mean_profile)
print()


print('Average popularity of recommendations:')
mean_recommendations = recommendation_data['popularity'].mean()
print(mean_recommendations)
print()
def plot_rank_popularity(recommendation_data, title=""):
    # Create the heatmap using the histplot function
    plt.figure(figsize=(12, 6))
    sns.histplot(x='rank', y='popularity', data=recommendation_data, bins=40, cmap='viridis')
    
    # Set the axis labels and title
    plt.xlabel('Rank')
    plt.ylabel('Popularity')
    plt.legend()
    plt.savefig(f"C:/Users/unrob/OneDrive - Universiteit Utrecht/Thesis/Spotify Recommender/Spotify_data_analysis/Plots/{title}.png")
    # Display the heatmap
    plt.show()


tail_cutoff = 19
head_cutoff= 45

def plot_popularity_category_counts(recommendation_data):
    # Calculate the number of tail, mid, and head items
    tail_items = recommendation_data[recommendation_data['popularity'] <= tail_cutoff].shape[0]
    mid_items = recommendation_data[(recommendation_data['popularity'] > tail_cutoff) & (recommendation_data['rank'] < head_cutoff)].shape[0]
    head_items = recommendation_data[recommendation_data['popularity'] >= head_cutoff].shape[0]
    
    # Create a bar plot to visualize the counts
    plt.figure(figsize=(8, 6))
    sns.barplot(x='Category', y='Count', data=pd.DataFrame({'Category': ['Tail', 'Mid', 'Head'], 'Count': [tail_items, mid_items, head_items]}))
    
    # Set the axis labels and title
    plt.xlabel('Category')
    plt.ylabel('Count')
    plt.title('Number of Tail, Mid, and Head Items')
    
    # Display the plot
    plt.show()

    
    print('Remark: The plot showing the number of tail, mid, and head items might not be meaningful, since it only includes more modern songs than in the original dataset \n'\
          'those might generally be more popular, the cutoffs for new data will probably be quite different')

def lfm_recommendation_subset(recommendation_data):
    valid_uris = list(ids_and_uris['uri'])
    
    recommendation_data['id'] = [uri.split(':')[-1] for uri in recommendation_data['uri']]
   
    lfm_subset_recommendations = recommendation_data[recommendation_data['id'].isin(valid_uris)]
    return lfm_subset_recommendations

def get_subset_statistics(recommendation_data, subset_recommendations):
    
    # Calculate the ratio for each user
    user_ratios = subset_recommendations.groupby('user').size() / recommendation_data.groupby('user').size()
    
    # Calculate the average, standard deviation, maximum, and minimum ratios
    average_ratio = user_ratios.mean()
    std_ratio = user_ratios.std()
    max_ratio = user_ratios.max()
    min_ratio = user_ratios.min()
    
    # Calculate the overall average sizes
    subset_size = subset_recommendations.groupby('user').size()
    original_size = recommendation_data.groupby('user').size()
    
    average_subset_size = subset_size.mean()
    average_original_size = original_size.mean()
    
    original_uniques = recommendation_data.groupby('user')['uri'].nunique()
    subset_uniques = subset_recommendations.groupby('user')['uri'].nunique()
    
    average_original_uniques = original_uniques.mean()
    average_subset_uniques = subset_uniques.mean()
    
    # Print the results
    print("Average Ratio: ", average_ratio)
    print("Std Ratio: ", std_ratio)
    print("Max Ratio: ", max_ratio)
    print("Min Ratio: ", min_ratio)
    print("Average Subset Size: ", average_subset_size)
    print("Average Original Size: ", average_original_size)
    print("Average Unique Subset Size: ", average_subset_uniques)
    print("Average Unique Original Size: ", average_original_uniques)

def lfm_statistics(lfm_user_stats, lfm_recommendation_stats):
    lfm_user_stats['Type'] = 'Profile'
    lfm_recommendation_stats['Type'] = 'Recommendations'
    
    # Merge the DataFrames based on the common 'user' column
    combined_df = pd.concat([lfm_user_stats, lfm_recommendation_stats], ignore_index=True)

    # Melt the DataFrame to transform the 'head', 'mid', and 'tail' columns into a single 'Type' column
    melted_df = combined_df.melt(id_vars=['user','Type', 'interactions', 'popularity_spotify'], value_vars=['head', 'tail', 'mid'], var_name='Popularity', value_name='Ratio')


    
    plt.figure()

    popularity_order = ['tail', 'mid', 'head']

    # Define custom colors for the LFMPopularity categories
    popularity_colors = {'tail': 'red', 'mid': 'green', 'head': 'blue'}

    # Create the count barplot with custom colors and order
    sns.barplot(data=melted_df, x='Type', y='Ratio', hue='Popularity',
                  palette=popularity_colors, hue_order=popularity_order)

    # Set labels and title
    plt.xlabel('User')
    plt.ylabel('Ratio')
    plt.savefig("C:/Users/unrob/OneDrive - Universiteit Utrecht/Thesis/Spotify Recommender/Spotify_data_analysis/Plots/PopularityCategoriesLFMUserRecommendationsb.png")


    plt.show()


def lfm_statistics_b(lfm_user_stats, lfm_recommendation_stats):
    lfm_user_stats['Type'] = 'Profile'
    lfm_recommendation_stats['Type'] = 'Recommendations'

    # Merge the DataFrames based on the common 'user' column
    combined_df = pd.concat([lfm_user_stats, lfm_recommendation_stats], ignore_index=True)

    # Melt the DataFrame to transform the 'head', 'mid', and 'tail' columns into a single 'Type' column
    melted_df = combined_df.melt(id_vars=['user', 'Type', 'interactions', 'popularity_spotify'], value_vars=['head', 'tail', 'mid'], var_name='Popularity', value_name='Ratio')

    plt.figure()

    popularity_order = ['tail', 'mid', 'head']
    type_order = ['Profile', 'Recommendations']



    # Create the count barplot with custom colors and order
    sns.barplot(data=melted_df, x='Popularity', y='Ratio', hue='Type',
                palette=customPalette, order=popularity_order, hue_order=type_order)

    # Set labels and title
    plt.xlabel('Popularity Category')
    plt.ylabel('Ratio')
    plt.legend(title='Data Type')
    plt.savefig("C:/Users/unrob/OneDrive - Universiteit Utrecht/Thesis/Spotify Recommender/Spotify_data_analysis/Plots/PopularityCategoriesLFMUserRecommendations.png")



    plt.show()


def spotify_statistics(track_data, recommendation_data, dataType='Spotify'):
    # Define the popularity ranges and labels
    popularity_ranges = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
    labels = [f'{i}-{i+10}' for i in popularity_ranges[:-1]]
    
    # Calculate the number of items within each popularity range in track_data
    track_popularity_counts = pd.cut(track_data['SpotifyPopularity'], bins=popularity_ranges, labels=labels, right=False).value_counts().sort_index()
    
    # Compute the ratio of items within each popularity range in track_data
    track_ratio = track_popularity_counts / track_data.shape[0]
    
    # Calculate the number of items within each popularity range in recommendation_data
    recommendation_popularity_counts = pd.cut(recommendation_data['popularity'], bins=popularity_ranges, labels=labels, right=False).value_counts().sort_index()
    
    # Compute the ratio of items within each popularity range in recommendation_data
    recommendation_ratio = recommendation_popularity_counts / recommendation_data.shape[0]
    
    # Combine the track_data and recommendation_data ratios into a single DataFrame
    data = pd.DataFrame({'Popularity Range': labels, 'Profile': track_ratio, 'Recommendations': recommendation_ratio})
    
    # Melt the DataFrame to have a single 'Ratio' column and a 'Data' column indicating the source
    melted_data = data.melt(id_vars='Popularity Range', var_name='Data', value_name='Ratio')
    
    # Plot the distributions using seaborn's barplot with hue
    sns.barplot(x='Popularity Range', y='Ratio', hue='Data', data=melted_data, palette=customPalette)
    plt.xlabel('Popularity Range')
    plt.ylabel('Ratio')
    plt.xticks(rotation=90)
    plt.legend()
    plt.savefig(f"C:/Users/unrob/OneDrive - Universiteit Utrecht/Thesis/Spotify Recommender/Spotify_data_analysis/Plots/PopularityCategoriesUserRecommendation{dataType}.png")

    plt.show()
    

    
def get_lfm_user_statistics(track_data):
    track_data_b = track_data.copy()
    track_data_b = track_data_b.rename(columns={'User': 'user',
                                                'LFMInteractions':'interactions',
                                                'LFMPopularity':'popularity_lfm',
                                                'SpotifyPopularity':'popularity_spotify'})
    
    # Calculate the average number of interactions per User
    avg_interactions = track_data_b.groupby('user')['interactions'].mean().reset_index()
    
    # Calculate the average ratios of head, mid, and tail items per User
    avg_ratios = track_data_b.groupby('user')['popularity_lfm'].value_counts(normalize=True).unstack().reset_index()
    
    # Fill NaN values with 0
    avg_ratios = avg_ratios.fillna(0)
    
    avg_sp_popularity =  track_data_b.groupby('user')['popularity_spotify'].mean().reset_index()
    # Merge the average interactions and ratios DataFrames
    merged_df = pd.merge(avg_interactions, avg_ratios, on='user')
    merged_df = pd.merge(merged_df, avg_sp_popularity, on='user')
    
   
    return merged_df
    
def get_lfm_recommendation_statistics(subset_recommendations):
    track_popularity = pd.read_csv("C:/Users/unrob/OneDrive - Universiteit Utrecht/Thesis/Spotify Recommender/Spotify_data_analysis/track_popularity.csv")
    uris = pd.read_csv("C:/Users/unrob/OneDrive - Universiteit Utrecht/Thesis/Spotify Recommender/Spotify_data_analysis/spotify_uris.csv")


    all_popularities = pd.merge(uris, track_popularity, on='track_id', how='left')
    
    subset_recommendations = subset_recommendations.merge(all_popularities[['uri','interactions', 'popularity']],
                                         suffixes=('_spotify', '_lfm'),               
                                         how='left', 
                                         left_on='id', 
                                         right_on='uri')
    # Calculate the average number of interactions per User
    avg_interactions = subset_recommendations.groupby('user')['interactions'].mean().reset_index()
    
    # Calculate the average ratios of head, mid, and tail items per User
    avg_ratios = subset_recommendations.groupby('user')['popularity_lfm'].value_counts(normalize=True).unstack().reset_index()
    
    avg_sp_popularity =  subset_recommendations.groupby('user')['popularity_spotify'].mean().reset_index()
    # Merge the average interactions and ratios DataFrames
    merged_df = pd.merge(avg_interactions, avg_ratios, on='user')
    merged_df = pd.merge(merged_df, avg_sp_popularity, on='user')
    
   
    return merged_df
    
    
#plot_rank_popularity(recommendation_data, title='rankPopularitySpotify')
#plot_popularity_category_counts(recommendation_data)

lfm_subset_recommendations = lfm_recommendation_subset(recommendation_data)

print('Average popularity of subset recommendations:')
mean_subset_recommendations = lfm_subset_recommendations['popularity'].mean()
print(mean_subset_recommendations)
print()

#plot_rank_popularity(lfm_subset_recommendations, title='rankPopularityLFM')
#plot_popularity_category_counts(lfm_subset_recommendations)

#get_subset_statistics(recommendation_data, lfm_subset_recommendations)


lfm_user_stats = get_lfm_user_statistics(track_data)
lfm_recommendation_stats = get_lfm_recommendation_statistics(lfm_subset_recommendations)

lfm_statistics(lfm_user_stats, lfm_recommendation_stats)
lfm_statistics_b(lfm_user_stats, lfm_recommendation_stats)


spotify_statistics(track_data, recommendation_data, dataType='Spotify')

spotify_statistics(track_data, lfm_subset_recommendations, dataType='LFM')