# -*- coding: utf-8 -*-
"""
Created on Sat Jun 24 14:01:54 2023

@author: unrob
"""


import os

abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)

import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
import statsmodels.api as sm
import main


sns.set(font=main.get_font())
customPalette  = sns.set_palette(main.get_color_palette(), color_codes=True)
cm = main.get_color_mapping()


popularities = pd.read_csv('C:/Users/unrob/OneDrive - Universiteit Utrecht/Thesis/Spotify Recommender/Spotify_data_analysis/popularity_comparisons.csv')

popularities = popularities.sort_values('interactions', ascending=True).reset_index(drop=True)
    

# Calculate the total number of interactions

total_interactions = popularities['interactions'].sum()


# Calculate the index range for head, mid, and tail items
head_indices = popularities[popularities['popularity'] == 'head'].index
mid_indices = popularities[popularities['popularity'] == 'mid'].index
tail_indices = popularities[popularities['popularity'] == 'tail'].index

# Determine the indices for last tail, first mid, last mid, and first head items
last_tail_item = tail_indices[-1]
first_mid_item = mid_indices[0]
last_mid_item = mid_indices[-1]
first_head_item = head_indices[0]


tail_cutoff = np.log1p((popularities.loc[last_tail_item, 'interactions'] + popularities.loc[first_mid_item, 'interactions']) / 2)
head_cutoff = np.log1p((popularities.loc[last_mid_item, 'interactions'] + popularities.loc[first_head_item, 'interactions']) / 2)

color_map={'head': cm['b'], 'mid': cm['g'], 'tail':cm['r']}
    
sns.set()

# Create the scatter plot
plt.figure(figsize=(12, 6))
ax = sns.scatterplot(x=np.log1p(popularities['interactions']), y='sp_popularity', hue='popularity', data=popularities, 
                     color=cm['gray'], palette=color_map)

# Linear regression
#sns.regplot(x=np.log1p(popularities['interactions']), y='sp_popularity', data=popularities, scatter=False, label='Linear Regression', ci=None, ax=ax)

# Polynomial regression
#sns.regplot(x=np.log1p(popularities['interactions']), y='sp_popularity', data=popularities, scatter=False, order=2, label='Polynomial Regression', ci=95, ax=ax)

# Lowess regression -> not fittings
#sns.lineplot(x=np.log1p(popularities['interactions']), y='sp_popularity', data=popularities, label='Lowess Regression', ci=None, ax=ax)

# GAM
#gam = LinearGAM().fit(np.log1p(popularities['interactions']), popularities['sp_popularity'])
#XX = np.linspace(np.log1p(popularities['interactions']).min(), np.log1p(popularities['interactions']).max(), num=100)
#yy = gam.predict(XX)
#plt.plot(XX, yy, color='black', label='GAM')
# Add logarithmic scale to x-axis
#ax.set_xscale('log')

# Perform OLS regression with a quadratic term
x = np.log1p(popularities['interactions'])
x_squared = x ** 2
X = sm.add_constant(np.column_stack((x, x_squared)))
y = popularities['sp_popularity']
model = sm.OLS(y, X)
results = model.fit()
print(results.summary())

pred_ols = results.get_prediction()

iv_l = pred_ols.summary_frame()["obs_ci_lower"]

iv_u = pred_ols.summary_frame()["obs_ci_upper"]


ax.plot(x, results.fittedvalues, "r--", label="OLS", color=cm['gray'])
ax.plot(x, iv_u, "r--")
ax.plot(x, iv_l, "r--")


# Set the tick positions and labels on the x-axis
x_ticks = [5, 10, 20, 40, 80, 160, 320, 640, 1280, 2560, 5120]
x_labels = [str(x) for x in x_ticks]
ax.set_xticks(np.log1p(x_ticks))
ax.set_xticklabels(x_labels)

# Set the y-limit to only positive values
ax.set_ylim(bottom=0)


ax.set_xlabel('# Interactions')
ax.set_ylabel('Spotify Popularity')


# Add vertical dotted lines for head, mid, and tail items
ax.axvline(x=head_cutoff, color=cm['b'], linestyle='--', label='Head')
ax.axvline(x=tail_cutoff, color=cm['r'], linestyle='--', label='Tail')



# Add symbols for head, mid, and tail items on top of the graph
ax.text((tail_cutoff + np.log1p(popularities.loc[popularities.index[0], 'interactions'])) / 2, popularities['sp_popularity'].max(), 'T', color=cm['r'], alpha=1, fontsize=12, ha='center')
ax.text((tail_cutoff + head_cutoff) / 2, popularities['sp_popularity'].max(), 'M', color=cm['g'], alpha=1, fontsize=12, ha='center')
ax.text((head_cutoff + np.log1p(popularities.loc[popularities.index[-1], 'interactions'])) / 2, popularities['sp_popularity'].max(), 'H', alpha=1, color=cm['b'], fontsize=12, ha='center')

ax.legend(loc="best")
plt.savefig("C:/Users/unrob/OneDrive - Universiteit Utrecht/Thesis/Spotify Recommender/Spotify_data_analysis/Plots/popularityMeasureComparison.png")

plt.show()




# Compute the correlation between 'SpotifyPopularity' and 'LFMInteractions'
correlation = popularities['interactions'].corr(popularities['sp_popularity'])

# Print the correlation
print(f"Correlation between SpotifyPopularity and LFMInteractions: {correlation:.2f}")