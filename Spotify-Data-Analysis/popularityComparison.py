# -*- coding: utf-8 -*-
"""
Created on Thu Jun 22 15:58:16 2023

@author: unrob
"""

import os

abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)

import pandas as pd
import csv
import main
import spotipy
from spotipy.oauth2 import SpotifyClientCredentials

client_id='here goes your Spotify client id'
client_secret='here goes your Spotify client secret'

#Authentication - without user
client_credentials_manager = SpotifyClientCredentials(client_id=client, 
                                                      client_secret=secret)
sp = spotipy.Spotify(client_credentials_manager = client_credentials_manager)

track_popularity = pd.read_csv(main.SptrackPopularityDataPath)
uris = pd.read_csv(main.SpspotifyTracksPath)


all_popularities = pd.merge(uris, track_popularity, on='track_id', how='left')

sample_popularities = all_popularities.sample(n=10000)
sample_popularities.reset_index(inplace=True, drop=True)
for idx, row in sample_popularities.iterrows():
    if idx % 1000 == 0:
        print(f'Progress: {idx / len(sample_popularities) * 100}%')
    uri = row['uri']
    sp_track = sp.track(uri)
    sp_pop = sp_track['popularity']

    sample_popularities.at[idx, 'sp_popularity'] = sp_pop
    sample_popularities.at[idx, 'sp_track'] = str(sp_track)
    
    
sample_popularities.to_csv(os.path.join(main.path, 'data', 'popularity_comparisons.csv'), index=False)