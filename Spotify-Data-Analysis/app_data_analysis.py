# -*- coding: utf-8 -*-
"""
Created on Tue Feb 28 16:54:35 2023

@author: unrob
"""
import secrets
from flask import Flask, request, render_template, redirect, session, url_for
import webbrowser

import csv
import os
import time
import datetime
import random
import string
import pandas as pd
import numpy as np
import spotipy
from requests.auth import HTTPBasicAuth
import requests
from requests_oauthlib import OAuth2Session

app = Flask(__name__)
app.secret_key = secrets.token_hex(16)
redirect_uri = 'http://localhost:8888/callback'
token_url = 'https://accounts.spotify.com/api/token'
auth_url = 'https://accounts.spotify.com/authorize'

client_id='here goes your Spotify client id'
client_secret='here goes your Spotify client secret'
# Set up Spotipy authentication credentials
scope = "user-read-private user-read-email user-read-recently-played user-top-read"

sp_oauth = spotipy.oauth2.SpotifyOAuth(client_id=client_id, client_secret=client_secret,
                                      redirect_uri=redirect_uri, scope=scope)

# Create a new Spotipy client with the access token
sp = spotipy.Spotify(client_credentials_manager=sp_oauth)

track_data = pd.read_csv('C:\\Users\\unrob\\OneDrive - Universiteit Utrecht\\Thesis\\Spotify Recommender\\Spotify_data_analysis\\track_data.csv')

track_data = track_data[track_data['Type']=='long_term']
users = track_data['User'].unique()

# Extract the necessary information from the recommendation response
recommended_tracks = pd.DataFrame(columns=['user', 'trial','rank', 'top_track_range', 'track', 'artist', 'album', 'image', 'uri', 'popularity'])

for user in users:
    print("Processing user:", user)
    
    for trial in range(10):
        print("Trial:", trial)
        
        # Get the user's top tracks
        top_tracks = list(track_data[track_data['User'] == user]['URI'])
        print(top_tracks)
        for track_num in range(0, len(top_tracks), 5):

            # Extract the track IDs from the top tracks
            seed_tracks = top_tracks[track_num:track_num+5]
            
            # Get personalized recommendations based on the user's seed tracks
            recommendations = sp.recommendations(limit=100, seed_tracks=seed_tracks)
            
            for idx, track in enumerate(recommendations['tracks']):
                recommended_tracks.loc[len(recommended_tracks)] = {
                    'user': user,
                    'trial': trial,
                    'rank': idx,
                    'top_track_range': f'{track_num} - {track_num + 4}',
                    'track': track.get('name', ''),
                    'artist': track['artists'][0].get('name', ''),
                    'album': track['album'].get('name', ''),
                    'image': track['album']['images'][0]['url'] if track['album']['images'] else '',
                    'uri': track.get('uri', ''),
                    'popularity': track.get('popularity', '')
                }

            
        print("Finished trial:", trial)
    
    print("Finished processing user:", user)


recommended_tracks.to_csv('C:\\Users\\unrob\\OneDrive - Universiteit Utrecht\\Thesis\\Spotify Recommender\\Spotify_data_analysis\\recommendation_data.csv')

