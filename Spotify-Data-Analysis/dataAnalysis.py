# -*- coding: utf-8 -*-
"""
Created on Mon Jun 26 17:55:05 2023

@author: unrob
"""
import os
import sys
current_dir = os.path.dirname(os.path.abspath(__file__))

parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)

import pandas as pd

track_data = pd.read_csv('C:\\Users\\unrob\\OneDrive - Universiteit Utrecht\\Thesis\\Script Submissions\\Spotify_data_analysis\\track_data.csv')

import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import main

sns.set(font=main.get_font())
customPalette  = sns.set_palette(main.get_color_palette(), color_codes=True)
cm = main.get_color_mapping()


plt.figure()

# Define the order of 'Type' and 'LFMPopularity' categories
type_order = ['recent', 'short_term', 'medium_term', 'long_term']
popularity_order = ['tail', 'mid', 'head']

# Define custom colors for the LFMPopularity categories
popularity_colors = {'tail': cm['r'], 'mid': cm['g'], 'head': cm['b']}

# Create the count barplot with custom colors and order
sns.countplot(data=track_data, x='Type', hue='LFMPopularity',
              palette=popularity_colors, hue_order=popularity_order, order=type_order)


# Set labels and title
plt.xlabel('Type')
plt.ylabel('Frequency')
plt.savefig("C:/Users/unrob/OneDrive - Universiteit Utrecht/Thesis/Script Submissions/Spotify_data_analysis/Plots/category_counts_per_type.png")
plt.show()




# Set the figure size
plt.figure(figsize=(10, 6))

nan_ratio = track_data.groupby(['User', 'Type'])['LFMPopularity'].apply(lambda x: x.isna().mean()).reset_index()

# Calculate the mean, standard deviation, minimum, and maximum of nan_ratio per user for each type
nan_stats = nan_ratio.groupby('Type')['LFMPopularity'].agg(['mean', 'std', 'min', 'max']).reset_index()

# Create a bar plot to compare the nan ratio per user for each type
sns.barplot(data=nan_ratio, x='Type', y='LFMPopularity', palette=customPalette, order=type_order)

# Set labels and title
plt.xlabel('Type')
plt.ylabel('Ratio')


# Add text to display the mean and standard deviation
for idx, row in nan_stats.iterrows():
    type_val = row['Type']
    mean = row['mean']
    std = row['std']
    min_p = row['min']
    max_p = row['max']
    
    plt.text(type_order.index(type_val), mean - 0.2, f'{mean:.2f}', ha='center', va='bottom', fontsize=16, color='white')
    plt.text(type_order.index(type_val), mean - 0.25, f'Std: {std:.2f}', ha='center', va='bottom', fontsize=16, color='white')
    plt.text(type_order.index(type_val), mean - 0.3, f'Min: {min_p:.2f}', ha='center', va='bottom', fontsize=16, color='white')
    plt.text(type_order.index(type_val), mean - 0.35, f'Max: {max_p:.2f}', ha='center', va='bottom', fontsize=16, color='white')

# Display the plot
plt.savefig("C:/Users/unrob/OneDrive - Universiteit Utrecht/Thesis/Script Submissions/Spotify_data_analysis/Plots/ratio_unknown_spotify_items.png")
plt.show()

'''
# Set the figure size
plt.figure(figsize=(10, 6))

# Create a scatter plot with hue
sns.scatterplot(data=track_data, x=np.log1p(track_data['LFMInteractions']), y='SpotifyPopularity', hue='LFMPopularity')

# Set labels and title
plt.xlabel('LFM Interactions (log scale)')
plt.ylabel('SP Popularity')
plt.title('Scatter Plot: LFM Interactions (log scale) vs SP Popularity (Hue: LFMPopularity)')

# Display the plot
plt.show()


# Compute the correlation between 'SpotifyPopularity' and 'LFMInteractions'
correlation = track_data['SpotifyPopularity'].corr(track_data['LFMInteractions'])

# Print the correlation
print(f"Correlation between SpotifyPopularity and LFMInteractions: {correlation:.2f}")
'''

'''
# Group by 'User' and select unique tracks
user_unique_tracks = track_data.groupby('User')['Track'].unique()

# Find the intersection of unique tracks for all users
common_tracks = set.intersection(*map(set, user_unique_tracks))

# Filter the dataframe to include only rows where 'Title' is in the common tracks
filtered_data = track_data[track_data['Track'].isin(common_tracks)]

# Group by 'Title' and check if 'URI' exists for both users
title_groups = filtered_data.groupby('Track').filter(lambda x: x['URI'].notna().all() and x['User'].nunique() == 2)

# Select the 'Title' and 'SpotifyPopularity' columns
result = title_groups[['Track', 'SpotifyPopularity']].drop_duplicates()

# Print the result
print(result)
'''