# -*- coding: utf-8 -*-
"""
Created on Thu Jun 29 16:30:36 2023

@author: unrob
"""
import pandas as pd
import secrets
from flask import Flask, request, render_template, redirect, session, url_for
import spotipy


app = Flask(__name__)
app.secret_key = secrets.token_hex(16)
redirect_uri = 'http://localhost:8888/callback'
token_url = 'https://accounts.spotify.com/api/token'
auth_url = 'https://accounts.spotify.com/authorize'

client_id='here goes your Spotify client id'
client_secret='here goes your Spotify client secret'
# Set up Spotipy authentication credentials
scope = "user-read-private user-read-email user-read-recently-played user-top-read"

sp_oauth = spotipy.oauth2.SpotifyOAuth(client_id=client_id, client_secret=client_secret,
                                      redirect_uri=redirect_uri, scope=scope)

# Create a new Spotipy client with the access token
sp = spotipy.Spotify(client_credentials_manager=sp_oauth)



recommendation_data = pd.read_csv('C:\\Users\\unrob\\OneDrive - Universiteit Utrecht\\Thesis\\Spotify Recommender\\Spotify_data_analysis\\recommendation_data.csv')
track_data = pd.read_csv('C:\\Users\\unrob\\OneDrive - Universiteit Utrecht\\Thesis\\Spotify Recommender\\Spotify_data_analysis\\track_data.csv')


before_april_count = 0
total_tracks = 0
type_counts = {}
type_totals = {}

i = 0

for uri in recommendation_data['uri'].unique()[:1000]:
    # Print progress every 50 tracks
    if i % 50 == 0:
        print(f"Progress: {i} tracks...")
    if pd.notna(uri):  # Check if the URI is not NaN
        track = sp.track(uri)
        release_date = track['album']['release_date']
        
        # Check if the track was released before March 2020
        if release_date < '2020-04-01':
            before_april = True
            before_april_count += 1
        else:
            before_april = False
            
        # Get the track types
        track_types = track_data.loc[track_data['URI'] == uri, 'Type'].tolist()
        for track_type in track_types:
            if before_april:
                if track_type in type_counts:
                    type_counts[track_type] += 1
                else:
                    type_counts[track_type] = 1
                
            # Update the total count for each track type
            if track_type in type_totals:
                type_totals[track_type] += 1
            else:
                type_totals[track_type] = 1
        
        total_tracks += 1
    
    i += 1

# Compute the overall ratio of tracks released before March 2020
ratio_before_april = before_april_count / total_tracks
print(f"Overall ratio of tracks released before April 2020: {ratio_before_april}")

# Compute the ratio and total count grouped by track type
print("Ratio of tracks released before April 2020 (grouped by track type):")
for track_type, count in type_counts.items():
    total_count = type_totals[track_type]
    ratio = count / total_count
    print(f"{track_type}: {ratio} (Total: {total_count})")
