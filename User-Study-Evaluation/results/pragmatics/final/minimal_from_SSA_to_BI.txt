strict digraph "" {
        graph [fontname="helvetica",
			labelloc=b,
			ranksep="0.5", nodesep="0.4"];
	    node [fontname="helvetica"];
        edge [fontname="helvetica"];

	subgraph cluster_SSA {
		graph [color=darkgreen,
			label=SSA
		];
		"Q: Perc. Popularity"	[shape=box; color=red;];
		"Q: Familiarity"	[shape=box; color=red;];
		"Q: Familiarity" -> "Q: Perc. Popularity" 	[dir=both,color=grey,
			#label="r(100) = .51
        #p < .001
        #***"
        ];
		"Q: Perc. Fairness"	[shape=box];
		"Q: Discovery"	[shape=box];
        "Q: Discovery" -> "Q: Perc. Fairness"	[dir=both,color=grey,
        #label="r(100) = .29
        #p = .004
        #**"
];
		"Q: Perc. Rec. Quality"	[shape=box];
		"Q: Perc. Fairness" -> "Q: Perc. Rec. Quality"	[dir=both,color=grey,
			#label="r(100) = .50
        #p < .001
        #***"
];
		"Q: Familiarity" ->"Q: Discovery" 	[dir=both,color=grey,
			#label="r(100) = -.35
        #p < .001
        #***"  
        ];
		"Q: Discovery" -> "Q: Perc. Rec. Quality"	[dir=both,color=grey,
        #label="r(100) = .66
        #p < .001
        #***"
];
	}

	subgraph cluster_BI {
		graph [color=pink,
			label=BI
		];
		"Q: Openness to Sim. Rec."	[shape=box];
		"Q: Use Intention"	[shape=box];
		"Q: Openness to Sim. Rec." -> "Q: Use Intention"	[dir=both,
			label="r(100) = .52
p < .001
***"];
		"Q: Choice Listening Intention"	[shape=box];
		"Q: Openness to Sim. Rec." -> "Q: Choice Listening Intention"	[dir=both,
			label="r(100) = .65
p < .001
***"];
		"Q: Use Intention" -> "Q: Choice Listening Intention"	[dir=both,
			label="r(100) = .56
p < .001
***"];
	}

	"Q: Perc. Fairness" -> "Q: Openness to Sim. Rec."	[label="r(100) = .35
p < .001
***"];
	"Q: Perc. Fairness" -> "Q: Use Intention"	[label="r(100) = .33
p < .001
***"];
	"Q: Perc. Fairness" -> "Q: Choice Listening Intention"	[label="r(100) = .43
p < .001
***"];
	"Q: Discovery" -> "Q: Openness to Sim. Rec."	[label="r(100) = .53
p < .001
***"];
	"Q: Discovery" -> "Q: Use Intention"	[label="r(100) = .70
p < .001
***"];
	"Q: Discovery" -> "Q: Choice Listening Intention"	[label="r(100) = .52
p < .001
***"];
	"Q: Perc. Rec. Quality" -> "Q: Openness to Sim. Rec."	[label="r(100) = .69
p < .001
***"];
	"Q: Perc. Rec. Quality" -> "Q: Use Intention"	[label="r(100) = .67
p < .001
***"];
	"Q: Perc. Rec. Quality" -> "Q: Choice Listening Intention"	[label="r(100) = .80
p < .001
***"];

}

