
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 13 13:39:09 2023

@author: unrob
"""

import os
import sys
import datetime
import matplotlib.font_manager as fm
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np


abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)


from scripts import Choice_analysis
from scripts import Choice_recommendation_comparison
from scripts import process_raw_questionnaires
from scripts import SEM
from scripts import pragmatic_evaluation as pe
from scripts import Ranking_analyser as RA
from scripts import profile_and_recommendation_analysis as PRA
from scripts import pragmatic_evaluation_models as pe_models
from scripts import pragmatic_evaluation_models_small as pes_models
from scripts import post_hocs as ph
from scripts import SEM_models
from scripts import additional_tests as at

# Adjust display settings to prevent truncation
pd.set_option('display.max_columns', None)  # Display all columns
pd.set_option('display.expand_frame_repr', False)  # Prevent horizontal truncation
pd.set_option('display.max_rows', None)  # Display all rows
pd.set_option('display.width', None)  # Allow the output to span multiple lines
pd.set_option('display.max_colwidth', None)
np.set_printoptions(threshold=sys.maxsize)



# Step 1: Define the font path for "opensans"
font_path = 'C:/Users/unrob/OneDrive - Universiteit Utrecht/Thesis/Script Submissions/Recommenders/Misc/fonts/OpenSans.ttf'  # Replace with the actual path to "opensans.ttf"
fm.fontManager.addfont(font_path)

# Step 2: Load the font using the font manager
prop = fm.FontProperties(fname=font_path)


def get_font():
    return prop.get_name()

custom_color_palette = ["#0047B3", "#B30000","#2D683A", "#5C3E8E", "#FF6600", "#008C8C", "#4D4D4D", "#7C2855"]
#custom_color_palette = ["#B3D1FF", "#FF9999","#A5C9A2", "#FFB6D9", "#FFD9B3", "#B5E7E7", "#CFCFCF", "#C6B2D6"]
color_mapping = {
    'b': custom_color_palette[0],
    'r': custom_color_palette[1],
    'g': custom_color_palette[2],
    'p': custom_color_palette[3],
    'o': custom_color_palette[4],
    'c': custom_color_palette[5],
    'gray': custom_color_palette[6],
    'dark_p': custom_color_palette[7]
}

def get_color_palette():
    return sns.color_palette(custom_color_palette)

def get_color_mapping():
    return color_mapping

sns.set(font=get_font())
customPalette  = sns.set_palette(get_color_palette(), color_codes=True)
cm = get_color_mapping()


mappings = np.load("data/Preprocessed/mappings.npz", allow_pickle=True)
dec_mappings = [mappings['user_index_map_inv'].item(), mappings['track_index_map_inv'].item()]
user_index_map, track_index_map = dec_mappings

del user_index_map

spids_ids = pd.read_csv("data/Preprocessed/spotify_uris.csv")
track_popularities = pd.read_csv("data/Preprocessed/track_popularity.csv")

import pandas as pd

study_stats = pd.read_csv("data/Processed/study_stats.csv")
study_stats_w_facs = pd.read_csv("data/Processed/study_stats_w_factors.csv")
user_stats = pd.read_csv("data/Processed/user_stats.csv")
recommendation_similarities = pd.read_csv("data/Processed/recommendation_similarities.csv")


'''
#SEM, not currently used since not feasible with data

#SEM.explore_cfa(study_stats)
#factor_loadings = SEM.do_cfa(study_stats)
#factor_loadings = pd.read_csv('results/CFA_init.csv')
#print(factor_loadings)
#new_df = pe.create_factors(study_stats, factor_loadings)

#SEM.do_sem(study_stats, cfa_model=SEM_models.cfa_model_C_and_Questionnaires, sem_model=SEM_models.sem_model_C_and_Questionnaires, save_path='results/SEM_init.csv')
#SEM.do_cfa(study_stats, cfa_model=SEM_models.cfa_model_C_and_Questionnaires)
#SEM.do_fa_cfa(study_stats, save_path="data/Processed/study_stats_w_cfa_factors.csv")
'''

'''
#for testing purposes
def test_some_columns(df):
    filtered_columns = ['rec_mean_interactions', 'rec_median_interactions', 'rec_popularity_lift', 
                      'rec_jensen_shannon', 'rec_head_ratio', 'rec_mid_ratio', 'rec_tail_ratio', 'time_spent', 
                      'choice_mean_interactions', 'choice_median_interactions', 'choice_popularity_lift', 
                      'choice_jensen_shannon', 'choice_head_ratio', 'choice_mid_ratio', 'choice_tail_ratio', 
                      'Q_Perceived_Popularity', 'Q_PerceivedFairness', 'Q_Discovery', 'Q_Familiarity', 'Q_PerceivedRecommendationQuality', 
                      'Q_RecommendationSatisfaction', 'Q_ChoiceSatisfaction', 'Q_PerceivedSystemEffectiveness', 
                      'Q_OpennessSimilarRecommendations', 'Q_UseIntention', 'Q_ChoiceListeningIntention']

    
    # Ensure the order of conditions
    condition_order = ["base", "fair", "cp"]
    for col in filtered_columns:
        #print(df[['condition', col]])
        plt.figure(figsize=(10, 6))
        sns.barplot(x='condition', y=col, data=df, order=condition_order)  # Use the specified order
        plt.title(f'{col} values by condition')
        plt.xticks(rotation=90)
        plt.draw()  # Draw the initial plot

        event = plt.waitforbuttonpress()
        plt.close()



#plt.switch_backend('Qt5Agg')           
#test_some_columns(study_stats_w_facs[study_stats_w_facs['user_type']=='Niche'])
     
'''   







#PRA.analyse_profiles(study_stats.copy()[['user_id', 'age', 'gender', 'user_type',
#                                         'prof_mean_interactions', 'prof_median_interactions', 'prof_num_interactions', 'prof_head_ratio', 'prof_mid_ratio', 'prof_tail_ratio',
#                                         'Validation_known', 'Validation_match'
#                                         ]], custom_color_palette, color_mapping)

#PRA.analyse_recommendations(study_stats.copy()[['user_id', 'age', 'gender', 'user_type',
#                                                'prof_mean_interactions', 'prof_median_interactions', 'prof_num_interactions', 'prof_head_ratio', 'prof_mid_ratio', 'prof_tail_ratio', 
#                                                 'post_num', 'condition', 'rec_mean_interactions', 'rec_median_interactions', 'rec_popularity_lift', 
#                                                 'rec_jensen_shannon', 'rec_head_ratio', 'rec_mid_ratio', 'rec_tail_ratio', 
#                                                 ]], recommendation_similarities, custom_color_palette, color_mapping)

#RA.analyse_ranking(study_stats, colorPalette=custom_color_palette)



#rename = {'Q_Familiarity' : 'Q: Familiarity',
#          'Q_Perceived_Popularity' : 'Q: Perc. Popularity'}

#for effect, metric_name in zip(['Q_Perceived_Popularity', 'Q_Familiarity'], ['Popularity', 'Familiarity']):
#    ph.paired_t_test(study_stats_w_facs, [effect], f"plots/Condition/PostHoc_{metric_name}.png" , 'Score on Likert Scale (-3 to 3)', custom_color_palette, color_mapping, ylim=(-3,5), figsize=(6,6), questionnaire=True)

"""
The normal includes all pathes as defined in the SEM section
The algorithmic only includes paths between PC, OSA, and INT
The simplified avoids those edges
The from OSA shows only paths coming from OSA, similar to PC and INT
The QUestionnaires shows only within the three questionnaire groups

The small should cover the entire simplified model

The last two: _Questionnaires_from_SSA and _Questionnaires_from_EXP try to further simplify the Questionnaires category
"""


Anova_Models = [pe_models.ANOVA_model, pe_models.algorithmic_ANOVA_model, pe_models.simplified_ANOVA_model,
                pes_models.minimal_ANOVA_model_from_OSA, pes_models.minimal_ANOVA_model_from_PC, 
                pes_models.minimal_ANOVA_model_to_INT, pes_models.minimal_ANOVA_model_Questionnaires,
                pes_models.minimal_ANOVA_model_Questionnaires_from_SSA, 
                pes_models.minimal_ANOVA_model_Questionnaires_from_SSA_only_SSA, pes_models.minimal_ANOVA_model_Questionnaires_from_SSA_to_EXP,
                pes_models.minimal_ANOVA_model_Questionnaires_from_SSA_to_BI, 
                pes_models.minimal_ANOVA_model_Questionnaires_from_EXP_to_BI,
                pes_models.minimal_ANOVA_model_musicality]

Correlation_Models = [pe_models.Correlation_model, pe_models.algorithmic_Correlation_model, pe_models.simplified_Correlation_model,
                pes_models.minimal_Correlation_model_from_OSA, pes_models.minimal_Correlation_model_from_PC, 
                pes_models.minimal_Correlation_model_to_INT, pes_models.minimal_Correlation_model_Questionnaires,
                pes_models.minimal_Correlation_model_Questionnaires_from_SSA, 
                pes_models.minimal_Correlation_model_Questionnaires_from_SSA_only_SSA, pes_models.minimal_Correlation_model_Questionnaires_from_SSA_to_EXP,
                pes_models.minimal_Correlation_model_Questionnaires_from_SSA_to_BI, 
                pes_models.minimal_Correlation_model_Questionnaires_from_EXP_to_BI,
                pes_models.minimal_Correlation_model_musicality]
suffixes = ["", "_algorithmic", "_simplified",
           "_minimal_from_OSA", "_minimal_from_PC",
           "_minimal_to_INT", "_minimal_Questionnaires",
           "_minimal_Questionnaires_from_SSA", 
           "_minimal_Questionnaires_from_SSA_only_SSA", "_minimal_Questionnaires_from_SSA_to_EXP",
           "_minimal_Questionnaires_from_SSA_to_BI", 
           "_minimal_Questionnaires_from_EXP_to_BI", "_minimal_muiscality"]

'''
for anova, correlation, suffix in zip(Anova_Models, Correlation_Models, suffixes):
    
    pe.compute_effects(study_stats_w_facs, list(study_stats_w_facs.columns), anova, correlation, 
                       f"results/pragmatics/pragmatic_evaluation{suffix}.csv")
    
    pragmatic_evaluation = pd.read_csv(f"results/pragmatics/pragmatic_evaluation{suffix}.csv")
    
    pe.create_directed_graph(pragmatic_evaluation, f"results/pragmatics/pragmatic_evaluation{suffix}.dot")
'''    




#at.discovery_for_fair(study_stats, study_stats_w_facs)
for pop_lift, pop_lift_label, questionnaire, questionnaire_label, savepath in zip(
        ['rec_popularity_lift']*2 + ['choice_popularity_lift'] * 5,
        ['R: Popularity Lift'] * 2 + ['C: Popularity Lift'] * 5,
        ['Q_ChoiceSatisfaction', 'Q_ChoiceListeningIntention', 'Q_RecommendationSatisfaction', 
         'Q_ChoiceSatisfaction', 'Q_PerceivedSystemEffectiveness', 'Q_OpennessSimilarRecommendations', 'Q_ChoiceListeningIntention'],
        ['Q: Choice Satisfaction', 'Q: Choice Listening Intention', 'Q: Recommendation Satisfaction', 
         'Q: Choice Satisfaction', 'Q: Perceived System Effectiveness', 'Q: Openness Similar Recommendations', 'Q: Choice Listening Intention'],
        ['scatter_rec_popularity_lift_ChoiceSatisfaction', 'scatter_rec_popularity_lift_ChoiceListeningIntention',  
         'scatter_choice_popularity_lift_RecommendationSatisfaction', 
         'scatter_choice_popularity_lift_ChoiceSatisfaction', 'scatter_choice_popularity_lift_PerceivedSystemEffectiveness', 
         'scatter_choice_popularity_lift_OpennessSimilarRecommendations', 'scatter_choice_popularity_lift_ChoiceListeningIntention']):
    save = 'plots/Scatter_Pop_Lift/' + savepath + '.png'

    at.plot_two_metrics(study_stats_w_facs, pop_lift , pop_lift_label, questionnaire, questionnaire_label,  save=save)


'''

for anova, correlation, suffix in zip(Anova_Models, Correlation_Models, suffixes):
    
    pe.compute_effects(study_stats_w_facs[study_stats_w_facs['user_type']=='Niche'], list(study_stats_w_facs.columns), anova, correlation, 
                       f"results/pragmatics/only_niche/pragmatic_evaluation{suffix}.csv")
    
    pragmatic_evaluation = pd.read_csv(f"results/pragmatics/only_niche/pragmatic_evaluation{suffix}.csv")
    
    pe.create_directed_graph(pragmatic_evaluation, f"results/pragmatics/only_niche/pragmatic_evaluation{suffix}.dot")
    


for anova, correlation, suffix in zip(Anova_Models, Correlation_Models, suffixes):
    
    pe.compute_effects(study_stats_w_facs[study_stats_w_facs['user_type']=='Diverse'], list(study_stats_w_facs.columns), anova, correlation, 
                       f"results/pragmatics/only_diverse/pragmatic_evaluation{suffix}.csv")
    
    pragmatic_evaluation = pd.read_csv(f"results/pragmatics/only_diverse/pragmatic_evaluation{suffix}.csv")
    
    pe.create_directed_graph(pragmatic_evaluation, f"results/pragmatics/only_diverse/pragmatic_evaluation{suffix}.dot")


'''

print(list(study_stats_w_facs.columns))
#print(list(study_stats.columns))