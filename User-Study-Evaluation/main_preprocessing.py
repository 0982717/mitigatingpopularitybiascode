# -*- coding: utf-8 -*-
"""
Created on Thu Jul 13 13:39:09 2023

@author: unrob
"""

import os
import sys
import datetime
import matplotlib.font_manager as fm
import seaborn as sns
import pandas as pd
import numpy as np


abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)
sys.path.append(dname)

from scripts import Choice_analysis
from scripts import Choice_recommendation_comparison
from scripts import process_raw_questionnaires
from scripts.factor_creation import create_factors



# Adjust display settings to prevent truncation
pd.set_option('display.max_columns', None)  # Display all columns
pd.set_option('display.expand_frame_repr', False)  # Prevent horizontal truncation
pd.set_option('display.max_rows', None)  # Display all rows
pd.set_option('display.width', None)  # Allow the output to span multiple lines



# Step 1: Define the font path for "opensans"
font_path = 'C:/Users/unrob/OneDrive - Universiteit Utrecht/Thesis/Script Submissions/Recommenders/Misc/fonts/OpenSans.ttf'  # Replace with the actual path to "opensans.ttf"
fm.fontManager.addfont(font_path)

# Step 2: Load the font using the font manager
prop = fm.FontProperties(fname=font_path)


def get_font():
    return prop.get_name()

custom_color_palette = ["#0047B3", "#B30000","#2D683A", "#5C3E8E", "#FF6600", "#008C8C", "#4D4D4D", "#7C2855"]
#custom_color_palette = ["#B3D1FF", "#FF9999","#A5C9A2", "#FFB6D9", "#FFD9B3", "#B5E7E7", "#CFCFCF", "#C6B2D6"]
color_mapping = {
    'b': custom_color_palette[0],
    'r': custom_color_palette[1],
    'g': custom_color_palette[2],
    'p': custom_color_palette[3],
    'o': custom_color_palette[4],
    'c': custom_color_palette[5],
    'gray': custom_color_palette[6],
    'dark_p': custom_color_palette[7]
}

def get_color_palette():
    return sns.color_palette(custom_color_palette)

def get_color_mapping():
    return color_mapping

sns.set(font=get_font())
customPalette  = sns.set_palette(get_color_palette(), color_codes=True)
cm = get_color_mapping()


mappings = np.load("data/Preprocessed/mappings.npz", allow_pickle=True)
dec_mappings = [mappings['user_index_map_inv'].item(), mappings['track_index_map_inv'].item()]
user_index_map, track_index_map = dec_mappings

del user_index_map

spids_ids = pd.read_csv("data/Preprocessed/spotify_uris.csv")
track_popularities = pd.read_csv("data/Preprocessed/track_popularity.csv")

import pandas as pd

recommendation_similarities = pd.DataFrame()
recommendation_stats = pd.DataFrame()
time_data = pd.DataFrame()
track_choices = pd.DataFrame()
recommendation_uris = pd.DataFrame()
user_profiles = pd.DataFrame()


pre_questionnaire = pd.DataFrame()
post_questionnaire = pd.DataFrame()
final_questionnaire = pd.DataFrame()


def load_data(questionnaires=True, interaction_data=True):
    if questionnaires == True:
        global pre_questionnaire
        global post_questionnaire
        global final_questionnaire
        
        pre_questionnaire = pd.read_csv("data/Questionnaires/Masterthesis-Pre-Questionnaire_October 2, 2023_02.35.csv")
        pre_questionnaire = pre_questionnaire.iloc[2:]
        post_questionnaire = pd.read_csv("data/Questionnaires/Masterthesis-Post-Questionnaire_October 2, 2023_02.34.csv")
        post_questionnaire = post_questionnaire.iloc[2:]
        final_questionnaire = pd.read_csv('data/Questionnaires/Masterthesis-Final-Questionnaire_October 2, 2023_02.36.csv')
        final_questionnaire = final_questionnaire.iloc[2:]
        
    if interaction_data == True:
        global recommendation_similarities
        global recommendation_stats
        global time_data
        global track_choices
        global user_profiles
        global recommendation_uris
        csv_directory = 'data/Interactiondata'

        # List of dataframe names
        dataframe_names = ['recommendation_similarities', 'recommendation_stats', 'time_data', 'track_choices', 'user_profiles', 'recommendation_uris']
        
        # Function to filter files by name
        def filter_csv_files_by_name(file_list, name):
            return [file for file in file_list if file.startswith(name)]
        
        # List all CSV files in the directory
        all_csv_files = os.listdir(csv_directory)
        
        # Load and merge the CSV files for each dataframe
        merged_dataframes = {}
        for name in dataframe_names:
            filtered_files = filter_csv_files_by_name(all_csv_files, name)
            if filtered_files:
                dfs = [pd.read_csv(os.path.join(csv_directory, file)) for file in filtered_files]
                merged_dataframes[name] = pd.concat(dfs, ignore_index=True)
        
        # Access the merged dataframes by name
        
        recommendation_similarities = merged_dataframes.get('recommendation_similarities')
        recommendation_stats = merged_dataframes.get('recommendation_stats')
        time_data = merged_dataframes.get('time_data')
        track_choices = merged_dataframes.get('track_choices')
        user_profiles = merged_dataframes.get('user_profiles')
        recommendation_uris = merged_dataframes.get('recommendation_uris')

load_data()
      

def filter_values():
    global recommendation_similarities
    global recommendation_stats
    global time_data
    global track_choices
    global user_profiles
    
    initial_user_ids = user_profiles['user_id'].unique()
    print('Initial number of users:', len(initial_user_ids))
    
    # Filter recommendation_similarities and recommendation_stats
    recommendation_similarities = recommendation_similarities[recommendation_similarities['user_id'].isin(initial_user_ids)]
    recommendation_stats = recommendation_stats[recommendation_stats['user_id'].isin(initial_user_ids)]
    
    # Filter time_data and track_choices
    time_data = time_data[time_data['user_id'].isin(initial_user_ids)]
    track_choices = track_choices[track_choices['user_id'].isin(initial_user_ids)]
    
    print('Number of users after filtering recommendation data:', len(recommendation_similarities['user_id'].unique()))
    print('Number of users after filtering time and track data:', len(time_data['user_id'].unique()))
    
    # Remove users with less than three rows in track_choices and time_data
    track_choices_counts = track_choices['user_id'].value_counts()
    time_data_counts = time_data['user_id'].value_counts()
    
    valid_users = set(track_choices_counts[track_choices_counts >= 3].index) & set(time_data_counts[time_data_counts >= 3].index)
    
    track_choices = track_choices[track_choices['user_id'].isin(valid_users)]
    time_data = time_data[time_data['user_id'].isin(valid_users)]
    
    print('Number of users after removing users with less than three rows:', len(track_choices['user_id'].unique()))
    
    # Filter user_profiles
    user_profiles = user_profiles[user_profiles['user_id'].isin(valid_users)]
    
    print('Final number of users:', len(user_profiles['user_id'].unique()))
    
    # Remove non-valid rows from recommendation_similarities and recommendation_stats
    recommendation_similarities = recommendation_similarities[recommendation_similarities['user_id'].isin(valid_users)]
    recommendation_stats = recommendation_stats[recommendation_stats['user_id'].isin(valid_users)]
    
    # Remove non-valid rows from time_data and track_choices
    time_data = time_data[time_data['user_id'].isin(valid_users)]
    track_choices = track_choices[track_choices['user_id'].isin(valid_users)]
    
    # Remove users not present in any of the filtered DataFrames
    common_users = set(recommendation_similarities['user_id']).union(set(recommendation_stats['user_id'])).union(set(time_data['user_id'])).union(set(track_choices['user_id']))
    user_profiles = user_profiles[user_profiles['user_id'].isin(common_users)]


filter_values()


def analyse_choices():
    global track_choices
    track_choices = Choice_analysis.extract_choice_track_ids(track_choices, spids_ids)
    
    choice_stats = Choice_analysis.choice_stats(track_choices, user_profiles, track_popularities)
    
    return choice_stats
 
choice_stats = analyse_choices()

stats, user_stats, recommendation_similarities = process_raw_questionnaires.process_data(pre_questionnaire, post_questionnaire, final_questionnaire, recommendation_stats, 
                                                                             time_data, user_profiles, choice_stats, recommendation_similarities)

stats.to_csv("data/Processed/study_stats.csv")
user_stats.to_csv("data/Processed/user_stats.csv")
recommendation_similarities.to_csv("data/Processed/recommendation_similarities.csv")
def compare_choices_and_recommendations():
    Choice_recommendation_comparison.compare_choices_recommendations(choice_stats, recommendation_stats)


processed_study_stats = create_factors(stats)

processed_study_stats.to_csv("data/Processed/study_stats_w_factors.csv")



#compare_choices_and_recommendations()
