# -*- coding: utf-8 -*-
"""
Created on Mon Aug 21 14:17:48 2023

@author: unrob
"""
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from scripts.post_hocs import paired_t_test
def analyse_profiles(profile_stats, colorPalette=None, color_mapping=None):
    profile_stats.drop_duplicates(inplace=True)
    print('Number of participants:')
    print(len(profile_stats['user_id'].unique()))
    print()
    
    print('Ages:')
    print(profile_stats['age'].agg(['mean', 'std', 'min', 'max']))
    #report according to https://academic.oup.com/ageing/article/46/4/576/3787761
    print()
    
    print('Gender: (1=male, 2=female, 3=non-binary, 4=prefer not to say)')
    print(profile_stats.groupby('gender').agg({'gender':'count'}))
    print()
    
    print('Profile Statistics:')
    print(profile_stats[['prof_mean_interactions', 'prof_median_interactions', 'prof_num_interactions']].describe())
    print()
    
    # Create boxenplots using Seaborn
    plt.figure(figsize=(10, 6))
    
    if colorPalette:
        sns.set_palette(colorPalette)
    
    plt.subplot(131)
    sns.boxenplot(data=profile_stats, y='prof_mean_interactions', k_depth='full')
    plt.ylabel('Mean Popularity', fontsize=18)
    plt.title('Mean Popularity', fontsize=18)
    
    plt.subplot(132)
    sns.boxenplot(data=profile_stats, y='prof_median_interactions', k_depth='full')
    plt.ylabel('Median Popularity', fontsize=18)
    plt.title('Median Popularity', fontsize=18)
    
    plt.subplot(133)
    sns.boxenplot(data=profile_stats, y='prof_num_interactions', k_depth='full')
    plt.ylabel('# of Songs', fontsize=18)
    plt.title('Number of Songs in Profile', fontsize=18)
    
    plt.tight_layout()
    plt.savefig(f"plots/User_Profiles/ProfilesDescriptive.png")
    plt.show()
    
    print('User Types')
    print(profile_stats.groupby('user_type').agg({'user_type':'count'}))
    print()
    
    print('Profile Ratios')
    print(profile_stats[['prof_head_ratio', 'prof_mid_ratio', 'prof_tail_ratio']].describe())
    print()
    # Melt the DataFrame to transform the 'head', 'mid', and 'tail' columns into a single 'Type' column
    melted_df = profile_stats.melt(id_vars=['user_id'], value_vars=['prof_head_ratio', 'prof_mid_ratio', 'prof_tail_ratio'], var_name='Popularity', value_name='Ratio')
    
    popularity_order = ['prof_tail_ratio', 'prof_mid_ratio', 'prof_head_ratio']
    
    if color_mapping is None:
        popularity_colors = {'prof_tail_ratio': 'red', 'prof_mid_ratio': 'green', 'prof_head_ratio': 'blue'}
    else:
        popularity_colors = {'prof_tail_ratio': color_mapping['r'], 'prof_mid_ratio': color_mapping['g'], 'prof_head_ratio': color_mapping['b']}
    
    plt.figure(figsize=(10, 6))
    
    # Create the bar plot with error bars to show variability
    sns.barplot(data=melted_df, x='Popularity', y='Ratio', palette=popularity_colors, order=popularity_order, errorbar='sd')
    
    # Set labels and title
    plt.xlabel('Popularity', fontsize=18)
    plt.ylabel('Ratio', fontsize=18)

    
    new_labels = ['tail', 'mid', 'head']

    plt.xticks(plt.xticks()[0], new_labels, fontsize=16)

    
    plt.tight_layout()
    plt.savefig(f"plots/User_Profiles/ProfilesCategoryRatios.png")
    plt.show()

    print('Validation statistics')
    print(profile_stats[['Validation_known', 'Validation_match']].describe())
    print('Number of participants with validation known < 0:')
    print(len(profile_stats[profile_stats['Validation_known']<0]))
    print('Number of participants with validation match < 0:')
    print(len(profile_stats[profile_stats['Validation_match']<0]))
    

    
def analyse_recommendations(stats, comparison_stats, colorPalette=None, color_mapping=None):
    print(comparison_stats[['algorithm_1', 'algorithm_2', 'jaccard_similarity']].groupby(['algorithm_1', 'algorithm_2']).describe())
    print()
    
    prof_stats = stats[['user_id', 'age', 'gender', 'user_type', 'prof_mean_interactions',
           'prof_median_interactions', 'prof_num_interactions', 'prof_head_ratio',
           'prof_mid_ratio', 'prof_tail_ratio']].drop_duplicates()
    rec_stats = stats[['user_id', 'post_num', 'condition',
    'rec_mean_interactions', 'rec_median_interactions',
    'rec_popularity_lift', 'rec_jensen_shannon', 'rec_head_ratio',
    'rec_mid_ratio', 'rec_tail_ratio']]
     

    
        
    # Select the common columns between the two DataFrames
    common_columns = ['user_id', 'mean_interactions', 'median_interactions','head_ratio', 'mid_ratio', 'tail_ratio']
    
    # Rename the columns in 'prof_stats' to match the column names in 'rec_stats'
    prof_stats_renamed = prof_stats.rename(columns={'prof_mean_interactions': 'mean_interactions',
                                                    'prof_median_interactions': 'median_interactions',
                                                    'prof_head_ratio': 'head_ratio',
                                                    'prof_mid_ratio': 'mid_ratio',
                                                    'prof_tail_ratio': 'tail_ratio'})
    prof_stats_filtered = prof_stats_renamed[common_columns].copy()
    prof_stats_filtered['condition'] = 'profile'

    rec_stats_renamed = rec_stats.rename(columns={'rec_mean_interactions': 'mean_interactions',
                                                    'rec_median_interactions': 'median_interactions',
                                                    'rec_jensen_shannon' : 'jensen_shannon',
                                                    'rec_popularity_lift': 'popularity_lift',
                                                    'rec_head_ratio': 'head_ratio',
                                                    'rec_mid_ratio': 'mid_ratio',
                                                    'rec_tail_ratio': 'tail_ratio'})

    
    # Concatenate the DataFrames along rows
    combined_stats = pd.concat([rec_stats_renamed, prof_stats_filtered], ignore_index=True)
    
    print(combined_stats.columns)
    
    print('Recommendation Statistics:')
    reduced_stats = combined_stats[['condition','mean_interactions', 'median_interactions',
                     'popularity_lift', 'jensen_shannon']]
    
    for effect, measure_name, ylabel in  zip(['mean_interactions', 'median_interactions',
                     'popularity_lift', 'jensen_shannon'],
                                     ['Mean', 'Median', 'PopLift', 'JensenShannon'], ['Mean', 'Median', 'Popularity Lift', 'Jensen-Shannon Divergence']):
        print(effect)
        print(reduced_stats[['condition', effect]].groupby('condition').describe())
        paired_t_test(reduced_stats[reduced_stats['condition']!='profile'], [effect], save = f"plots/Recommendations/ConditionsPostHoc{measure_name}.png", 
                      colorPalette=colorPalette, color_mapping=color_mapping, figsize=(6,6), ylabel=ylabel)
        print()
    print()
    algorithm_order = ['profile', 'base', 'fair', 'cp']

    if color_mapping is None:
        algorithm_colors = {'profile': 'blue', 'base': 'gray', 'fair': 'orange', 'cp':'green'}
    else:
        algorithm_colors = {'profile': color_mapping['b'], 'base': color_mapping['gray'], 'fair': color_mapping['o'], 'cp': color_mapping['g']}
    
    
    if colorPalette:
        sns.set_palette(colorPalette)
        
    # Create boxenplots using Seaborn for descriptive statistics
    plt.figure(figsize=(10, 10))
    

    plt.subplot(211)
    sns.boxenplot(data=combined_stats, x='condition', y='mean_interactions', order=algorithm_order, palette=algorithm_colors, k_depth='full')
    plt.ylabel('Mean Popularity', fontsize=18)
    plt.xlabel('Condition', fontsize=18)
    plt.title('Mean Popularity', fontsize=22)
    
    plt.subplot(212)
    sns.boxenplot(data=combined_stats, x='condition', y='median_interactions', order=algorithm_order, palette=algorithm_colors, k_depth='full')
    plt.ylabel('Median Popularity', fontsize=18)
    plt.xlabel('Condition', fontsize=18)
    plt.title('Median Popularity', fontsize=22)

    new_labels = ['Profiles', 'Base', 'FA*IR', 'CP']

    plt.xticks(plt.xticks()[0], new_labels, fontsize=16)
    
    plt.tight_layout()
    plt.savefig(f"plots/Recommendations/ConditionsDescriptive.png")
    plt.show()
    
    
    
    # Create boxenplots using Seaborn for user centred statistics
    plt.figure(figsize=(10, 10))
    

    plt.subplot(211)
    sns.boxenplot(data=combined_stats.dropna(), x='condition', y='popularity_lift', order=algorithm_order[1:], palette=algorithm_colors, k_depth='full')
    plt.ylabel('Popularity Lift', fontsize=18)
    plt.xlabel('Condition', fontsize=18)
    plt.title('Popularity Lift', fontsize=22)
    
    plt.subplot(212)
    sns.boxenplot(data=combined_stats.dropna(), x='condition', y='jensen_shannon', order=algorithm_order[1:], palette=algorithm_colors, k_depth='full')
    plt.ylabel('UPD', fontsize=18)
    plt.xlabel('Condition', fontsize=18)
    plt.title('User Popularity Deviation', fontsize=22)

    new_labels = ['Base', 'FA*IR', 'CP']

    plt.xticks(plt.xticks()[0], new_labels, fontsize=16)
    
    plt.tight_layout()
    plt.savefig(f"plots/Recommendations/ConditionsUserCentred.png")
    plt.show()
    
    
    print('Recommendation Ratios')
    reduced_stats = combined_stats[['condition','head_ratio',
                     'mid_ratio', 'tail_ratio']]
    print()
    for ratio, measure_name, ylabel in  zip(['head_ratio',
                     'mid_ratio', 'tail_ratio'], 
                                ['Head', 'Mid', 'Tail'], ['Head Ratio', 'Mid Ratio', 'Tail Ratio']):
        print(ratio)
        print(reduced_stats[['condition', ratio]].groupby('condition').describe())
        paired_t_test(reduced_stats[reduced_stats['condition']!='profile'], [ratio], save = f"plots/Recommendations/ConditionsPostHoc{measure_name}.png", 
                      colorPalette=colorPalette, color_mapping=color_mapping, figsize=(6,6), ylabel=ylabel)
        print()
    print()
    
    
    # Melt the DataFrame to transform the 'head', 'mid', and 'tail' columns into a single 'Type' column
    melted_df = combined_stats.melt(id_vars=['user_id', 'condition'], value_vars=['head_ratio', 'mid_ratio', 'tail_ratio'], var_name='Popularity', value_name='Ratio')
    
    popularity_order = ['tail_ratio', 'mid_ratio', 'head_ratio']
    
    if color_mapping is None:
        popularity_colors = {'tail_ratio': 'red', 'mid_ratio': 'green', 'head_ratio': 'blue'}
    else:
        popularity_colors = {'tail_ratio': color_mapping['r'], 'mid_ratio': color_mapping['g'], 'head_ratio': color_mapping['b']}
    
    plt.figure(figsize=(10, 6))
    
    # Create the bar plot with error bars to show variability
    sns.barplot(data=melted_df, x='condition', y='Ratio', hue='Popularity', palette=popularity_colors, order=algorithm_order, hue_order=popularity_order, errorbar='sd')
    
    # Set labels and title
    plt.xlabel('Condition', fontsize=18)
    plt.ylabel('Ratio', fontsize=18)

    
    new_labels = ['Profiles', 'Base', 'FA*IR', 'CP']

    plt.xticks(plt.xticks()[0], new_labels, fontsize=16)
    
    legend_labels = ['Tail', 'Mid', 'Head']  # Replace with your desired legend labels
    legend = plt.legend(legend_labels, title='Popularity', title_fontsize=18, fontsize=16)

    for handle, color in zip(legend.legendHandles, list(popularity_colors.values())):
        handle.set_color(color)
        handle.set_linewidth(16 * 0.6)
        
    
    plt.tight_layout()
    plt.savefig(f"plots/Recommendations/ConditionsCategoryRatios.png")
    plt.show()


    print('Number of user for which FA*IR created 92\% tail items:')
    print(len(reduced_stats[(reduced_stats['condition'] == 'fair') & (reduced_stats['tail_ratio'] == 0.92)]))
