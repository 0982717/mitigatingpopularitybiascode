# -*- coding: utf-8 -*-
"""
Created on Fri Sep 22 16:17:56 2023

@author: unrob
"""
import scipy.stats as stats
import seaborn as sns
import matplotlib.pyplot as plt


def discovery_for_fair(df, df_w_facs):
    # Filter rows where condition is 'fair'
    filtered_df = df[df['condition'] == 'fair']

    
    facs_df = df_w_facs.copy()[df_w_facs['condition']=='fair']
    
    valid_rows_dict = dict(zip(filtered_df['user_id'], filtered_df['Rank_Pl']))

    # Create a new 'Rank_Pl' column in the second DataFrame and update it with values from valid_rows
    facs_df['Rank_Pl'] = facs_df['user_id'].map(valid_rows_dict)
    print(facs_df[['Q_Discovery', 'Rank_Pl']].groupby('Rank_Pl').describe())
    print()
    
    # Perform independent t-tests based on 'Rank_Pl'
    rank_pl_values = facs_df['Rank_Pl'].unique()
    t_test_results = []

    for i in range(len(rank_pl_values)):
        for j in range(i+1, len(rank_pl_values)):
            group1 = facs_df[facs_df['Rank_Pl'] == rank_pl_values[i]]['Q_Discovery']
            group2 = facs_df[facs_df['Rank_Pl'] == rank_pl_values[j]]['Q_Discovery']
            t_stat, p_value = stats.ttest_ind(group1, group2)
            t_test_results.append({
                'Comparison': f'Rank_Pl {rank_pl_values[i]} vs Rank_Pl {rank_pl_values[j]}',
                'T-Statistic': t_stat,
                'P-Value': p_value
            })

    # Print the t-test results
    for result in t_test_results:
        print(result['Comparison'])
        print(f"T-Statistic: {result['T-Statistic']}")
        print(f"P-Value: {result['P-Value']}")
        print()


def plot_two_metrics(df, metric_1, metric_1_label, metric_2, metric_2_label, save=None):
    plt.figure(figsize=(8, 6))

    # Create a scatter plot
    sns.scatterplot(x=metric_1, y=metric_2, data=df, color='#0047B3')

    # Add a regression line
    sns.regplot(x=metric_1, y=metric_2, data=df, scatter=False, color="#B30000", label='Regression Line')

    # Set plot labels and title
    plt.xlabel(metric_1_label)
    plt.ylabel(metric_2_label)


    # Save the plot if save parameter is provided
    if save is not None:
        plt.savefig(save)

    # Display the plot
    plt.legend()
    plt.show()