# -*- coding: utf-8 -*-
"""
Created on Tue Jul 25 15:44:59 2023

@author: unrob
"""

import pandas as pd
import numpy as np

def process_data(raw_pre, raw_post, raw_final, recommendation_stats, time_data, user_profiles, choice_stats, recommendation_similarities):
    df_pre = pd.DataFrame()
    
    df_pre['user_id'] = raw_pre['user_id']

    df_pre.loc[df_pre['user_id'] == '2R9DWZW8ZX', 'user_id'] = 'GO0FBQPQ37'
    df_pre.loc[df_pre['user_id'] == '92MFA1V57I', 'user_id'] = '4JUIIVYHRN'
    df_pre.loc[df_pre['user_id'] == 'B1U1F82L7D', 'user_id'] = 'WFZTMKTIO8'
    df_pre.loc[df_pre['user_id']=='GWIV7BSD40', 'user_id'] = '50D6P5Z9M0'
    
    df_pre['age'] = raw_pre['Age']
    df_pre['gender'] = raw_pre['Gender']
    
    df_pre['MS01'] = pd.to_numeric(raw_pre['Q1_1']) - 4
    df_pre['MS02'] = pd.to_numeric(raw_pre['Q1_2']) - 4 
    df_pre['MS03'] = pd.to_numeric(raw_pre['Q1_3']) - 4
    df_pre['MS04'] = pd.to_numeric(raw_pre['Q1_4']) - 4
    df_pre['MS05'] = pd.to_numeric(raw_pre['Q1_5']) - 4
    df_pre['MS06'] = pd.to_numeric(raw_pre['Q1_6']) - 4
    df_pre['MS07'] = pd.to_numeric(raw_pre['Q1_7']) - 4
    df_pre['MS08'] = pd.to_numeric(raw_pre['Q1_8']) - 4
    df_pre['MS09'] = pd.to_numeric(raw_pre['Q1_9']) - 4
    df_pre['MS10'] = pd.to_numeric(raw_pre['Q1_10']) - 4
    df_pre['MS11'] = pd.to_numeric(raw_pre['Q1_11']) - 4
    df_pre['MS12'] = pd.to_numeric(raw_pre['Q1_6.1']) - 4
    df_pre['MS13'] = pd.to_numeric(raw_pre['Q1_12']) - 4
    df_pre['MS14'] = pd.to_numeric(raw_pre['Q1_13']) - 4
    df_pre['MS15'] = pd.to_numeric(raw_pre['Q1_14']) - 4
    df_pre['MS16'] = pd.to_numeric(raw_pre['Q2_1']) - 4
    df_pre['MS17'] = pd.to_numeric(raw_pre['Q3_1']) - 4
    df_pre['MS18'] = pd.to_numeric(raw_pre['Q4_1']) - 4
    df_pre['ME01'] = pd.to_numeric(raw_pre['Q1_1.1']) - 4
    df_pre['ME02'] = pd.to_numeric(raw_pre['Q1_2.1']) - 4
    df_pre['ME03'] = pd.to_numeric(raw_pre['Q1_3.1']) - 4
    df_pre['ME04'] = pd.to_numeric(raw_pre['Q1_4.1']) - 4
    df_pre['ME05'] = pd.to_numeric(raw_pre['Q1_5.1']) - 4
    df_pre['ME06'] = pd.to_numeric(raw_pre['Q1_6.1']) - 4
    df_pre['ME07'] = pd.to_numeric(raw_pre['Q1_7.1']) - 4
    df_pre['ME08'] = pd.to_numeric(raw_pre['Q2_1.1']) - 4
    df_pre['ME09'] = pd.to_numeric(raw_pre['Q3_1.1']) - 4
    'Q1_6.1'
    df_post = pd.DataFrame()
    df_post['user_id'] = raw_post['user_id']
    df_post['condition'] = raw_post['condition']

    df_post['post_num'] = pd.to_numeric(raw_post['post_num']) -1
    
    
    
    df_post['A_fair'] = np.where(df_post['condition'] == 'fair', 1, 0)
    df_post['A_cp'] = np.where(df_post['condition'] == 'cp', 1, 0)
    
    df_post['PP01'] = pd.to_numeric(raw_post['1-PP_1']) - 4
    df_post['PP02'] = pd.to_numeric(raw_post['1-PP_2']) - 4
    df_post['PP03'] = pd.to_numeric(raw_post['1-PP_3']) - 4
    df_post['PP04'] = pd.to_numeric(raw_post['1-PP_4']) - 4
    df_post['PP05'] = pd.to_numeric(raw_post['1-PP_5']) - 4
    df_post['PF01'] = pd.to_numeric(raw_post['1-PF_1']) - 4
    df_post['PF02'] = pd.to_numeric(raw_post['1-PF_2']) - 4
    df_post['PF03'] = pd.to_numeric(raw_post['1-PF_3']) - 4
    df_post['D01'] = pd.to_numeric(raw_post['1-D_1']) - 4
    df_post['D02'] = pd.to_numeric(raw_post['1-D_2']) - 4
    df_post['D03'] = pd.to_numeric(raw_post['1-D_3']) - 4
    df_post['D04'] = pd.to_numeric(raw_post['1-D_4']) - 4
    df_post['D05'] = pd.to_numeric(raw_post['1-D_5']) - 4
    df_post['F01'] = pd.to_numeric(raw_post['1-F_1']) - 4
    df_post['F02'] = pd.to_numeric(raw_post['1-F_2']) - 4
    df_post['F03'] = pd.to_numeric(raw_post['1-F_3']) - 4
    df_post['F04'] = pd.to_numeric(raw_post['1-F_4']) - 4
    df_post['F05'] = pd.to_numeric(raw_post['1-F_5']) - 4
    df_post['PQ01'] = pd.to_numeric(raw_post['1-PQ_1']) - 4
    df_post['PQ02'] = pd.to_numeric(raw_post['1-PQ_2']) - 4
    df_post['PQ03'] = pd.to_numeric(raw_post['1-PQ_3']) - 4
    df_post['PQ04'] = pd.to_numeric(raw_post['1-PQ_4']) - 4
    df_post['PQ05'] = pd.to_numeric(raw_post['1-PQ_5']) - 4
    df_post['PQ06'] = pd.to_numeric(raw_post['1-PQ_6']) - 4
    df_post['PQ07'] = pd.to_numeric(raw_post['1-PQ_7']) - 4
    
    df_post['RS01'] = pd.to_numeric(raw_post['1-RS_1']) - 4
    df_post['RS02'] = pd.to_numeric(raw_post['1-RS_2']) - 4
    df_post['RS03'] = pd.to_numeric(raw_post['1-RS_3']) - 4
    df_post['RS04'] = pd.to_numeric(raw_post['1-RS_4']) - 4
    df_post['RS05'] = pd.to_numeric(raw_post['1-RS_5']) - 4
    df_post['RS06'] = pd.to_numeric(raw_post['1-RS_6']) - 4
    df_post['RS07'] = pd.to_numeric(raw_post['1-RS_7']) - 4
    df_post['CS01'] = pd.to_numeric(raw_post['1-CS_1']) - 4
    df_post['CS02'] = pd.to_numeric(raw_post['1-CS_2']) - 4
    df_post['CS03'] = pd.to_numeric(raw_post['1-CS_3']) - 4
    df_post['CS04'] = pd.to_numeric(raw_post['1-CS_4']) - 4
    df_post['CS05'] = pd.to_numeric(raw_post['1-CS_5']) - 4
    df_post['CS06'] = pd.to_numeric(raw_post['1-CS_6']) - 4
    df_post['CS07'] = pd.to_numeric(raw_post['1-CS_7']) - 4
    df_post['CS08'] = pd.to_numeric(raw_post['1-CS_8']) - 4
    df_post['SE01'] = pd.to_numeric(raw_post['1-SE_1']) - 4
    df_post['SE02'] = pd.to_numeric(raw_post['1-SE_2']) - 4
    df_post['SE03'] = pd.to_numeric(raw_post['1-SE_3']) - 4
    df_post['SE04'] = pd.to_numeric(raw_post['1-SE_4']) - 4
    df_post['SE05'] = pd.to_numeric(raw_post['1-SE_5']) - 4
    df_post['SE06'] = pd.to_numeric(raw_post['1-SE_6']) - 4
    df_post['SE07'] = pd.to_numeric(raw_post['1-SE_7']) - 4
    
    df_post['UI01'] = pd.to_numeric(raw_post['1-UI_1']) - 4
    df_post['UI02'] = pd.to_numeric(raw_post['1-UI_2']) - 4
    df_post['UI03'] = pd.to_numeric(raw_post['1-UI_3']) - 4
    df_post['CLI01'] = pd.to_numeric(raw_post['1-CLI_1']) - 4
    df_post['CLI02'] = pd.to_numeric(raw_post['1-CLI_2']) - 4
    df_post['OS01'] = pd.to_numeric(raw_post['1-OS_1']) - 4
    df_post['OS02'] = pd.to_numeric(raw_post['1-OS_2']) - 4
    df_post['OS03'] = pd.to_numeric(raw_post['1-OS_3']) - 4
    df_post['OS04'] = pd.to_numeric(raw_post['1-OS_4']) - 4
    df_post['OS05'] = pd.to_numeric(raw_post['1-OS_5']) - 4
    
    df_post['Rank_Pl_explanation'] = raw_post['Q16']
    df_post['attention'] = pd.to_numeric(raw_post['Attention Check'])
    
    df_post['Rank_Pl'] = np.nan
    
    #for i in range(3):
    #    raw_post.loc[raw_post['user_id'] == '6XNM0KB28G' & raw_post[],]
    
    
    for user_id in df_post['user_id'].unique():
        for i in range(3):
            
            
            value = raw_post.loc[(raw_post['user_id'] == user_id) & \
                                 (raw_post['post_num']=='3'), f'Q15_{i+1}']
            #if user did not change the order, the result is recorded as nan values, fill them manually with standard order
           
            value = i+1 if len(value) != 1 or value.isna().item() else value.item()          
            df_post.loc[(df_post['user_id'] == user_id) & (df_post['post_num']==i), 'Rank_Pl'] = value
    print(df_post['Rank_Pl'])
    df_final = pd.DataFrame()
    df_final['user_id'] = raw_final['user_id']
    df_final['Validation_known'] = pd.to_numeric(raw_final['Q1_1']) - 50
    df_final['Validation_match'] = pd.to_numeric(raw_final['Q1_2']) - 50
    df_final['Remarks'] = raw_final['Remarks']
    
    
    df_recommendations = pd.DataFrame()
    df_recommendations['user_id'] = recommendation_stats['user_id']
    df_recommendations['condition'] = recommendation_stats['algorithm']
    df_recommendations['rec_mean_interactions'] = recommendation_stats['mean_interactions']
    df_recommendations['rec_median_interactions'] = recommendation_stats['median_interactions']
    df_recommendations['rec_popularity_lift'] = recommendation_stats['popularity_lift']
    df_recommendations['rec_jensen_shannon'] = recommendation_stats['jensen_shannon']
    df_recommendations['rec_head_ratio'] = recommendation_stats['head_ratio']
    df_recommendations['rec_mid_ratio'] = recommendation_stats['mid_ratio']
    df_recommendations['rec_tail_ratio'] = recommendation_stats['tail_ratio']
    
    
    df_time = pd.DataFrame()
    df_time['user_id'] = time_data['user_id']
    df_time['condition'] = time_data['condition']
    df_time['time_spent'] = time_data['time_spent']
    
    df_profile = pd.DataFrame()
    df_profile['user_id'] = user_profiles['user_id']
    df_profile['prof_mean_interactions'] = user_profiles['mean_interactions']
    df_profile['prof_median_interactions'] = user_profiles['median_interactions']
    df_profile['prof_num_interactions'] = user_profiles['num_filtered_interactions']
    df_profile['prof_head_ratio'] = user_profiles['head_ratio']
    df_profile['prof_mid_ratio'] = user_profiles['mid_ratio']
    df_profile['prof_tail_ratio'] = user_profiles['tail_ratio']
    df_profile['user_type'] = user_profiles['user_type']
    
    df_choices = pd.DataFrame()
    df_choices['user_id'] = choice_stats['user_id']
    df_choices['condition'] = choice_stats['condition']
    df_choices['num_choices'] = choice_stats['num_choices']
    df_choices['choice_mean_interactions'] = choice_stats['mean_interactions']
    df_choices['choice_median_interactions'] = choice_stats['median_interactions']
    df_choices['choice_popularity_lift'] = choice_stats['popularity_lift']
    df_choices['choice_jensen_shannon'] = choice_stats['jensen_shannon']
    df_choices['choice_head_ratio'] = choice_stats['head_ratio']
    df_choices['choice_mid_ratio'] = choice_stats['mid_ratio']
    df_choices['choice_tail_ratio'] = choice_stats['tail_ratio']
    
    
    valid_user_ids = df_post['user_id'].unique()
    print(f'Number of unqiue users: {len(valid_user_ids)}')

    
    valid_user_ids = get_valid_user_ids(df_post, valid_user_ids)
    print(f"Number of unique users after filtering unfitting post-questionnaires: {len(valid_user_ids)}")
    
    valid_user_ids = get_valid_user_ids(df_pre, valid_user_ids)
    print(f"Number of unique users after filtering unfitting pre-questionnaires: {len(valid_user_ids)}")
    
    valid_user_ids = get_valid_user_ids(df_final, valid_user_ids)
    print(f"Number of unique users after filtering unfitting final-questionnaires: {len(valid_user_ids)}")
    
    valid_user_ids = get_valid_user_ids(df_profile, valid_user_ids)
    print(f"Number of unique users after filtering unfitting profiles: {len(valid_user_ids)}")
    
    print(df_recommendations[df_recommendations['user_id']=='SIBGAX0G3P'])
    valid_user_ids = get_valid_user_ids(df_recommendations, valid_user_ids)
    print(f"Number of unique users after filtering unfitting recommendations: {len(valid_user_ids)}")
    
    valid_user_ids = get_valid_user_ids(df_choices, valid_user_ids)
    print(f"Number of unique users after filtering unfitting recommendation choices: {len(valid_user_ids)}")
    
    valid_user_ids = get_valid_user_ids(df_time, valid_user_ids)
    print(f"Number of unique users after filtering unfitting time stats: {len(valid_user_ids)}")
    
    valid_user_ids = df_post[
        (df_post['user_id'].isin(valid_user_ids)) & 
        (df_post['post_num'] == 0) & 
        (df_post['attention'] == 3)
    ]['user_id'].unique()
    print(f"Number of unique users after filtering failed attention checks: {len(valid_user_ids)}")
    
    valid_user_ids = df_final[
        (df_final['user_id'].isin(valid_user_ids)) & 
        (df_final['Validation_known'] > -50) & 
        (df_final['Validation_match'] > -50)
    ]['user_id'].unique()
    
    print(f"Number of unique users after filtering for poor profile accuracy: {len(valid_user_ids)}")
    
    df_post = df_post[df_post['user_id'].isin(valid_user_ids)]
    
    df = pd.merge(df_pre, df_post, how='right', on='user_id')
    df = pd.merge(df, df_final, how='left', on='user_id')
    df = pd.merge(df, df_profile, how='left', on='user_id')
    df = pd.merge(df, df_recommendations, how='left', on=['user_id', 'condition'])
    df = pd.merge(df, df_choices, how='left', on=['user_id', 'condition'])
    df = pd.merge(df, df_time, how='left', on=['user_id', 'condition'])
    
    user_df = pd.merge(df_pre, df_final, how='left', on='user_id')
    user_df = pd.merge(user_df, df_profile, how='left', on='user_id')
    
    return df, user_df, recommendation_similarities

    
def get_valid_user_ids(df, initial_valid_user_ids):
    
    # Check if 'post_num' exists in the DataFrame
    if 'post_num' in df.columns:
        # Define the required values for 'post_num' and 'condition'
        required_post_nums = [0, 1, 2]
        required_conditions = ['base', 'fair', 'cp']

        # Group by 'user_id' and filter rows as per the conditions
        valid_user_groups = df.groupby('user_id').filter(
            lambda group: (
                len(group) == 3 and
                set(group['post_num']) == set(required_post_nums) and
                set(group['condition']) == set(required_conditions)
            )
        )
    elif 'condition' in df.columns:
        
        required_conditions = ['base', 'fair', 'cp']

        # Group by 'user_id' and filter rows as per the conditions
        valid_user_groups = df.groupby('user_id').filter(
            lambda group: (
                len(group) == 3 and
                set(group['condition']) == set(required_conditions)
            )
        )
    else:
        # Group by 'user_id' and filter rows where the length of each group is 1
        valid_user_groups = df.groupby('user_id').filter(lambda group: len(group) == 1)
    
    # Get the user_ids that meet the criteria
    valid_user_ids = valid_user_groups['user_id'].unique()

    # Find the intersection between initial_valid_user_ids and new valid_user_ids
    valid_user_ids_intersection = set(initial_valid_user_ids) & set(valid_user_ids)

    if len(valid_user_ids_intersection) < len(initial_valid_user_ids):
        # Find the user_ids that have been removed
        removed_user_ids = set(initial_valid_user_ids) - set(valid_user_ids)
    
        print(f"Removed user_ids: {removed_user_ids}")
    return valid_user_ids_intersection