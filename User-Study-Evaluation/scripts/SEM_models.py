# -*- coding: utf-8 -*-
"""
Created on Wed Aug 30 16:25:51 2023

@author: unrob
"""

fa_cfa_model = {
    'Q_PerceivedPopularity' : ['PP01', 'PP02', 'PP03', 'PP04', 'PP05'],
    'Q_PerceivedFairness'  : ['PF01', 'PF02', 'PF03'],
    'Q_Discovery' : ['D01', 'D02', 'D03', 'D04', 'D05'],
    'Q_Familiarity' : ['F01', 'F02', 'F03', 'F04', 'F05'],
    'Q_PerceivedRecommendationQuality' : ['PQ01', 'PQ02', 'PQ03', 'PQ04', 'PQ05', 'PQ06', 'PQ07'],
    'Q_RecommendationSatisfaction' : ['RS01', 'RS02', 'RS03', 'RS04', 'RS05', 'RS06', 'RS07'],
    'Q_ChoiceSatisfaction' : ['CS01', 'CS02', 'CS03', 'CS04', 'CS05', 'CS06', 'CS07', 'CS08'],
    'Q_PerceivedSystemEffectiveness' : ['SE01', 'SE02', 'SE03', 'SE04', 'SE05', 'SE06', 'SE07'],

    'Q_OpennessSimilarRecommendations' : ['OS01', 'OS02', 'OS03', 'OS04', 'OS05'],
    'Q_UseIntention' : ['UI01', 'UI02', 'UI03'],
    'Q_ChoiceListeningIntention' : ['CLI01', 'CLI02'],
    
    }

# CFA model specification
cfa_model = '''
Q_MusicalSophistication =~ MS01 + MS02 + MS03 + MS04 + MS05 + MS06 + MS07 + MS08 + MS09 + MS10 + MS11 + MS12 + MS13 + MS14 + MS15
Q_MusicalEngagement =~ ME01 + ME02 + ME03 + ME04 + ME05 + ME06 + ME07 + ME08 + ME09

Q_PerceivedPopularity =~ PP01 + PP02 + PP03 + PP04 + PP05
Q_PerceivedFairness  =~ PF01 + PF02 + PF03
Q_Discovery =~ D01 + D02 + D03 + D04 + D05
Q_Familiarity =~ F01 + F02 + F03 + F04 + F05
Q_PerceivedRecommendationQuality =~ PQ01 + PQ02 + PQ03 + PQ04 + PQ05 + PQ06 + PQ07
Q_RecommendationSatisfaction =~ RS01 + RS02 + RS03 + RS04 + RS05 + RS06 + RS07
Q_ChoiceSatisfaction =~ CS01 + CS02 + CS03 + CS04 + CS05 + CS06 + CS07 + CS08
Q_PerceivedSystemEffectiveness =~ SE01 + SE02 + SE03 + SE04 + SE05 + SE06 + SE07

Q_OpennessSimilarRecommendations =~ OS01 + OS02 + OS03 + OS04 + OS05
Q_UseIntention =~ UI01 + UI02 + UI03
Q_ChoiceListeningIntention =~ CLI01 + CLI02

'''

# Create the SEM using the updated CFA model
# Initially include all paths, afterwards remove non-significant relations
sem_model = '''
#post_num?
# SSA Factors
Q_PerceivedPopularity, Q_PerceivedFairness, Q_Discovery, Q_Familiarity, Q_PerceivedRecommendationQuality ~ 
                    A_fair + A_cp + post_num + 
                    rec_mean_interactions + rec_median_interactions + rec_popularity_lift + rec_jensen_shannon + rec_head_ratio + rec_mid_ratio + rec_tail_ratio + 
                    prof_mean_interactions + prof_median_interactions + prof_num_interactions + prof_head_ratio + prof_mid_ratio + prof_tail_ratio
Q_PerceivedPopularity, Q_PerceivedFairness, Q_Discovery, Q_Familiarity, Q_PerceivedRecommendationQuality ~~
                    Q_PerceivedPopularity + Q_PerceivedFairness + Q_Discovery + Q_Familiarity + Q_PerceivedRecommendationQuality

# Experience Factors
Q_RecommendationSatisfaction, Q_ChoiceSatisfaction, Q_PerceivedSystemEffectiveness ~ 
                    A_fair + A_cp + post_num +
                    rec_mean_interactions + rec_median_interactions + rec_popularity_lift + rec_jensen_shannon + rec_head_ratio + rec_mid_ratio + rec_tail_ratio + 
                    prof_mean_interactions + prof_median_interactions + prof_num_interactions + prof_head_ratio + prof_mid_ratio + prof_tail_ratio +
                    Q_PerceivedPopularity + Q_PerceivedFairness + Q_Discovery + Q_Familiarity + Q_PerceivedRecommendationQuality 
Q_RecommendationSatisfaction, Q_ChoiceSatisfaction, Q_PerceivedSystemEffectiveness ~~ 
                    time_spent + choice_mean_interactions + choice_median_interactions + choice_popularity_lift + choice_jensen_shannon + choice_head_ratio + choice_mid_ratio + choice_tail_ratio
Q_RecommendationSatisfaction, Q_ChoiceSatisfaction, Q_PerceivedSystemEffectiveness ~~
                    Q_RecommendationSatisfaction + Q_ChoiceSatisfaction + Q_PerceivedSystemEffectiveness
                    




time_spent, choice_mean_interactions, choice_median_interactions, choice_popularity_lift, choice_jensen_shannon, choice_head_ratio, choice_mid_ratio, choice_tail_ratio ~ 
                    A_fair + A_cp + post_num +
                    rec_mean_interactions + rec_median_interactions + rec_popularity_lift + rec_jensen_shannon + rec_head_ratio + rec_mid_ratio + rec_tail_ratio + 
                    prof_mean_interactions + prof_median_interactions + prof_num_interactions + prof_head_ratio + prof_mid_ratio + prof_tail_ratio +
                    Q_PerceivedPopularity + Q_PerceivedFairness + Q_Discovery + Q_Familiarity + Q_PerceivedRecommendationQuality + 
time_spent, choice_mean_interactions, choice_median_interactions, choice_popularity_lift, choice_jensen_shannon, choice_head_ratio, choice_mid_ratio, choice_tail_ratio ~~
                    Q_RecommendationSatisfaction + Q_ChoiceSatisfaction + Q_PerceivedSystemEffectiveness
time_spent, choice_mean_interactions, choice_median_interactions, choice_popularity_lift, choice_jensen_shannon, choice_head_ratio, choice_mid_ratio, choice_tail_ratio ~~
                    time_spent + choice_mean_interactions + choice_median_interactions + choice_popularity_lift + choice_jensen_shannon + choice_head_ratio + choice_mid_ratio + choice_tail_ratio


# Intention Factors
Q_OpennessSimilarRecommendations, Q_UseIntention, Q_ChoiceListeningIntention ~ 
                    A_fair + A_cp + post_num +
                    rec_mean_interactions + rec_median_interactions + rec_popularity_lift + rec_jensen_shannon + rec_head_ratio + rec_mid_ratio + rec_tail_ratio + 
                    prof_mean_interactions + prof_median_interactions + prof_num_interactions + prof_head_ratio + prof_mid_ratio + prof_tail_ratio + 
                    Q_PerceivedPopularity + Q_PerceivedFairness + Q_Discovery + Q_Familiarity + Q_PerceivedRecommendationQuality + 
                    Q_RecommendationSatisfaction + Q_ChoiceSatisfaction + Q_PerceivedSystemEffectiveness + 
                    time_spent + choice_mean_interactions + choice_median_interactions + choice_popularity_lift + choice_jensen_shannon + choice_head_ratio + choice_mid_ratio + choice_tail_ratio
Q_OpennessSimilarRecommendations, Q_UseIntention, Q_ChoiceListeningIntention ~~ 
                    Q_OpennessSimilarRecommendations + Q_UseIntention + Q_ChoiceListeningIntention

'''


cfa_model_C_and_Questionnaires = '''
Q_MusicalSophistication =~ MS01 + MS02 + MS03 + MS04 + MS05 + MS06 + MS07 + MS08 + MS09 + MS10 + MS11 + MS12 + MS13 + MS14 + MS15
Q_MusicalEngagement =~ ME01 + ME02 + ME03 + ME04 + ME05 + ME06 + ME07 + ME08 + ME09

Q_PerceivedPopularity =~ PP01 + PP02 + PP03 + PP04 + PP05
Q_PerceivedFairness  =~ PF01 + PF02 + PF03
Q_Discovery =~ D01 + D02 + D03 + D04 + D05
Q_Familiarity =~ F01 + F02 + F03 + F04 + F05
Q_PerceivedRecommendationQuality =~ PQ01 + PQ02 + PQ03 + PQ04 + PQ05 + PQ06 + PQ07
Q_RecommendationSatisfaction =~ RS01 + RS02 + RS03 + RS04 + RS05 + RS06 + RS07
Q_ChoiceSatisfaction =~ CS01 + CS02 + CS03 + CS04 + CS05 + CS06 + CS07 + CS08
Q_PerceivedSystemEffectiveness =~ SE01 + SE02 + SE03 + SE04 + SE05 + SE06 + SE07

Q_OpennessSimilarRecommendations =~ OS01 + OS02 + OS03 + OS04 + OS05
Q_UseIntention =~ UI01 + UI02 + UI03
Q_ChoiceListeningIntention =~ CLI01 + CLI02

'''


sem_model_C_and_Questionnaires = '''
#post_num?
# SSA Factors
Q_PerceivedPopularity, Q_PerceivedFairness, Q_Discovery, Q_Familiarity, Q_PerceivedRecommendationQuality ~ A_fair + A_cp + post_num + rec_mean_interactions + rec_median_interactions + rec_popularity_lift + rec_jensen_shannon + rec_head_ratio + rec_mid_ratio + rec_tail_ratio + Q_MusicalEngagement + Q_MusicalSophistication 
                    
Q_PerceivedPopularity, Q_PerceivedFairness, Q_Discovery, Q_Familiarity, Q_PerceivedRecommendationQuality ~~ Q_PerceivedPopularity + Q_PerceivedFairness + Q_Discovery + Q_Familiarity + Q_PerceivedRecommendationQuality

# Experience Factors
Q_RecommendationSatisfaction, Q_ChoiceSatisfaction, Q_PerceivedSystemEffectiveness ~ A_fair + A_cp + post_num + rec_mean_interactions + rec_median_interactions + rec_popularity_lift + rec_jensen_shannon + rec_head_ratio + rec_mid_ratio + rec_tail_ratio + Q_MusicalEngagement + Q_MusicalSophistication + Q_PerceivedPopularity + Q_PerceivedFairness + Q_Discovery + Q_Familiarity + Q_PerceivedRecommendationQuality 
                    
Q_RecommendationSatisfaction, Q_ChoiceSatisfaction, Q_PerceivedSystemEffectiveness ~~ Q_RecommendationSatisfaction + Q_ChoiceSatisfaction + Q_PerceivedSystemEffectiveness
                    


# Intention Factors
Q_OpennessSimilarRecommendations, Q_UseIntention, Q_ChoiceListeningIntention ~ A_fair + A_cp + post_num + rec_mean_interactions + rec_median_interactions + rec_popularity_lift + rec_jensen_shannon + rec_head_ratio + rec_mid_ratio + rec_tail_ratio +  Q_MusicalEngagement + Q_MusicalSophistication + Q_PerceivedPopularity + Q_PerceivedFairness + Q_Discovery + Q_Familiarity + Q_PerceivedRecommendationQuality + Q_RecommendationSatisfaction + Q_ChoiceSatisfaction + Q_PerceivedSystemEffectiveness 
                    
Q_OpennessSimilarRecommendations, Q_UseIntention, Q_ChoiceListeningIntention ~~ Q_OpennessSimilarRecommendations + Q_UseIntention + Q_ChoiceListeningIntention

'''