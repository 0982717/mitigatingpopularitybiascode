import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from semopy import Model, semplot, report, efa, gather_statistics, calc_stats
import matplotlib.pyplot as plt
from factor_analyzer.factor_analyzer import FactorAnalyzer

from factor_analyzer import ConfirmatoryFactorAnalyzer, ModelSpecificationParser
# Adjust display settings to prevent truncation
pd.set_option('display.max_columns', None)  # Display all columns
pd.set_option('display.expand_frame_repr', False)  # Prevent horizontal truncation
pd.set_option('display.max_rows', None)  # Display all rows
pd.set_option('display.width', None)  # Allow the output to span multiple lines


'https://arxiv.org/pdf/1905.09376.pdf'

from scripts import SEM_models as models




def get_questionnaire_subset(df, with_OSA=False):
    df = df[['PP01', 'PP02', 'PP03', 'PP04', 'PP05', 'PF01', 'PF02', 'PF03', 'D01', 'D02', 'D03', 'D04', 'D05', 
             'F01', 'F02', 'F03', 'F04', 'F05', 'PQ01', 'PQ02', 'PQ03', 'PQ04', 'PQ05', 'PQ06', 'PQ07',
             'RS01', 'RS02', 'RS03', 'RS04', 'RS05', 'RS06', 'RS07', 'CS01', 'CS02', 'CS03', 'CS04', 'CS05', 'CS06', 'CS07', 'CS08', 'SE01', 'SE02', 'SE03', 'SE04', 'SE05', 'SE06', 'SE07',
             'OS01', 'OS02', 'OS03', 'OS04', 'OS05', 'UI01', 'UI02', 'UI03', 'CLI01', 'CLI02']]
    return df

def explore_cfa(df):
    q_df = get_questionnaire_subset(df)
    #print(efa.explore_cfa_model(q_df))
    
    fa = FactorAnalyzer(rotation='varimax', n_factors=11)
    fa.fit(q_df)
    ev, v = fa.get_eigenvalues()
    
    plt.scatter(range(1,q_df.shape[1]+1), ev)
    plt.plot(range(1,q_df.shape[1]+1), ev)
    plt.title('Scree Plot')
    plt.xlabel('Factors')
    plt.ylabel('Eigenvalue')
    plt.grid()
    plt.show()
    
    EFA = pd.DataFrame(fa.loadings_, index=[q_df.columns])
    
    fig, ax = plt.subplots()
    im = ax.imshow(EFA)
    plt.show()
    

def do_fa_cfa(df, cfa_model=models.fa_cfa_model, save_path=None):
    q_df = get_questionnaire_subset(df)
    model_spec = ModelSpecificationParser.parse_model_specification_from_dict(q_df, cfa_model)
    cfa = ConfirmatoryFactorAnalyzer(model_spec, disp=False)
    cfa.fit(q_df.values)
    print("Factor Loadings:")
    print(cfa.loadings_)
    print()
    print("Factors Covariances")
    print(cfa.factor_varcovs_)
    print("Factor Standard Errors")
    print(cfa.get_standard_errors())
    
    if save_path is not None:
        new_fac_scores = cfa.transform(q_df.values)
        fac_scores = pd.DataFrame(new_fac_scores, columns =['Q_PerceivedPopularity', 
                                                            'Q_PerceivedFairness', 'Q_Discovery', 
                                                            'Q_Familiarity', 'Q_PerceivedRecommendationQuality',
                                                            'Q_RecommendationSatisfaction', 'Q_ChoiceSatisfaction', 
                                                            'Q_PerceivedSystemEffectiveness', 'Q_OpennessSimilarRecommendations', 
                                                            'Q_UseIntention' , 'Q_ChoiceListeningIntention'])
        df = pd.concat([df, fac_scores], axis=1)
        df.to_csv(save_path)
    
    

def do_cfa(df, cfa_model=models.cfa_model):
    # Fit CFA model
    cfa_m =  Model(cfa_model)
    cfa_r = cfa_m.fit(df)
    #print(cfa_r)
    
    #print(calc_stats(cfa_r))
    
    # Inspect the CFA results and loadings
    cfa_ins = cfa_m.inspect(std_est=True)
    
    print(cfa_ins)
    
    # Filter variables based on the threshold for loading and for significant p value
    variables_to_remove = []
    for idx, row in cfa_ins.iterrows():
    
        var, op, estimate, p = row['lval'], row['op'], row['Est. Std'], row['p-value']
        if p == '-':
            p = 0
        if op == '~' and p > 0.05:
            variables_to_remove.append(var)

    print(variables_to_remove)

    cfa_ins.to_csv('results/CFA_init.csv')
    
    return cfa_ins





def do_sem(df, cfa_model=models.cfa_model, sem_model=models.sem_model, save_path=None):
    # Combine the CFA and SEM models
    full_model = cfa_model + sem_model
    
    # Fit the SEM
    sem_m = Model(full_model)
    sem_r = sem_m.fit(df)
    
    sem_ins = sem_m.inspect()
    if save_path != None:
        sem_ins.to_csv(save_path)
    print(sem_ins)

    
    g = semplot(sem_m, "plots/sem.png")
    
    with open('results/SEM_init.dot', "w") as dot_file:
        dot_file.write(g)
    
    plt.show(g)
    #print(g)
    
    #Further filtering. Filter out non-significant paths (Knijnenburg) and run again
    
    
    
    
def investigate_missing_values(df, cfa_model=models.cfa_model):
    cfa_variables = []  # List to store the variables from the CFA model
    for line in cfa_model.splitlines():
        if '~' in line:
            cfa_variables.extend(line.split('~ ')[1].split(' + '))


    constant_columns = []  # List to store the names of constant columns

    # Check for constant columns in the DataFrame
    for col in df.columns:
        if col in cfa_variables and df[col].nunique() == 1:
            constant_columns.append(col)

    if constant_columns:
        print("Constant columns found in the CFA variables:")
        print(constant_columns)
    else:
        print("No constant columns found in the CFA variables.")



    missing_columns = []
    
    # Iterate through each variable from the CFA model
    for col in cfa_variables:

        if df[col].isnull().any():
            missing_columns.append(col)
    
    if not missing_columns:
        print("No missing values found in the variables used in the CFA.")
    else:
        print("Missing values found in the following variables:")
        print(missing_columns)
    
    return missing_columns