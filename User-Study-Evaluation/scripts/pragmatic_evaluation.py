# -*- coding: utf-8 -*-
"""
Created on Sat Jul 29 14:58:57 2023

@author: unrob
"""

import pandas as pd
import statsmodels.api as sm
from statsmodels.formula.api import ols
from scipy.stats import pearsonr
import math
import pingouin as pg
from scripts.pragmatic_evaluation_models import ANOVA_model, Correlation_model

def compute_effects(df, factors=[], ANOVA_model=ANOVA_model, Correlation_model=Correlation_model, save_file=None, print_results=False):
    result_list = []
    
    for factor1 in factors:
        
        stats = pd.DataFrame(columns=['factor1', 'factor2', 'statistic', 'p', 'effect_size', 'p<.05', 'Statistic_symbol'])
        if factor1 in ANOVA_model.keys():
            
            for factor2 in ANOVA_model[factor1]:

                
                #The code in comments does not represent a repeated measures anova
                
                #mod = ols(f'{factor2} ~ {factor1}',
                #          data=df).fit()
                #aov_table = sm.stats.anova_lm(mod, typ=2)
                
                #F = results['']

                #p = aov_table.loc[factor1, 'Pr > F']
                
                #esq_sm = aov_table['sum_sq'][0]/(aov_table['sum_sq'][0]+aov_table['sum_sq'][1]) #https://www.pybloggers.com/2016/02/four-ways-to-conduct-one-way-anovas-with-python/
                
                
                results = pg.rm_anova(dv=factor2, within=factor1, subject='user_id', data=df, detailed=True)#https://www.reneshbedre.com/blog/repeated-measure-anova.html
                

                

                
                F = results['F'][0]
                p = results['p-unc'][0]
                eta_sq = results['ng2'][0]
                Statistic_symbol = f'F({results["DF"][0]},{len(df)-2})'
                
                
                significant = '***' if p < 0.001 else '**' if p < 0.01 else '*' if p < 0.05 else 'o'
                
                stats = pd.concat([stats, pd.DataFrame({'factor1': [factor1], 'factor2': [factor2], 'statistic': [F], 'p': [p], 'effect_size': [eta_sq], 'p<.05': [significant], 'Statistic_symbol' : Statistic_symbol})],
                                    ignore_index=True)
                
        elif factor1 in Correlation_model.keys():
            for factor2 in Correlation_model[factor1]:
                # Compute Pearson Correlation Coefficient and its p-value
                corr_coefficient, p_value = pearsonr(df[factor1], df[factor2])
                
                # Calculate the t-statistic
                n = len(df)
                df_corr = n - 2  # Degrees of freedom for correlation
                t_statistic = corr_coefficient *  math.sqrt(df_corr / (1 - corr_coefficient**2))**0.5
                significant = '***' if p_value < 0.001 else '**' if p_value < 0.01 else '*' if p_value < 0.05 else 'o'
                
                stats = pd.concat([stats, pd.DataFrame({'factor1': [factor1], 'factor2': [factor2], 'statistic': [t_statistic], 'p':[p_value], 'effect_size': [corr_coefficient], 'p<.05': [significant], 'Statistic_symbol' : f'r({df_corr})'})],
                                     ignore_index=True)
        else:
            continue
        
        result_list.append(stats)    
        if print_results:
            print(f'Stats for factor {factor1}:')
            print(stats)
            print()
        
    result_df = pd.concat(result_list, ignore_index=True)
    
    if save_file is not None:
        result_df.to_csv(save_file, index=False)
    
    return result_df


categories = {
    "OSA": [
        "condition",
        "post_num",
        "rec_mean_interactions",
        "rec_median_interactions",
        "rec_popularity_lift",
        "rec_jensen_shannon",
        "rec_head_ratio",
        "rec_mid_ratio",
        "rec_tail_ratio"

    ],
    "PC": [
        "prof_mean_interactions",
        "prof_median_interactions",
        "prof_num_interactions",
        "prof_head_ratio",
        "prof_mid_ratio",
        "prof_tail_ratio",
        "Q_MusicalSophistication",
        "Q_MusicalEngagement",
        "Age",
        "Gender"
    ],
    "SSA": [
        "Q_Perceived_Popularity",
        "Q_PerceivedFairness",
        "Q_Discovery",
        "Q_Familiarity",
        "Q_PerceivedRecommendationQuality"
    ],
    "EXP": [
        "Q_RecommendationSatisfaction",
        "Q_ChoiceSatisfaction",
        "Q_PerceivedSystemEffectiveness"
    ],
    "INT": [
        "time_spent",
        "choice_mean_interactions",
        "choice_median_interactions",
        "choice_popularity_lift",
        "choice_jensen_shannon",
        "choice_head_ratio",
        "choice_mid_ratio",
        "choice_tail_ratio"
    ],
    "BI": [
        "Q_OpennessSimilarRecommendations",
        "Q_UseIntention",
        "Q_ChoiceListeningIntention"
    ]
}


styles = {
        "octagon":
            [
            "condition",
            "post_num",
            "rec_mean_interactions",
            "rec_median_interactions",
            "rec_popularity_lift",
            "rec_jensen_shannon",
            "rec_head_ratio",
            "rec_mid_ratio",
            "rec_tail_ratio",
        ],
        "ellipse":
            [
            "prof_mean_interactions",
            "prof_median_interactions",
            "prof_num_interactions",
            "prof_head_ratio",
            "prof_mid_ratio",
            "prof_tail_ratio",
            "time_spent",
            "choice_mean_interactions",
            "choice_median_interactions",
            "choice_popularity_lift",
            "choice_jensen_shannon",
            "choice_head_ratio",
            "choice_mid_ratio",
            "choice_tail_ratio",
        ],
        "box":
            [
            "Age",
            "Gender",
            "Q_MusicalSophistication",
            "Q_MusicalEngagement",
            "Q_Perceived_Popularity",
            "Q_PerceivedFairness",
            "Q_Discovery",
            "Q_Familiarity",
            "Q_PerceivedRecommendationQuality",
            "Q_RecommendationSatisfaction",
            "Q_ChoiceSatisfaction",
            "Q_PerceivedSystemEffectiveness",
            "Q_OpennessSimilarRecommendations",
            "Q_UseIntention",
            "Q_ChoiceListeningIntention"
        ]
    }
    
# Define subgraph styles
subgraph_styles = {
    "OSA": {"label": "OSA", "color": "purple"},
    "PC": {"label": "PC", "color": "red"},
    "SSA": {"label": "SSA", "color": "darkgreen"},
    "EXP": {"label": "EXP", "color": "orange"},
    "INT": {"label": "INT", "color": "blue"},
    "BI": {"label": "BI", "color": "pink"}
}

renaming = {
    "condition" : "Condition",
    "post_num": "Trial Nmbr",
    "rec_mean_interactions" : "R: Mean POP",
    "rec_median_interactions": "R: Med POP",
    "rec_popularity_lift": "R: Pop Lift",
    "rec_jensen_shannon" : "R: UPD",
    "rec_head_ratio": "R: Head Ratio",
    "rec_mid_ratio" : "R: Mid Ratio",
    "rec_tail_ratio" : "R: Tail Ratio",
    "prof_mean_interactions": "P: Mean POP",
    "prof_median_interactions" : "P: Med POP",
    "prof_num_interactions" : "P: Profile Size",
    "prof_head_ratio" : "P: Head Ratio",
    "prof_mid_ratio" : "P: Mid Ratio",
    "prof_tail_ratio" : "P: Tail Ratio",
    "time_spent" : "Time Spent",
    "choice_mean_interactions" : "C: Mean POP",
    "choice_median_interactions" : "C: Med POP",
    "choice_popularity_lift" : "C: Pop Lift",
    "choice_jensen_shannon" : "C: UPD",
    "choice_head_ratio" : "C: Head Ratio",
    "choice_mid_ratio" : "C: Mid Ratio",
    "choice_tail_ratio" : "C: Tail Ratio",
    "Age" : "Age",
    "Gender": "Gender",
    "Q_MusicalSophistication" : "Q: Musical Soph.",
    "Q_MusicalEngagement" : "Q: Musical Eng.",
    "Q_Perceived_Popularity" : "Q: Perc. Popularity",
    "Q_PerceivedFairness" : "Q: Perc. Fairness",
    "Q_Discovery" : "Q: Discovery",
    "Q_Familiarity" : "Q: Familiarity",
    "Q_PerceivedRecommendationQuality" : "Q: Perc. Rec. Quality",
    "Q_RecommendationSatisfaction": "Q: Rec. Satisfaction",
    "Q_ChoiceSatisfaction": "Q: Choice Satisfaction",
    "Q_PerceivedSystemEffectiveness" : "Q: Perc. System Effectiveness",
    "Q_OpennessSimilarRecommendations": "Q: Openness to Sim. Rec.",
    "Q_UseIntention": "Q: Use Intention",
    "Q_ChoiceListeningIntention" : "Q: Choice Listening Intention"
    }

import pygraphviz as pgv

def create_directed_graph(results, save_file=None):
    graph = pgv.AGraph(strict=True, directed=True)
    
    
    
    for style in styles.keys():
        graph.add_nodes_from([renaming[node] for node in styles[style]], shape=style)
        
    added_edges = set()

    for _, row in results.iterrows():
        factor1 = renaming[row['factor1']]
        factor2 = renaming[row['factor2']]
        
        value = row['p']
        formatted_p = f'{value:.3f}'
        
        if value < 0.001:
            formatted_p = 'p < .001'
        else:
            if formatted_p.startswith('0'):
                formatted_p = formatted_p.lstrip('0')
            formatted_p = 'p = ' + formatted_p
        if factor1 in ["Condition", "Trial Nmbr", "Gender"]:
            label = f"{row['Statistic_symbol']} = {row['statistic']:.2f}\n{formatted_p}\n\u03B7\u00B2: {row['effect_size']:.2f}\n{row['p<.05']}"
        else:
            label = f"{row['Statistic_symbol']} = {row['effect_size']:.2f}\n{formatted_p}\n{row['p<.05']}"

            
        
        significant = row['p<.05']
        
        if significant != 'o':
            edge = (factor1, factor2)
            reverse_edge = (factor2, factor1)
            
            if edge in added_edges or reverse_edge in added_edges:
                continue
            else:
                added_edges.add(edge)
                
            if reverse_edge_exists(factor1, factor2, results):
                graph.add_edge(factor1, factor2, dir="both", label=label)
            else:
                graph.add_edge(factor1, factor2, label=label)
    
    # Add subgraphs with specified styles
    for subgraph_name, subgraph_style in subgraph_styles.items():
        graph.add_subgraph(
            [renaming[node] for node in categories[subgraph_name]],
            name='cluster_' + subgraph_name,
            **subgraph_style
        )
    #graph.add_style
    
    if save_file is not None:
        graph.write(save_file)
    
    return graph


def reverse_edge_exists(factor1, factor2, results):
    reverse_exists = any(
        (renaming[row['factor1']] == factor2 and renaming[row['factor2']] == factor1) for _, row in results.iterrows()
    )
    return reverse_exists