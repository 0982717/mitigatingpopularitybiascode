# -*- coding: utf-8 -*-
"""
Created on Thu Jul 13 16:36:55 2023

@author: unrob
"""
import math
import pandas as pd

def jensen_shannon(df_a, df_b):
    epsilon = 1e-8  # Small non-zero value
    A = 0
    B = 0

    for c in ['head_ratio', 'mid_ratio', 'tail_ratio']:
        A_ratio = df_a[c]
        if type(A_ratio) == pd.Series:
            A_ratio = A_ratio.item()
            
        B_ratio = df_b[c]
        if type(B_ratio) == pd.Series:
            B_ratio = B_ratio.item()
        
        
        if A_ratio == 0:
            A_ratio += epsilon

        if B_ratio == 0:
            B_ratio += epsilon

        A += A_ratio * math.log2((2 * A_ratio) / (A_ratio + B_ratio))
        B += B_ratio * math.log2((2 * B_ratio) / (A_ratio + B_ratio))

    js = (A + B) / 2
    return js