# -*- coding: utf-8 -*-
"""
Created on Thu Jul 13 14:27:23 2023

@author: unrob
"""
import ast
import pandas as pd
import math 
import statistics
from scripts.Statistics import jensen_shannon

def extract_choice_track_ids(track_choices, spids_ids):
    track_choices['uri'] = track_choices['uri'].apply(ast.literal_eval)
    track_choices['rank'] = track_choices['rank'].apply(ast.literal_eval)
    #track_choices['rank'] = track_choices['rank'].apply(lambda lst: [float(rank) for rank in lst])

    track_choices['track_ids'] = track_choices['uri'].apply(lambda uris: uris_to_track_ids(uris, spids_ids))
    del track_choices['uri']
    return track_choices
    
def uris_to_track_ids(uris, spids_ids):
    spids = [uri.split(':')[-1] for uri in uris]
    matching_rows = spids_ids[spids_ids['uri'].isin(spids)]
    track_ids = matching_rows.groupby('uri')['track_id'].first().tolist()
    return track_ids

def choice_stats(track_choices, user_profiles, track_popularities):
    choice_stats = pd.DataFrame(columns=['user_id', 'condition', 'num_choices', 'mean_rank', 
                                         'mean_interactions', 'median_interactions', 
                                         'popularity_lift', 'jensen_shannon',
                                         'head_ratio', 'mid_ratio', 'tail_ratio'])
    for user_id in user_profiles['user_id'].unique():
        user_profile = user_profiles[user_profiles['user_id']==user_id]
        
        user_track_choices = track_choices[track_choices['user_id']==user_id]
        profile_mean_interactions = user_profile['mean_interactions'].item()
        for condition in user_track_choices['condition']:
            mean_rank = statistics.mean(user_track_choices[user_track_choices['condition']==condition]['rank'].item())

            choices = user_track_choices[user_track_choices['condition']==condition]['track_ids'].item()
            pops = track_popularities.copy()
            pops = pops[pops['track_id'].isin(choices)]
            
            
            num_choices = pops.shape[0]
            pop_counts = pops['popularity'].value_counts()
            head_ratio = pop_counts.get('head', 0) / num_choices
            mid_ratio = pop_counts.get('mid', 0) / num_choices
            tail_ratio = pop_counts.get('tail', 0) / num_choices
            
            
            mean_interactions = pops['interactions'].mean()
            median_interactions = pops['interactions'].median()
            
            popularity_lift = (mean_interactions - profile_mean_interactions) / profile_mean_interactions
            
            js = jensen_shannon(
                {'head_ratio': head_ratio, 'mid_ratio': mid_ratio, 'tail_ratio': tail_ratio},
                user_profile)
            # Create a dictionary of the values
            row = {
                'user_id': user_id,
                'condition': condition,
                'num_choices': num_choices,
                'mean_rank': mean_rank,
                'mean_interactions': mean_interactions,
                'median_interactions': median_interactions,
                'popularity_lift': popularity_lift,
                'jensen_shannon': js,
                'head_ratio': head_ratio,
                'mid_ratio': mid_ratio,
                'tail_ratio': tail_ratio
            }
            
            choice_stats.loc[len(choice_stats)] = row
            
            
            #add values to choice_stats
    return choice_stats
            

def descriptive_stats(choice_stats):
    return

