# -*- coding: utf-8 -*-
"""
Created on Mon Aug  7 14:27:48 2023

@author: unrob
"""

def create_factors(df):
    df_w_facs = df[['user_id', 'condition','post_num', 'user_type',
                    'rec_mean_interactions', 'rec_median_interactions', 'rec_popularity_lift', 'rec_jensen_shannon', 'rec_head_ratio', 'rec_mid_ratio', 'rec_tail_ratio', 
                    'prof_mean_interactions', 'prof_median_interactions', 'prof_num_interactions', 'prof_head_ratio', 'prof_mid_ratio', 'prof_tail_ratio',
                    'time_spent', 'choice_mean_interactions', 'choice_median_interactions', 'choice_popularity_lift', 'choice_jensen_shannon', 
                    'choice_head_ratio', 'choice_mid_ratio', 'choice_tail_ratio']].copy()
    '''
    regression_factor_loadings =factor_loadings[factor_loadings['op']=='~']
    for factor in regression_factor_loadings['rval'].unique():
        items = regression_factor_loadings.loc[regression_factor_loadings['rval'] == factor, 'lval']
        estimates = regression_factor_loadings.loc[regression_factor_loadings['rval'] == factor, 'Est. Std']
        
        #print(items, estimates)
        #print(df[items])
        factor_score = df[items].values.dot(estimates)

        df_w_facs[f'Q_{factor}'] = factor_score
    return df_w_facs

    '''
    #instead of using 1/18 etc use the standardized cfa loadings
    df_w_facs['Q_MusicalSophistication'] = (1/18) * df['MS01'] + (1/18) * df['MS02'] + (1/18) * df['MS03'] + (1/18) * df['MS04'] + (1/18) * df['MS05'] + -\
        (1/18) * df['MS06'] - (1/18) * df['MS07'] + (1/18) * df['MS08'] - (1/18) * df['MS09'] + (1/18) * df['MS10'] - \
            (1/18) * df['MS11'] + (1/18) * df['MS12'] - (1/18) * df['MS13'] - (1/18) * df['MS14'] + (1/18) * df['MS15'] +\
                (1/18) * df['MS16'] + (1/18) * df['MS17'] +  (1/18) * df['MS18']
                
    df_w_facs['Q_MusicalEngagement'] = (1/9) * df['ME01'] + (1/9) * df['ME02'] + (1/9) * df['ME03'] + (1/9) * df['ME04'] - (1/9) * df['ME05'] + \
        (1/9) * df['ME06'] + (1/9) * df['ME07'] + (1/9) * df['ME08'] + (1/9) * df['ME09']
    
    df_w_facs['Q_Perceived_Popularity'] = (1/5) * df['PP01'] + (1/5) * df['PP02'] - (1/5) * df['PP03'] + (1/5) * df['PP04'] + (1/5) * df['PP05']
    df_w_facs['Q_PerceivedFairness'] = (1/3) * df['PF01'] - (1/3) * df['PF02'] - (1/3) * df['PF03'] 
    df_w_facs['Q_Discovery'] = (1/5) * df['D01'] + (1/5) * df['D02'] + (1/5) * df['D03'] + (1/5) * df['D04'] + (1/5) * df['D05']
    df_w_facs['Q_Familiarity'] = (1/5) * df['F01'] - (1/5) * df['F02'] + (1/5) * df['F03'] + (1/5) * df['F04'] + (1/5) * df['F05']
    df_w_facs['Q_PerceivedRecommendationQuality'] = (1/7) * df['PQ01'] + (1/7) * df['PQ02'] + (1/7) * df['PQ03'] + (1/7) * df['PQ04'] - \
        (1/7) * df['PQ05'] - (1/7) * df['PQ06'] - (1/7) * df['PQ07']
        
    df_w_facs['Q_RecommendationSatisfaction'] = (1/7) * df['RS01'] + (1/7) * df['RS02'] + (1/7) * df['RS03'] + (1/7) * df['RS04'] - \
        (1/7) * df['RS05'] + (1/7) * df['RS06'] + (1/7) * df['RS07']
    df_w_facs['Q_ChoiceSatisfaction'] = (1/8) * df['CS01'] + (1/8) * df['CS02'] + (1/8) * df['CS03'] - (1/8) * df['CS04'] + \
        (1/8) * df['CS05'] - (1/8) * df['CS06'] + (1/8) * df['CS07'] + (1/8) * df['CS08']
    df_w_facs['Q_PerceivedSystemEffectiveness'] = (1/7) * df['SE01'] - (1/7) * df['SE02'] + (1/7) * df['SE03'] + (1/7) * df['SE04'] - \
        (1/7) * df['SE05'] + (1/7) * df['SE06'] + (1/7) * df['SE07']
    
    df_w_facs['Q_OpennessSimilarRecommendations'] = (1/5) * df['OS01'] + (1/5) * df['OS02'] + (1/5) * df['OS03'] + (1/5) * df['OS04'] + (1/5) * df['OS05']
    df_w_facs['Q_UseIntention'] = (1/3) * df['UI01'] + (1/3) * df['UI02'] + (1/3) * df['UI03']
    df_w_facs['Q_ChoiceListeningIntention'] = (1/2) * df['CLI01'] + (1/2) * df['CLI02']
    
    return df_w_facs