# -*- coding: utf-8 -*-
"""
Created on Sun Aug 20 15:01:22 2023

@author: unrob
"""
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from scipy.stats import chi2_contingency, chi2



def analyse_ranking(study_stats, colorPalette=None):
    df = study_stats.copy()[['user_id', 'condition', 'post_num', 'Rank_Pl']]
    
    print(df['Rank_Pl'].value_counts())
    # Group data by system and calculate mean and standard deviation
    descriptive_stats = df[['condition', 'Rank_Pl']].groupby('condition').describe()
    
    counts = df.groupby('condition')['Rank_Pl'].value_counts().reset_index(name='count').sort_values(by=['condition', 'Rank_Pl'])
    
    
    print("Descriptive Analysis:")
    print(descriptive_stats)
    print(counts)

    algorithm_order = ['base', 'fair', 'cp']

    # Set the color palette
    if colorPalette is not None:
        sns.set_palette(colorPalette)

    plt.figure(figsize=(10, 6))
    sns.countplot(data=df, x='condition', hue='Rank_Pl', order=algorithm_order)
    plt.xlabel('Ranking', fontsize=18)
    plt.ylabel('Count', fontsize=18)
    plt.legend(title='Rank', title_fontsize=18, fontsize=16)
    
    new_labels = ['Base', 'FA*IR', 'CP']

    plt.xticks(plt.xticks()[0], new_labels, fontsize=16)
    
    plt.savefig(f"plots/Ranking/Ranking_Counts.png")
    
    plt.show()
    
    # Bar plot to visualize the mean rankings by system
    plt.figure(figsize=(10, 6))
    sns.barplot(x='condition', y='Rank_Pl', data=df, errorbar='sd', order=algorithm_order)
    plt.xlabel('System', fontsize=18)
    plt.ylabel('Mean Ranking', fontsize=18)
    
    new_labels = ['Base', 'FA*IR', 'CP']

    plt.xticks(plt.xticks()[0], new_labels, fontsize=16)
    
    plt.savefig(f"plots/Ranking/Ranking_Mean.png")
    
    plt.show()
    
    # Create a cross-tabulation table
    cross_tab = pd.crosstab(df['condition'], df['Rank_Pl'])
    
    custom_order = ['base', 'fair', 'cp']

    cross_tab = cross_tab.loc[custom_order]
    observed = cross_tab.values
    # Perform Chi-Square test for independence
    chi2, p_value, dof, expected = chi2_contingency(cross_tab)

    print("Chi-Square value:", chi2)
    print("P-value:", p_value)
    print("Degrees of freedom:", dof)
    print("Expected frequencies:", expected)
    
    # Visualize the cross-tabulation
    plt.figure(figsize=(10, 6))
    sns.heatmap(cross_tab, annot=True, cmap="YlGnBu", cbar=False)
    plt.xlabel('Rank', fontsize=18)
    plt.ylabel('Condition', fontsize=18)
    new_labels = ['Base', 'FA*IR', 'CP']

    plt.yticks(plt.yticks()[0], new_labels, fontsize=16)
    
    plt.savefig(f"plots/Ranking/RankingsCrossTab.png")
    plt.show()
    
