# -*- coding: utf-8 -*-
"""
Created on Thu Jul 13 13:50:22 2023

@author: unrob
"""

def uris_to_track_ids(uris, spids_ids):
    print(uris)
    spids = [uri.split(':')[-1] for uri in uris]
    matching_rows = spids_ids[spids_ids['uri'].isin(spids)]
    print(matching_rows)
    track_ids = matching_rows.groupby('uri')['track_id'].first().tolist()
    return track_ids