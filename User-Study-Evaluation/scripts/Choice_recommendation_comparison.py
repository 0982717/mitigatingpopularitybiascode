# -*- coding: utf-8 -*-
"""
Created on Thu Jul 13 16:36:06 2023

@author: unrob
"""
from scripts.Statistics import jensen_shannon

def compare_choices_recommendations(choice_stats, recommendation_stats):
    js = 0
    for idx, choice_row in choice_stats.iterrows():
        recommendation_row = recommendation_stats[(recommendation_stats['user_id']==choice_row['user_id']) &
                                                  (recommendation_stats['algorithm']== choice_row['condition'])]
        
        js += jensen_shannon(choice_row, recommendation_row)
    avg_js = js / choice_stats.shape[0]
    print(f'Average jensen shannon of {avg_js}')
    