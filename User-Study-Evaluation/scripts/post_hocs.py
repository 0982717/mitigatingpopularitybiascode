# -*- coding: utf-8 -*-
"""
Created on Tue Aug 22 14:51:57 2023

@author: unrob
"""
import seaborn as sns
import matplotlib.pyplot as plt
import scipy
import statannot



def paired_t_test(df, factors, save=None, colorPalette=None, color_mapping=None, ylim=None, includesProfile=False, figsize=(10,10), questionnaire=False, ylabel='Score on Likert Scale (-3 to 3)'):
    stats = df[['condition'] + factors]
    if includesProfile:
        algorithm_order = ['profile', 'base', 'fair', 'cp']
        algorithm_names = ['Profiles', 'Base', 'FA*IR', 'CP']
        if color_mapping is None:
            algorithm_colors = {'profile': 'blue', 'base': 'gray', 'fair': 'orange', 'cp': 'green'}
        else:
            algorithm_colors = {'profile': color_mapping['b'], 'base': color_mapping['gray'], 'fair': color_mapping['o'], 'cp': color_mapping['g']}
    else:
        algorithm_order = ['base', 'fair', 'cp']
        algorithm_names = ['Base', 'FA*IR', 'CP']
        if color_mapping is None:
            algorithm_colors = {'base': 'gray', 'fair': 'orange', 'cp': 'green'}
        else:
            algorithm_colors = {'base': color_mapping['gray'], 'fair': color_mapping['o'], 'cp': color_mapping['g']}

    if colorPalette:
        sns.set_palette(colorPalette)

    # Plot barplots with asterisks indicating significance
    plt.figure(figsize=figsize)
    for factor in factors:
       
        plt.subplot(len(factors), 1, factors.index(factor) + 1)
        ax = sns.boxplot(data=stats, x='condition', y=factor, palette=[algorithm_colors[alg] for alg in algorithm_order], order=algorithm_order)

        box_pairs = [(alg1, alg2) for i, alg1 in enumerate(algorithm_order) for j, alg2 in enumerate(algorithm_order) if i < j]

        # Add asterisks to indicate significance
        statannot.add_stat_annotation(ax, data=stats, x='condition', y=factor,
                                      box_pairs=box_pairs,
                                      test='t-test_paired', text_format='star', loc='inside', verbose=2,
                                      comparisons_correction=None, order=algorithm_order)

        plt.xlabel('Condition', fontsize=18)
        plt.ylabel(ylabel, fontsize=18)

        plt.xticks(range(len(algorithm_order)), algorithm_names, fontsize=16)
        if ylim is not None:
            plt.ylim(ylim)
        
        if questionnaire == True:
            
            yticks = ax.get_yticks()
            ytick_labels = ax.get_yticklabels()

            # Filter the y-tick labels for values within the desired ylim range
            filtered_labels = [label if -3 <= tick <= 3 else '' for tick, label in zip(yticks, ytick_labels)]
            
            # Set the filtered y-tick labels
            ax.set_yticklabels(filtered_labels)
            
        plt.tight_layout()
    if save != None:
        plt.savefig(save)
    plt.show()